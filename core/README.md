# Module "core"

## Module description
This module provides the core solvers for the two interface modules.

Some details that might be interesting:

The visibility of members is tightly controlled, and most things are at most `internal`, so that the consuming modules
can only see and use the things they are meant to. Consuming modules don't even need to know any of the classes from
this module, they can get hold of the solvers by starting a Koin (Dependency Injection library) instance with the
module provided in [Koin.kt](./src/main/kotlin/Koin.kt) and use that to inject a `Solvers` instance.

There are generic interfaces `Solver` and `Solvers`. A `Solver` is not much more than a function that transforms one
String (the puzzle input) to another String (the puzzle solution). `Solvers` just serves as a mechanism to look up
Solvers by arbitrary keys. This way, those interfaces can serve for all kinds of puzzles that take some input.
Maybe fancy implementing a Sudoku solver with them? 

These generic interfaces are backed by the `AdventOfCode` class which provides the actual implementations for the
Advent of Code puzzles, which are defined in classes implementing the `Day` interface. The `AdventOfCode` class
assumes the role of mapping those classes to `Solver` instances.

## Running the tests
`./gradlew :core:check`
