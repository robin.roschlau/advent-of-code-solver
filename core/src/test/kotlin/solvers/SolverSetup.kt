package com.github.roschlau.adventofcode.core.solvers

import com.github.roschlau.adventofcode.core.args
import com.github.roschlau.adventofcode.core.solvers.y2017.Year2017
import com.github.roschlau.adventofcode.core.solvers.y2018.Year2018
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.fail
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

/**
 * Checks for easy mistakes when assigning AoC Days to keys
 */
internal class SolverSetup {

    @ParameterizedTest(name = "{0}")
    @MethodSource("years")
    fun `day classnames match their keys`(year: Year) {
        val yearNo = year.year()
        year.all().forEach { (dayNo, day) ->
            assertEquals(day.expectedYearAndDay(), yearNo to dayNo)
        }
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource("years")
    fun `no duplicate days`(year: Year) {
        val duplicates = year.all()
            .groupingBy { it.second.javaClass.canonicalName }
            .eachCount()
            .filter { it.value > 1 }
            .map { it.key }
        if (duplicates.isNotEmpty()) {
            fail("There are duplicated days in 2018: $duplicates")
        }
    }

    @Suppress("unused")
    companion object {
        @JvmStatic
        fun years() = args(Year2018, Year2017)
    }


    private val dayNamePattern = Regex("""\.y(20\d\d)\.Day(\d+)""")
    private fun Day.expectedYearAndDay(): Pair<Int, Int> {
        val name = javaClass.canonicalName
        val (year, day) = dayNamePattern.find(name)!!.destructured
        return year.toInt() to day.toInt()
    }

    private val yearNamePattern = Regex("""\.y(20\d\d)\.Year(\d+)""")
    private fun Year.year(): Int {
        val name = javaClass.canonicalName
        val (packageYear, classnameYear) = yearNamePattern.find(name)!!.destructured
        assertEquals(packageYear, classnameYear)
        return classnameYear.toInt()
    }
}
