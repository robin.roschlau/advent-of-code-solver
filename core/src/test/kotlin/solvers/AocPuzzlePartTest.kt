package solvers

import com.github.roschlau.adventofcode.core.solvers.AocPuzzlePart
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class AocPuzzlePartTest {

    @Test
    fun `test part 1 from Int`() {
        assertEquals(
            AocPuzzlePart.First,
            AocPuzzlePart.fromInt(1)
        )
    }

    @Test
    fun `test part 2 from Int`() {
        assertEquals(
            AocPuzzlePart.Second,
            AocPuzzlePart.fromInt(2)
        )
    }

    @Test
    fun `test invalid part`() {
        assertEquals(
            null,
            AocPuzzlePart.fromInt(3)
        )
    }

}
