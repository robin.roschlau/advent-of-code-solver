package com.github.roschlau.adventofcode.core.solvers.y2022

import assertk.assertThat
import assertk.assertions.containsExactly
import com.github.roschlau.adventofcode.core.args
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

class Day10Test {

    @Test
    fun `run small test program`() {
        val signalStrengths = Day10.CPU().run(sequenceOf(Day10.Noop, Day10.AddX(3), Day10.AddX(-5)))
        assertThat(signalStrengths.toList()).containsExactly(
            Day10.CPU.State(1, 1),
            Day10.CPU.State(2, 1),
            Day10.CPU.State(3, 1),
            Day10.CPU.State(4, 4),
            Day10.CPU.State(5, 4)
        )
    }

    @ParameterizedTest(name = "{0} -> {1}")
    @MethodSource("part1Args")
    fun `part 1`(input: String, solution: String) = runBlocking {
        Assertions.assertEquals(solution, Day10.solvePart1(input))
    }

    @ParameterizedTest(name = "{0} -> {1}")
    @MethodSource("part2Args")
    fun `part 2`(input: String, solution: String) = runBlocking {
        Assertions.assertEquals(solution, Day10.solvePart2(input))
    }

    @Suppress("unused")
    companion object {
        @JvmStatic
        fun part1Args() = args(
            testInput to "13140",
            input to "11960",
        )

        @JvmStatic
        fun part2Args() = args(
            testInput to testOutput,
            input to output,
        )
    }
}

private val testOutput = """
    ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  
    ###   ###   ###   ###   ###   ###   ### 
    ####    ####    ####    ####    ####    
    #####     #####     #####     #####     
    ######      ######      ######      ####
    #######       #######       #######     
""".trimIndent()

private val testInput = """
    addx 15
    addx -11
    addx 6
    addx -3
    addx 5
    addx -1
    addx -8
    addx 13
    addx 4
    noop
    addx -1
    addx 5
    addx -1
    addx 5
    addx -1
    addx 5
    addx -1
    addx 5
    addx -1
    addx -35
    addx 1
    addx 24
    addx -19
    addx 1
    addx 16
    addx -11
    noop
    noop
    addx 21
    addx -15
    noop
    noop
    addx -3
    addx 9
    addx 1
    addx -3
    addx 8
    addx 1
    addx 5
    noop
    noop
    noop
    noop
    noop
    addx -36
    noop
    addx 1
    addx 7
    noop
    noop
    noop
    addx 2
    addx 6
    noop
    noop
    noop
    noop
    noop
    addx 1
    noop
    noop
    addx 7
    addx 1
    noop
    addx -13
    addx 13
    addx 7
    noop
    addx 1
    addx -33
    noop
    noop
    noop
    addx 2
    noop
    noop
    noop
    addx 8
    noop
    addx -1
    addx 2
    addx 1
    noop
    addx 17
    addx -9
    addx 1
    addx 1
    addx -3
    addx 11
    noop
    noop
    addx 1
    noop
    addx 1
    noop
    noop
    addx -13
    addx -19
    addx 1
    addx 3
    addx 26
    addx -30
    addx 12
    addx -1
    addx 3
    addx 1
    noop
    noop
    noop
    addx -9
    addx 18
    addx 1
    addx 2
    noop
    noop
    addx 9
    noop
    noop
    noop
    addx -1
    addx 2
    addx -37
    addx 1
    addx 3
    noop
    addx 15
    addx -21
    addx 22
    addx -6
    addx 1
    noop
    addx 2
    addx 1
    noop
    addx -10
    noop
    noop
    addx 20
    addx 1
    addx 2
    addx 2
    addx -6
    addx -11
    noop
    noop
    noop
""".trimIndent()

private val output = """
    ####   ##  ##  #### ###   ##  #    #  # 
    #       # #  # #    #  # #  # #    #  # 
    ###     # #    ###  #  # #    #    #### 
    #       # #    #    ###  # ## #    #  # 
    #    #  # #  # #    #    #  # #    #  # 
    ####  ##   ##  #    #     ### #### #  # 
""".trimIndent()

private val input = """
    addx 1
    noop
    noop
    noop
    addx 5
    addx 5
    noop
    noop
    addx 9
    addx -5
    addx 1
    addx 4
    noop
    noop
    noop
    addx 6
    addx -1
    noop
    addx 5
    addx -2
    addx 7
    noop
    addx 3
    addx -2
    addx -38
    noop
    noop
    addx 32
    addx -22
    noop
    addx 2
    addx 3
    noop
    addx 2
    addx -2
    addx 7
    addx -2
    noop
    addx 3
    addx 2
    addx 5
    addx 2
    addx -5
    addx 10
    noop
    addx 3
    noop
    addx -38
    addx 1
    addx 27
    noop
    addx -20
    noop
    addx 2
    addx 27
    noop
    addx -22
    noop
    noop
    noop
    noop
    addx 3
    addx 5
    addx 2
    addx -11
    addx 16
    addx -2
    addx -17
    addx 24
    noop
    noop
    addx 1
    addx -38
    addx 15
    addx 10
    addx -15
    noop
    addx 2
    addx 26
    noop
    addx -21
    addx 19
    addx -33
    addx 19
    noop
    addx -6
    addx 9
    addx 3
    addx 4
    addx -21
    addx 4
    addx 20
    noop
    addx 3
    addx -38
    addx 28
    addx -21
    addx 9
    addx -8
    addx 2
    addx 5
    addx 2
    addx -9
    addx 14
    addx -2
    addx -5
    addx 12
    addx 3
    addx -2
    addx 2
    addx 7
    noop
    noop
    addx -27
    addx 28
    addx -36
    noop
    addx 1
    addx 5
    addx -1
    noop
    addx 6
    addx -1
    addx 5
    addx 5
    noop
    noop
    addx -2
    addx 20
    addx -10
    addx -3
    addx 1
    addx 3
    addx 2
    addx 4
    addx 3
    noop
    addx -30
    noop
""".trimIndent()
