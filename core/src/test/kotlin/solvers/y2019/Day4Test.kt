package com.github.roschlau.adventofcode.core.solvers.y2019

import com.github.roschlau.adventofcode.core.args
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

class Day4Test {

    @ParameterizedTest(name = "{0} -> {1}")
    @MethodSource("part1Args")
    fun `part 1`(input: String, solution: String) = runBlocking {
        assertEquals(solution, Day4.solvePart1(input))
    }

    @ParameterizedTest(name = "{0} -> {1}")
    @MethodSource("part2Args")
    fun `part 2`(input: String, solution: String) = runBlocking {
        assertEquals(solution, Day4.solvePart2(input))
    }

    @Suppress("unused")
    companion object {
        @JvmStatic
        fun part1Args() = args(
             input to "1063"
        )
        @JvmStatic
        fun part2Args() = args(
            input to "686"
        )
    }
}

private val input = """246540-787419"""
