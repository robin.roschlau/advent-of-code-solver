package com.github.roschlau.adventofcode.core.solvers.y2019

import com.github.roschlau.adventofcode.core.args
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

class Day1Test {

    @ParameterizedTest(name = "{0} -> {1}")
    @MethodSource("part1Args")
    fun `part 1`(input: String, solution: String) = runBlocking {
        assertEquals(solution, Day1.solvePart1(input))
    }

    @ParameterizedTest(name = "{0} -> {1}")
    @MethodSource("part2Args")
    fun `part 2`(input: String, solution: String) = runBlocking {
        assertEquals(solution, Day1.solvePart2(input))
    }

    @Suppress("unused")
    companion object {
        @JvmStatic
        fun part1Args() = args(
             "12" to "2"
            ,"14" to "2"
            ,"1969" to "654"
            ,"100756" to "33583"
            ,input to "3318604"
        )
        @JvmStatic
        fun part2Args() = args(
            "14" to "2"
           ,"1969" to "966"
           ,"100756" to "50346"
           ,input to "4975039"
        )
    }
}

private val input = """
    123457
    98952
    65241
    62222
    144922
    111868
    71513
    74124
    140122
    133046
    65283
    107447
    144864
    136738
    118458
    91049
    71486
    100320
    143765
    88677
    62034
    139946
    81017
    128668
    126450
    56551
    136839
    64516
    91821
    139909
    52907
    78846
    102008
    58518
    128627
    71256
    133546
    90986
    50808
    139055
    88769
    94491
    128902
    55976
    103658
    123605
    113468
    128398
    61725
    100388
    96763
    101378
    139952
    138298
    87171
    51840
    64828
    58250
    88273
    136781
    120097
    127291
    143752
    117291
    100023
    147239
    71296
    100907
    127612
    122424
    62942
    95445
    74040
    118994
    81810
    146408
    98939
    71359
    112120
    100630
    139576
    98998
    92481
    53510
    76343
    125428
    73447
    62472
    91370
    73506
    126539
    50739
    73133
    81906
    100856
    52758
    142303
    107605
    77797
    124355
""".trimIndent()
