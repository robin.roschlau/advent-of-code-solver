package com.github.roschlau.adventofcode.core.solvers

import com.github.roschlau.adventofcode.core.solvers.y2017.Day1Test
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class SolverLookup {

    @Test
    fun `test dummy solver`() = runBlocking {
        assertEquals(
            "Testinput",
            AdventOfCode().findSolver(AdventOfCode.dummyKey)!!.solve("Testinput")
        )
    }

    @Test
    fun `test real solver`() = runBlocking {
        val key = AdventOfCode.buildKey(2017, 1, AocPuzzlePart.First)
        for (arg in Day1Test.part1Args()) {
            val (input, solution) = arg.get()
            assertEquals(
                solution as String,
                AdventOfCode().findSolver(key)!!.solve(input as String)
            )
        }
    }

}
