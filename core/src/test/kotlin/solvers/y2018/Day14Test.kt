package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.args
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

class Day14Test {

    @ParameterizedTest(name = "{0} -> {1}")
    @MethodSource("part1Args")
    fun `part 1`(input: String, solution: String) = runBlocking {
        assertEquals(solution, Day14().solvePart1(input))
    }

    @ParameterizedTest(name = "{0} -> {1}")
    @MethodSource("part2Args")
    fun `part 2`(input: String, solution: String) = runBlocking {
        assertEquals(solution, Day14().solvePart2(input))
    }

    @Suppress("unused")
    companion object {
        @JvmStatic
        fun part1Args() = args(
            "9" to "5158916779"
            ,"5" to "0124515891"
            ,"18" to "9251071085"
            ,"2018" to "5941429882"
            ,"513401" to "5371393113"
        )
        @JvmStatic
        fun part2Args() = args(
            "51589" to "9"
            ,"01245" to "5"
            ,"92510" to "18"
            ,"59414" to "2018"
            ,"513401" to "20286858"
        )
    }
}
