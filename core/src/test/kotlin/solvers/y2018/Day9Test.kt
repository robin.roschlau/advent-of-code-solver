package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.args
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

class Day9Test {

    @ParameterizedTest(name = "{0} -> {1}")
    @MethodSource("part1Args")
    fun `part 1`(input: String, solution: String) = runBlocking {
        assertEquals(solution, Day9().solvePart1(input))
    }

    @ParameterizedTest(name = "{0} -> {1}")
    @MethodSource("part2Args")
    fun `part 2`(input: String, solution: String) = runBlocking {
        assertEquals(solution, Day9().solvePart2(input))
    }

    @Suppress("unused")
    companion object {
        @JvmStatic
        fun part1Args() = args(
             "9 players; last marble is worth 25 points" to "32"
            ,"10 players; last marble is worth 1618 points" to "8317"
            ,"13 players; last marble is worth 7999 points" to "146373"
            ,"17 players; last marble is worth 1104 points" to "2764"
            ,"21 players; last marble is worth 6111 points" to "54718"
            ,"30 players; last marble is worth 5807 points" to "37305"
            ,input to "388131"
        )
        @JvmStatic
        fun part2Args() = args(
            "9 players; last marble is worth 25 points" to "22563"
            ,input to "3239376988"
        )
    }
}

private val input = "459 players; last marble is worth 72103 points"
