package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.args
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

class Day11Test {

    @ParameterizedTest(name = "{0} -> {1}")
    @MethodSource("part1Args")
    fun `part 1`(input: String, solution: String) = runBlocking {
        assertEquals(solution, Day11().solvePart1(input))
    }

    @ParameterizedTest(name = "{0} -> {1}")
    @MethodSource("part2Args")
    fun `part 2`(input: String, solution: String) = runBlocking {
        assertEquals(solution, Day11().solvePart2(input))
    }

    @Suppress("unused")
    companion object {
        @JvmStatic
        fun part1Args() = args(
            "18" to "33,45"
            ,"42" to "21,61"
            ,"5153" to "235,18"
        )
        @JvmStatic
        fun part2Args() = args(
            "18" to "90,269,16"
            ,"42" to "232,251,12"
            ,"5153" to "236,227,12"
        )
    }
}
