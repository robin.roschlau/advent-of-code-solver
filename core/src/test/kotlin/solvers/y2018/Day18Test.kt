package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.args
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

class Day18Test {

    @ParameterizedTest(name = "{0} -> {1}")
    @MethodSource("part1Args")
    fun `part 1`(input: String, solution: String) = runBlocking {
        assertEquals(solution, Day18().solvePart1(input))
    }

    @ParameterizedTest(name = "{0} -> {1}")
    @MethodSource("part2Args")
    fun `part 2`(input: String, solution: String) = runBlocking {
        assertEquals(solution, Day18().solvePart2(input))
    }

    @Suppress("unused")
    companion object {
        @JvmStatic
        fun part1Args() = args(
            inputTest to "1147"
            ,input to "603098"
        )
        @JvmStatic
        fun part2Args() = args(
            inputTest to "0"
            ,input to "210000"
        )
    }
}

private val inputTest = """
.#.#...|#.
.....#|##|
.|..|...#.
..|#.....#
#.#|||#|#|
...#.||...
.|....|...
||...#|.#|
|.||||..|.
...#.|..|.
""".trimIndent()

private val input = """
....#......|.#........###|...||.#|||.||.|...|.#.|.
|#|#....|#...||.|#.#|.|.|..#...||.#|#.|.#||#||.|#.
.....##...#..###..##.|.|....|..#.|...#.#.....|.|..
|....#|||#..||##....||#||.|..|...|..#....#|#....|#
|......|......|.#...|.....|..|.#...#.#..#|.....#|.
|#...#....#...#.|..#..|...|#..|.#.......#..#....|#
....|#.|#...........##...||......##...||#...#..|.|
.#.....|..|...|#..##||..#.#...#...#|.#...#.|....#.
.##|.....|||.....||.#...#...#...#......##...||#...
.||#.|#..|.....#.|.|..........|.#..|##...||...|#..
|......|..#...#.##||..||..#.|..|.|....##|..|..|.|.
|#...####.#.|.....|#..#....|#.#..|.#.#|####.#..|..
.#|.....#.|....|###..#||...#|...||.|.#|#.....|##..
#.|..#|..#...||#.#|...#.##|..|..#...|#.....|..#|..
#.|.....##..||##....|..|.|.|..##.#..|||.....|.....
......##|..|#.|#||...#.|..#..|.#....|..#....#..|##
|........|#.#.|.##...#|..|##.....|##.|.#....#.#...
#.#..#..|.........#|.##.......|...|.#..#.#|####.#.
.....#||#..|......#|.....#..|||..##.......#.#..#.#
#...........#|..#..|.||.|||.|....#||....|#..##.#..
.|...#..##|#...#.||.|##.|..#.||.#.#.#.###...#...#|
|#|...|.......#..#..#....|.###..|.||...|.#...|....
..#.#......|..|.||#.||.......|..#|.#.|..|.#..#.#.#
#..#...|...|..|..#....|..|...|#|...#......#...#...
|...|.|.||#...|...|....|...#.|.#.|.|.|#..|..###.#.
..|.|.....|##...##....|..|.....||...#..||##......|
.#.#....|.##|....|.|..|.|##..#...|##|.#.#.##|....#
..#|.|.....|.#....|...||....|.#..|#.#.|#|.||.||...
.|##.|.#|#|...|...##.||.....|.#|....|.....|#..||..
|.#|...||....#|..#...|.....|.....#|...|#.#|..|....
.|...|....###.|....||##||..|#||.#|##|..|.#.......|
...#.||###|#|.#.|...#...|.##.|.||#..#.......||.#.#
.#|....|#.|.###.##|...|#...#.||..##...#.#|##...#.#
..|#|#..#..#..#|#.....|.#.|...|..#.#......###..|.|
#.|.|..#.#.#.#.....|........|#.||..#......#|.....#
...#.#|#|.|.###|#...#.|......#|.......##||......#.
.#|#.|#..#|...|.|...##|.#....|#........|..|.#.#.#.
..|.##.|#..|...#|.#...#........|.|#|.#.|.|..|#|.#.
...#.#.#||.|||...|#||..##.....###......#..#|||#..#
...#.....#||##.|..#.#|......||..#..#..#..|..|..|..
####.|....|.......|.|.#...|...#.#.......|.|.#...||
..|.|#|.#..##..##...#.....|...|...#|.|...#|..#..##
|...##.#|.........#..||#..||.#....||#..|..||....#|
.#..........#|#.#|#.|...|#....|..|...|...##....|#.
|.|#..|..|......#..|...|..##|||#...|..##|...#.|#..
||#.||.#|.|...#.........#...|.|##.#.|#||.|.#|..#..
|..|..|..#....#...|.......#|.........|#....#|....|
##..###......#|.........|.......|...||.......#|..#
|..............#.......#...#|.|||..#..|..#........
...|||.#.|.#.|..#.....##|....###.#.|....|.......|.
""".trimIndent()
