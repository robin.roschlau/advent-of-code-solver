package com.github.roschlau.adventofcode.core.grids

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class FixedGrid2DTest {

    @Test
    fun `produces correct neighbours`() {
        val grid = FixedGrid2D(
            arrayOf(
                arrayOf(1, 2, 3),
                arrayOf(4, 5, 6),
                arrayOf(7, 8, 9),
            )
        )
        assertEquals(
            listOf(1, 2, 3, 4, 6, 7, 8, 9),
            grid.get8Neighbours(1 co 1),
        )
    }

    @Test
    fun `produces correct neighbours at edge`() {
        val grid = FixedGrid2D(
            arrayOf(
                arrayOf(1, 2, 3),
                arrayOf(4, 5, 6),
                arrayOf(7, 8, 9),
            )
        )
        assertEquals(
            listOf(2, 5, 6),
            grid.get8Neighbours(0 co 2),
        )
    }
}
