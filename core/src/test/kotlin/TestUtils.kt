package com.github.roschlau.adventofcode.core

import org.junit.jupiter.params.provider.Arguments

/**
 * Transforms a series of pairs into a [java.util.stream.Stream] of [Arguments].
 */
fun <T> args(vararg args: T) =
    args.map { Arguments.of(it) }.stream()

/**
 * Transforms a series of pairs into a [java.util.stream.Stream] of [Arguments].
 */
@JvmName("pairArgs")
fun <I, O> args(vararg args: Pair<I, O>) =
    args.map { Arguments.of(it.first, it.second) }.stream()

/**
 * Transforms a series of triplets into a [java.util.stream.Stream] of [Arguments]:
 *
 * ```
 * args(
 *     a1 to a2 to a3,
 *     b1 to b2 to b3
 * )
 * ```
 */
@JvmName("tripleArgs")
fun <I1, I2, O> args(vararg args: Pair<Pair<I1, I2>, O>) =
    args.map { Arguments.of(it.first.first, it.first.second, it.second) }.stream()
