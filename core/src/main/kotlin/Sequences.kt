package com.github.roschlau.adventofcode.core


data class Cycle<T>(
    val period: Int,
    val startIndex: Int,
    val elements: Sequence<T>
) {
    fun elementAt(i: Long): T {
        require(i >= startIndex) { "Tried to access element at index $i, cycle starts at index $startIndex" }
        return elements.elementAt(((i - startIndex) % period).toInt())
    }
}

/**
 * Attempts to find a cyclical, infinitely repeating pattern in a sequence of elements.
 * This function assumes that each element in the sequence is only dependent on its immediate predecessor, so the first
 * element that's encountered twice in the Sequence is considered the start of the cycle and it is assumed the Sequence
 * will repeat infinitely from there on. This works well for game of life style sequences, where each state is directly
 * derived from the previous one, but doesn't work for sequences where any value might appear multiple times before the
 * sequence develops a cyclical pattern.
 */
fun <T> Sequence<T>.findCycle(): Cycle<T> {
    val elements = mutableMapOf<T, Int>()
    val (index, element) = this
        .withIndex()
        .first {
            if (it.value in elements) {
                true
            } else {
                elements += it.value to it.index
                false
            }
        }
    val startIndex = elements[element]!!
    val period = index - startIndex
    return Cycle(
        period = period,
        startIndex = startIndex,
        elements = this.drop(startIndex).take(period)
    )
}

/**
 * Repeats this iterable infinitely. If this iterable has no elements, the resulting sequence will be empty.
 */
internal fun <T : Any> Iterable<T>.repeat(): Sequence<T> =
    when {
        !iterator().hasNext() -> sequenceOf()
        else -> generateSequence { asSequence() }.flatten()
    }

/**
 * Works exactly like [Sequence.fold], except it doesn't return only the end result, but a sequence of all intermediary
 * results after each application of the accumulator function.
 * `seq.accumulating(init, acc).last()` yields the same result as `seq.fold(init, acc)`
 * The first element in the new sequence will be [initial] before having been combined with the first element of this
 * sequence, so the size of the new sequence will be the size of this sequence plus one.
 * ```
 * sequenceOf(1, 1, 1).accumulating(0, Int::add) == sequenceOf(0, 1, 2, 3)
 * ```
 */
internal fun <R, T> Sequence<T>.accumulating(initial: R, accumulator: (R, T) -> R): Sequence<R> =
    sequence {
        var accumulated = initial
        yield(accumulated)
        forEach { elem ->
            accumulated = accumulator(accumulated, elem)
            yield(accumulated)
        }
    }

/**
 * Returns the first element that is encountered a second time in this sequence, or null if the sequence terminates
 * before yielding a repeated item.
 * ```
 * sequenceOf(1, 2, 3, 4, 2, 3, 1).firstRepeated() == 2
 * ```
 */
internal fun <T : Any> Sequence<T>.firstRepeated(): T? {
    val known = mutableSetOf<T>()
    return this.firstOrNull { !known.add(it) }
}
