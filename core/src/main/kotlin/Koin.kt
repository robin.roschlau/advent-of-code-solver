package com.github.roschlau.adventofcode.core

import com.github.roschlau.adventofcode.core.solvers.AdventOfCode
import com.github.roschlau.adventofcode.core.solvers.Solvers
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.experimental.builder.single

val coreModule = module {
    // Declare AdventOfCode as a bean and also register it as the Bean to inject when the type `Solvers` is requested
    single<AdventOfCode>() bind Solvers::class
}
