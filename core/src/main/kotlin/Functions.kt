package com.github.roschlau.adventofcode.core

/**
 * Repeatedly applies this function to it's output a set number of [times], using the last output as input.
 */
operator fun <T> ((T) -> T).times(times: Int): (T) -> T = { t ->
    var result = t
    repeat(times) {
        result = this(result)
    }
    result
}
