package com.github.roschlau.adventofcode.core

fun Regex.findAllOverlapping(input: CharSequence, startIndex: Int = 0): Sequence<MatchResult> {
    if (startIndex < 0 || startIndex > input.length) {
        throw IndexOutOfBoundsException("Start index out of bounds: $startIndex, input length: ${input.length}")
    }
    return generateSequence({ find(input, startIndex) }) { find(input, it.range.first + 1) }
}
