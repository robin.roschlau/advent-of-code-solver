package com.github.roschlau.adventofcode.core

fun <T> Iterable<T>.permutations(k: Int = this.count()) = variations(k, repetitions = false)

/**
 * Returns a Set of all possible variations of size [k] of this iterable.
 *
 * Warning: This function works recursively, so beware of using it on large collections!
 *
 * @param repetitions controls whether or not the resulting variations may contain repetitions of distinct elements
 */
fun <T> Iterable<T>.variations(k: Int, repetitions: Boolean = false): Set<List<T>> {
    val list = this as? List<T> ?: this.toList()
    require(k >= 0) { "K must not be negative" }
    require(k <= list.size || repetitions) { "K must not be larger than the size of this Iterable if no repetitions are allowed" }
    if (k == 0) return setOf()
    if (list.size == 1) return setOf(list.toList())
    if (k == 1) return list.map { listOf(it) }.toSet()
    return list
        .flatMap { pickedItem ->
            val remaining = if (repetitions) list else list - pickedItem
            remaining.variations(k - 1, repetitions).map { listOf(pickedItem) + it }
        }
        .toSet()
}

fun <T> Iterable<T>.combinations(k: Int, repetitions: Boolean = false): Set<List<T>> = this
    .variations(k, repetitions)
    .distinctBy { permutation ->
        permutation
            .groupBy { it }
            .mapValues { it.value.size }
    }
    .toSet()

fun <T> Iterable<T>.combinations(): List<Pair<T, T>> = this
    .mapIndexed { index, first ->
        this.drop(index + 1).map { first to it }
    }
    .flatten()

/**
 * Returns a List of all possible Pairings between elements of this Iterable. In the context of this function,
 * (X, Y) and (Y, X) are treated as different pairings, so calling `listOf(1, 2, 3).possiblePairings(false)` will yield
 * `[(1, 2), (2, 1), (1, 3), (3, 1), (2, 3), (3, 2)]`.
 * Calling this method is just a convenience for calling [variations] with k=2.
 */
fun <T> Iterable<T>.possiblePairings(includeSelf: Boolean = false): List<Pair<T, T>> {
    val result = variations(2, includeSelf)
    check(result.all { it.size == 2 })
    return result.map { it[0] to it[1] }
}

fun Sequence<Int>.minMax() = (this.minOrNull() ?: 0) to (this.maxOrNull() ?: 0)
fun Iterable<Int>.range() = (this.minOrNull() ?: 0) .. (this.maxOrNull() ?: 0)
