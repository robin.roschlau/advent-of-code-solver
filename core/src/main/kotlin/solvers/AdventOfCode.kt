package com.github.roschlau.adventofcode.core.solvers

import com.github.roschlau.adventofcode.core.solvers.y2017.Year2017
import com.github.roschlau.adventofcode.core.solvers.y2018.Year2018
import com.github.roschlau.adventofcode.core.solvers.y2019.Year2019
import com.github.roschlau.adventofcode.core.solvers.y2020.Year2020
import com.github.roschlau.adventofcode.core.solvers.y2021.Year2021
import com.github.roschlau.adventofcode.core.solvers.y2022.Year2022
import com.github.roschlau.adventofcode.core.solvers.y2023.Year2023

/**
 * Solvers for the Advent of Code puzzles. Individual solvers are implemented in their year's respective package:
 * `com.github.roschlau.adventofcode.core.solvers.y20XX`
 */
class AdventOfCode : Solvers {

    override val knownKeys by lazy { solvers.keys }

    internal val solvers: Map<String, Solver> =
        listOf(
            2017 to Year2017.all(),
            2018 to Year2018.all(),
            2019 to Year2019.all(),
            2020 to Year2020.all(),
            2021 to Year2021.all(),
            2022 to Year2022.all(),
            2023 to Year2023.all(),
        )
            .flatMap { (year, days) ->
                days.flatMap { (dayNo, day) ->
                    listOf(
                        buildKey(year, dayNo, AocPuzzlePart.First) to solver(day::solvePart1),
                        buildKey(year, dayNo, AocPuzzlePart.Second) to solver(day::solvePart2),
                    )
                }
            }
            // Insert a dummy solver here, so we can test access to the AoC Solvers without relying on actually having
            // implemented any specific one
            .plus(dummyKey to dummySolver())
            .toMap()

    override fun findSolver(key: String): Solver? {
        return solvers[key]
    }

    companion object {
        /**
         * A key that's not used for any real puzzle and instead identifies a dummy solver returning the input as its
         * solution.
         */
        val dummyKey = buildKey(0, 0, AocPuzzlePart.First)

        /**
         * Gives you the key for a specific AoC puzzle from the year, day and puzzle part.
         */
        fun buildKey(year: Int, day: Int, aocPuzzlePart: AocPuzzlePart): String {
            return "AOC-$year-$day-${aocPuzzlePart.int}"
        }
    }
}

/** One year of AoC puzzles */
internal interface Year {
    /** All the AoC puzzles of this year. All those that are implemented, anyway. */
    fun all(): List<Pair<Int, Day>>
}

/** One day of AoC that can solve both of its parts */
internal interface Day {
    suspend fun solvePart1(input: String): Any
    suspend fun solvePart2(input: String): Any

    companion object {
        operator fun <T> invoke(
            parseInput: (String) -> T,
            part1: (T) -> Any,
            part2: (T) -> Any,
        ) = object : Day {
            override suspend fun solvePart1(input: String) = part1(parseInput(input))
            override suspend fun solvePart2(input: String) = part2(parseInput(input))
        }
    }
}

/**
 * Each puzzle of AoC has two parts, so we need a solver for each part.
 */
enum class AocPuzzlePart(val int: Int) {
    First(1), Second(2);

    companion object {
        fun fromInt(int: Int): AocPuzzlePart? =
            when (int) {
                1 -> First
                2 -> Second
                else -> null
            }
    }
}
