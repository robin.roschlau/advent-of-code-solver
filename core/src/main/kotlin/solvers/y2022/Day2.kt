package com.github.roschlau.adventofcode.core.solvers.y2022

import com.github.roschlau.adventofcode.core.solvers.Day
import com.github.roschlau.adventofcode.core.solvers.y2022.Day2.Move.*

object Day2 : Day {

    override suspend fun solvePart1(input: String): String {
        return input.lineSequence()
            .map { it.split(" ") }
            .map { (opponent, me) -> Round(Move.parse(opponent), Move.parse(me)) }
            .sumOf { it.score }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        return input.lineSequence()
            .map { it.split(" ") }
            .map { (opponent, me) -> Round(Move.parse(opponent), Outcome.parse(me)) }
            .sumOf { it.score }
            .toString()
    }

    class Round(
        private val opponent: Move,
        private val me: Move,
    ) {
        val outcome by lazy {
            when (opponent) {
                me -> Outcome.Draw
                Rock -> winIf(me == Paper)
                Paper -> winIf(me == Scissors)
                Scissors -> winIf(me == Rock)
            }
        }
        val score by lazy {
            me.score + outcome.score
        }

        companion object {
            operator fun invoke(opponent: Move, outcome: Outcome): Round =
                Move.values()
                    .map { Round(opponent, it) }
                    .first { it.outcome == outcome }
        }
    }

    enum class Move(
        val letters: String,
        val score: Int,
    ) {
        Rock("AX", 1), Paper("BY", 2), Scissors("CZ", 3);

        companion object {
            fun parse(char: String) = values().first { char in it.letters }
        }
    }

    enum class Outcome(val letters: String, val score: Int) {
        Win("Z", 6), Draw("Y", 3), Lose("X", 0);

        companion object {
            fun parse(char: String) = Outcome.values().first { char in it.letters }
        }
    }

    fun winIf(condition: Boolean) = if (condition) Outcome.Win else Outcome.Lose
}
