package com.github.roschlau.adventofcode.core.solvers.y2022

import com.github.roschlau.adventofcode.core.solvers.Day

object Day6 : Day {

    override suspend fun solvePart1(input: String): String {
        return findIndexAfterFirstSequenceOfDistinctCharacters(input, 4)
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        return findIndexAfterFirstSequenceOfDistinctCharacters(input, 14)
            .toString()
    }

    private fun findIndexAfterFirstSequenceOfDistinctCharacters(input: String, length: Int) =
        input.windowed(length, step = 1)
            .first { it.toSet().size == length }
            .let { input.indexOf(it) + length }
}
