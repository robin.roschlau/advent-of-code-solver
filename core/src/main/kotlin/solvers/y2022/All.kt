package com.github.roschlau.adventofcode.core.solvers.y2022

import com.github.roschlau.adventofcode.core.solvers.Year

internal object Year2022 : Year {
    override fun all() = listOf(
        1 to Day1,
        2 to Day2,
        3 to Day3,
        4 to Day4,
        5 to Day5,
        6 to Day6,
        7 to Day7,
        8 to Day8,
        9 to Day9,
        10 to Day10,
        13 to Day13,
        14 to Day14,
        15 to Day15,
        20 to Day20,
        21 to Day21,
    )
}
