package com.github.roschlau.adventofcode.core.solvers.y2022

import com.github.roschlau.adventofcode.core.solvers.Day

object Day1 : Day {

    override suspend fun solvePart1(input: String): String {
        return input.split("\n\n")
            .maxOf { elf -> elf.lines().sumOf { it.toInt() } }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        return input.split("\n\n")
            .map { elf -> elf.lines().sumOf { it.toInt() } }
            .sortedDescending()
            .take(3)
            .sum()
            .toString()
    }
}
