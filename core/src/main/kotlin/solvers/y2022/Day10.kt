package com.github.roschlau.adventofcode.core.solvers.y2022

import com.github.roschlau.adventofcode.core.solvers.Day

object Day10 : Day {

    override suspend fun solvePart1(input: String): String {
        val program = CpuInstruction.parse(input)
        return CPU().run(program)
            .filter { (i, _) -> (i + 20) % 40 == 0 }
            .sumOf { state -> state.cycle * state.x }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        val program = CpuInstruction.parse(input)
        return drawCRT(CPU().run(program))
    }

    sealed interface CpuInstruction {
        companion object {
            fun parse(input: String): Sequence<CpuInstruction> =
                input.lineSequence().map {
                    when (it) {
                        "noop" -> Noop
                        else -> AddX(it.removePrefix("addx ").toInt())
                    }
                }
        }
    }

    object Noop : CpuInstruction
    class AddX(val v: Int) : CpuInstruction

    class CPU {
        private var x = 1

        data class State(val cycle: Int, val x: Int)

        /**
         * Runs the program and returns the signal strength values for every passed cycle
         */
        fun run(program: Sequence<CpuInstruction>): Sequence<State> = sequence {
            var cycle = 1
            for (instruction in program) {
                when (instruction) {
                    is AddX -> {
                        yield(State(cycle, x))
                        cycle++
                        yield(State(cycle, x))
                        cycle++
                        x += instruction.v
                    }

                    Noop -> {
                        yield(State(cycle, x))
                        cycle++
                    }
                }
            }
        }
    }

    fun drawCRT(states: Sequence<CPU.State>): String =
        states.windowed(40, step = 40).joinToString("\n") { line ->
            line.withIndex().joinToString("") { (position, state) ->
                if (position in (state.x - 1)..(state.x + 1)) {
                    "#"
                } else {
                    " "
                }
            }
        }
}
