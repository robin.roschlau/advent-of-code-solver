package com.github.roschlau.adventofcode.core.solvers.y2022

import com.github.roschlau.adventofcode.core.solvers.Day
import com.github.roschlau.adventofcode.core.solvers.y2018.join

object Day5 : Day {

    override suspend fun solvePart1(input: String): String {
        val (ship, instructions) = parse(input)
        Crane.CM9000.unload(ship, instructions)
        return ship.topCrates()
    }

    override suspend fun solvePart2(input: String): String {
        val (ship, instructions) = parse(input)
        Crane.CM9001.unload(ship, instructions)
        return ship.topCrates()
    }

    class Ship(private val stacks: List<MutableList<Char>>) {
        fun move(count: Int, fromIndex: Int, toIndex: Int, crane: Crane) {
            crane.move(count, stacks[fromIndex], stacks[toIndex])
        }

        fun topCrates() =
            stacks.map { it.last() }.join()
    }

    enum class Crane {
        CM9000,
        CM9001;

        fun unload(ship: Ship, instructions: Sequence<String>) {
            instructions.forEach { line ->
                val (count, from, to) = Regex("""move (\d+) from (\d) to (\d)""").matchEntire(line)!!.destructured
                ship.move(count.toInt(), from.toInt() - 1, to.toInt() - 1, this)
            }
        }

        fun move(count: Int, from: MutableList<Char>, to: MutableList<Char>): Unit = when (this) {
            CM9000 -> repeat(count) { to += from.removeLast() }
            CM9001 -> {
                to += from.takeLast(count)
                repeat(count) { from.removeLast() }
            }
        }
    }

    private fun parse(input: String): Pair<Ship, Sequence<String>> {
        val (shipInput, instructionsInput) = input.split("\n\n")
        val ship = parseShip(shipInput)
        return ship to instructionsInput.lineSequence()
    }

    private fun parseShip(input: String): Ship {
        val stacksUpsideDown = input.lineSequence()
            .map { it.withIndex().filter { (i) -> i % 4 == 1 }.joinToString("") { (_, char) -> char.toString() } }
            .toList()
            .reversed()
        val stacks = List(stacksUpsideDown.first().length) { mutableListOf<Char>() }
        for (line in stacksUpsideDown) {
            line.withIndex()
                .filter { (_, char) -> char != ' ' }
                .forEach { (i, char) -> stacks[i] += char }
        }
        return Ship(stacks)
    }
}
