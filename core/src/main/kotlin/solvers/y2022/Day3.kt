package com.github.roschlau.adventofcode.core.solvers.y2022

import com.github.roschlau.adventofcode.core.solvers.Day

object Day3 : Day {

    override suspend fun solvePart1(input: String): String {
        return input.lineSequence()
            .sumOf { contents ->
                val left = contents.take(contents.length / 2)
                val right = contents.drop(contents.length / 2)
                val duplicate = left.toSet().intersect(right.toSet())
                    .also { check(it.size == 1) { "$it contained more than one char" } }
                    .first()
                duplicate.priority
            }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        return input.lineSequence()
            .windowed(3, 3)
            .sumOf { (a, b, c) ->
                val badge = a.toSet().intersect(b.toSet()).intersect(c.toSet())
                check(badge.size == 1)
                badge.first().priority
            }
            .toString()
    }

    val Char.priority get() = when (this) {
        in 'a'..'z' -> this.code - 96
        in 'A'..'Z' -> this.code - 65 + 27
        else -> throw IllegalArgumentException("$this not a valid character")
    }
}
