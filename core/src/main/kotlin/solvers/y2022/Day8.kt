package com.github.roschlau.adventofcode.core.solvers.y2022

import com.github.roschlau.adventofcode.core.solvers.Day

object Day8 : Day {

    override suspend fun solvePart1(input: String): String {
        val grid = input.lines()
        val visibleGrid = Array(grid.size) { IntArray(grid[0].length) { -1 } }
        for (y in grid.indices) {
            var maxTop = -1
            for (x in grid[y].indices) {
                val tree = grid[y][x].digitToInt()
                if (tree > maxTop) {
                    visibleGrid[y][x] = tree
                    maxTop = tree
                }
            }
            var maxBottom = -1
            for (x in grid[y].indices.reversed()) {
                val tree = grid[y][x].digitToInt()
                if (tree > maxBottom) {
                    visibleGrid[y][x] = tree
                    maxBottom = tree
                }
            }
        }
        for (x in grid[0].indices) {
            var maxTop = -1
            for (y in grid.indices) {
                val tree = grid[y][x].digitToInt()
                if (tree > maxTop) {
                    visibleGrid[y][x] = tree
                    maxTop = tree
                }
            }
            var maxBottom = -1
            for (y in grid.indices.reversed()) {
                val tree = grid[y][x].digitToInt()
                if (tree > maxBottom) {
                    visibleGrid[y][x] = tree
                    maxBottom = tree
                }
            }
        }
        return visibleGrid
            .flatMap { it.toList() }
            .count { it > -1 }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        val grid = input.lines().map { row -> row.map { it.digitToInt() } }
        val scenicScores = Array(grid.size) { IntArray(grid[0].size) { 0 } }
        for ((x, y) in grid.indices.flatMap { y -> grid[y].indices.map { x -> x to y } }) {
            if (x == 0 || y == 0 || x == grid[y].lastIndex || y == grid.lastIndex) {
                continue
            }
            val tree = grid[y][x]
            val viewingDistances = mapOf(
                "left " to ((x - 1) downTo 1).takeWhile { grid[y][it] < tree }.count() + 1,
                "right" to ((x + 1) until grid[y].lastIndex).takeWhile { grid[y][it] < tree }.count() + 1,
                "up   " to ((y - 1) downTo 1).takeWhile { grid[it][x] < tree }.count() + 1,
                "down " to ((y + 1) until grid.lastIndex).takeWhile { grid[it][x] < tree }.count() + 1,
            )
            scenicScores[y][x] = viewingDistances.values.reduce(Int::times)
        }
        return scenicScores
            .flatMap { it.toList() }
            .max()
            .toString()
    }
}
