package com.github.roschlau.adventofcode.core.solvers.y2022

import com.github.roschlau.adventofcode.core.solvers.Day

object Day4 : Day {

    override suspend fun solvePart1(input: String): String {
        return input.lineSequence()
            .map { it.split(",").map(::parseRange) }
            .count { (a, b) -> a in b || b in a }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        return input.lineSequence()
            .map { it.split(",").map(::parseRange) }
            .count { (a, b) -> a.overlaps(b) }
            .toString()
    }

    fun parseRange(input: String): IntRange {
        val (a, b) = input.split("-")
        return a.toInt()..b.toInt()
    }

    operator fun IntRange.contains(other: IntRange) =
        other.first in this && other.last in this

    fun IntRange.overlaps(other: IntRange) =
        other.first <= this.last && other.last >= this.first
}
