package com.github.roschlau.adventofcode.core.solvers.y2022

import com.github.roschlau.adventofcode.core.solvers.Day

object Day7 : Day {

    override suspend fun solvePart1(input: String): String {
        val root = parseInput(input)
        return root.lsRecursively()
            .filterIsInstance<Directory>()
            .filter { it.size <= 100000 }
            .sumOf { it.size }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        val totalSpace = 70_000_000L
        val minimumNeeded = 30_000_000L
        val root = parseInput(input)
        val usedSpace = root.size
        val remainingSpace = totalSpace - usedSpace
        val spaceToClear = minimumNeeded - remainingSpace
        return root.lsRecursively()
            .filterIsInstance<Directory>()
            .filter { it.size >= spaceToClear }
            .minOf { it.size }
            .toString()
    }

    fun parseInput(input: String): Directory {
        val root = Directory()
        var currentPath = listOf("/")
        for (line in input.lineSequence()) {
            when {
                line.startsWith("$ cd") -> currentPath = navigate(currentPath, line.drop(5))
                line.startsWith("$ ls") -> { /* Noop */ }
                else -> root.getDir(currentPath.drop(1)).add(parseLsOutputLine(line))
            }
        }
        return root
    }

    fun navigate(currentPath: List<String>, to: String): List<String> {
        return when (to) {
            ".." -> currentPath.dropLast(1)
            "/" -> listOf("/")
            else -> currentPath + to
        }
    }

    fun parseLsOutputLine(line: String): Pair<String, FileSystemObject> {
        return when {
            line.startsWith("dir ") ->
                line.drop(4) to Directory()
            else ->
                line.split(" ")
                    .let { (size, name) -> name to File(size.toLong()) }
        }
    }

    sealed class FileSystemObject {
        abstract val size: Long
    }
    class Directory : FileSystemObject() {
        private val objects = mutableMapOf<String, FileSystemObject>()

        override val size: Long
            get() = objects.values.sumOf { it.size }

        fun add(obj: Pair<String, FileSystemObject>) {
            this.objects += obj
        }

        fun getDir(relativePath: List<String>): Directory {
            if (relativePath.isEmpty()) return this
            val next = requireNotNull(objects[relativePath.first()] as? Directory) { "$relativePath not found or not a directory" }
            return if (relativePath.size == 1) {
                next
            } else {
                next.getDir(relativePath.drop(1))
            }
        }

        fun lsRecursively(): List<FileSystemObject> {
            return objects.values.toList() +
                    objects.values.filterIsInstance<Directory>().flatMap { it.lsRecursively() }
        }
    }
    class File(override val size: Long) : FileSystemObject()
}
