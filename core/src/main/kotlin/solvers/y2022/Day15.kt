package com.github.roschlau.adventofcode.core.solvers.y2022

import com.github.roschlau.adventofcode.core.grids.Vector2DInt
import com.github.roschlau.adventofcode.core.grids.co
import com.github.roschlau.adventofcode.core.grids.manhattanDistanceTo
import com.github.roschlau.adventofcode.core.grids.plus
import com.github.roschlau.adventofcode.core.solvers.Day

object Day15 : Day {

    override suspend fun solvePart1(input: String): String {
        val sensorData = parse(input)
        val targetRow = if (sensorData.maxOf { (sensor) -> sensor.y } < 1000) 10 else 2_000_000
        val coveredPositions = sensorData
            .map { (sensor, beacon) -> sensor to sensor.manhattanDistanceTo(beacon) }
            .flatMap { (sensor, distance) -> positionsWithinManhattanRadius(sensor, distance) }
        return coveredPositions.filter { it.y == targetRow }.distinct().count().toString()
    }

    override suspend fun solvePart2(input: String): String {
        return input.lineSequence()
            .map { TODO() }
            .toString()
    }

    fun parse(input: String): Sequence<Pair<Vector2DInt, Vector2DInt>> {
        val regex = Regex("""x=(-?\d+?), y=(-?\d+?)""")
        return input.lineSequence()
            .map { line ->
                regex.findAll(line)
                    .map { it.destructured }
                    .map { (x, y) -> x.toInt() to y.toInt() }
                    .toList()
                    .let { (x, y) -> (x.first co x.second) to (y.first co y.second) }
            }
    }

    fun positionsWithinManhattanRadius(center: Vector2DInt, radius: Int): Sequence<Vector2DInt> {
        return (0..radius).asSequence()
            .flatMap { positionsWithDistanceOf(center, it) }
    }

    fun positionsWithDistanceOf(center: Vector2DInt, distance: Int): Sequence<Vector2DInt> {
        require(distance >= 0)
        if (distance == 0) return sequenceOf(center)
        return (0 until distance)
            .zip(distance downTo 1)
            .asSequence()
            .flatMap { (x, y) ->
                sequenceOf(
                    x co y, // top right edge
                    -y co x, // top left edge
                    y co -x, // bottom right edge
                    -x co -y, // bottom left edge
                ).map { center + it }
            }
    }
}
