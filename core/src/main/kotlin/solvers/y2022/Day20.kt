package com.github.roschlau.adventofcode.core.solvers.y2022

import com.github.roschlau.adventofcode.core.solvers.Day
import java.lang.IllegalStateException
import kotlin.math.absoluteValue

object Day20 : Day {

    override suspend fun solvePart1(input: String): String {
        return input.lineSequence()
            .map { it.toLong() }
            .let { EncryptedFile(it.toList()) }
            .also { it.mix() }
            .let { file ->
                val zeroIndex = file.indexOf(0)
                listOf(1000, 2000, 3000).sumOf { file[zeroIndex + it] }
            }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        return input.lineSequence()
            .map { it.toLong() * 811589153 }
            .let { EncryptedFile(it.toList()) }
            .also { file -> repeat(10) { file.mix() } }
            .let { file ->
                val zeroIndex = file.indexOf(0)
                listOf(1000, 2000, 3000).sumOf { file[zeroIndex + it] }
            }
            .toString()
    }

    class EncryptedFile(numbers: List<Long>) {
        private val originalOrder: List<NumberNode>
        private val size = numbers.size
        private var currentFirst: NumberNode

        init {
            val all = mutableListOf<NumberNode>()
            currentFirst = NumberNode(numbers[0])
            var prev = currentFirst
            var next: NumberNode
            all += currentFirst
            for (number in numbers.drop(1)) {
                next = NumberNode(number, prev)
                all += next
                prev.next = next
                prev = next
            }
            prev.next = currentFirst
            currentFirst.prev = prev
            originalOrder = all
            check((originalOrder + originalOrder[0]).zipWithNext().all { (a, b) -> a.next == b && b.prev == a })
        }

        operator fun get(index: Int): Long {
            return numbersInCurrentOrder().drop(index % size).first().value
        }

        fun indexOf(value: Long): Int {
            return numbersInCurrentOrder().withIndex().first { it.value.value == value }.index
        }

        fun mix() {
            for (node in originalOrder) {
                repeat((node.value.absoluteValue % (size - 1)).toInt()) {
                    val swapPartner = if (node.value >= 0) node.next else node.prev
                    swapNeighbors(node, swapPartner!!)
                }
            }
        }

        private fun numbersInCurrentOrder(): Sequence<NumberNode> {
            val root = currentFirst
            return generateSequence(root) { prev -> prev.next?.takeIf { it != root } }
        }

        private fun swapNeighbors(a: NumberNode, b: NumberNode) {
            val first = when {
                a.next == b -> a
                b.next == a -> b
                else -> throw IllegalStateException("Nodes $a and $b are not neighbors")
            }
            val second = if (first == a) b else a
            val prev = first.prev!!
            first.prev = second
            second.prev = prev
            val next = second.next!!
            second.next = first
            first.next = next
            prev.next = second
            next.prev = first
            check(first.prev == second && second.next == first)
        }

        private class NumberNode(
            val value: Long,
            var prev: NumberNode? = null,
            var next: NumberNode? = null,
        ) {
            override fun toString(): String {
                return "(${prev?.value} $value ${next?.value})"
            }
        }
    }
}
