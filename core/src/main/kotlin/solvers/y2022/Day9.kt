package com.github.roschlau.adventofcode.core.solvers.y2022

import com.github.roschlau.adventofcode.core.grids.*
import com.github.roschlau.adventofcode.core.solvers.Day

object Day9 : Day {
    override suspend fun solvePart1(input: String): String {
        return solve(input, 2).toString()
    }

    override suspend fun solvePart2(input: String): String {
        return solve(input, 10).toString()
    }

    fun solve(input: String, ropeSegments: Int): Int {
        val tailTrail = SparseGrid2D<Visited>()
        val rope = Rope2D(ropeSegments)
        for (line in input.lineSequence()) {
            val direction = Direction2D.forChar(line[0])
            repeat(line.drop(2).toInt()) {
                rope.moveHead(direction)
                tailTrail[rope.tail()] = Visited
            }
        }
        println(tailTrail.show { (x, y), cell -> if (x == 0 && y == 0) 's' else if (cell == Visited) '#' else '.' })
        return tailTrail.count()
    }

    class Rope2D(length: Int) {
        private val segments = List(length) { 0 co 0 }.toMutableList()

        fun moveHead(direction: Direction2D) {
            segments[0] += direction.unitVector
            for (i in segments.indices.drop(1)) {
                val cur = segments[i]
                val prev = segments[i - 1]
                if (cur != prev && cur !in prev.adjacent()) {
                    val diff = prev - cur
                    segments[i] += diff.x.coerceIn(-1..1) co diff.y.coerceIn(-1..1)
                    check(segments[i] in prev.adjacent())
                }
            }
        }

        fun tail() = segments.last()
    }

    object Visited
}
