package com.github.roschlau.adventofcode.core.solvers.y2022

import com.github.roschlau.adventofcode.core.solvers.Day
import com.github.roschlau.adventofcode.core.solvers.y2022.Day21.Op.Function.Div
import com.github.roschlau.adventofcode.core.solvers.y2022.Day21.Op.Function.Minus
import com.github.roschlau.adventofcode.core.solvers.y2022.Day21.Op.Function.Plus
import com.github.roschlau.adventofcode.core.solvers.y2022.Day21.Op.Function.Times

object Day21 : Day {

    override suspend fun solvePart1(input: String): String {
        return input.lineSequence()
            .associate { line ->
                val (name, expression) = line.split(": ")
                name to Expression.parse(expression)
            }
            .let { expressions ->
                val solvedExpressions = expressions.entries
                    .mapNotNull { (name, op) -> (op as? Literal)?.let { name to op.value } }
                    .toMap().toMutableMap()
                val unsolvedOps = expressions.filterValues { it is Op }.toMutableMap() as MutableMap<String, Op>
                while (unsolvedOps.isNotEmpty()) {
                    for ((name, op) in unsolvedOps.toList()) {
                        val a = solvedExpressions[op.a] ?: continue
                        val b = solvedExpressions[op.b] ?: continue
                        solvedExpressions[name] = op.op.function(a, b)
                        unsolvedOps.remove(name)
                    }
                }
                solvedExpressions["root"]!!
            }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        return input.lineSequence()
            .associate { line ->
                val (name, expression) = line.split(": ")
                when (name) {
                    "humn" -> name to Human
                    "root" -> name to expression.split(" ").let { (a, _, b) -> Equality(a, b) }
                    else -> name to Expression.parse(expression)
                }
            }
            .let { expressions ->
                val solvedExpressions = expressions.entries
                    .mapNotNull { (name, op) -> (op as? Literal)?.let { name to op.value } }
                    .toMap().toMutableMap()
                val unsolvedOps = expressions.filterValues { it is Op }.toMutableMap() as MutableMap<String, Op>
                var previousSize = 0
                while (unsolvedOps.size != previousSize) {
                    previousSize = unsolvedOps.size
                    for ((name, op) in unsolvedOps.toList()) {
                        val a = solvedExpressions[op.a] ?: continue
                        val b = solvedExpressions[op.b] ?: continue
                        solvedExpressions[name] = op.op.function(a, b)
                        unsolvedOps.remove(name)
                    }
                }
                println("Remaining Ops: ${unsolvedOps.size}")
                expressions["root"]!!.solveForHuman(0, expressions + solvedExpressions.mapValues { Literal(it.value) })
            }
            .toString()
    }

    sealed interface Expression {
        companion object {
            fun parse(input: String): Expression {
                return input.toLongOrNull()?.let(::Literal) ?: Op.parse(input)
            }
        }

        fun solveForHuman(goal: Long, variables: Map<String, Expression>): Long
    }

    data class Literal(val value: Long) : Expression {
        override fun solveForHuman(goal: Long, variables: Map<String, Expression>) =
            throw UnsupportedOperationException()
    }

    data class Op(val a: String, val b: String, val op: Function) : Expression {
        override fun solveForHuman(goal: Long, variables: Map<String, Expression>): Long {
            val a = variables[a]!!
            val b = variables[b]!!
            val (literal, unsolved) = when {
                a is Literal -> a to b
                b is Literal -> b to a
                else -> throw IllegalArgumentException("Can't solve with two variable parts")
            }
            return when (op) {
                Plus -> unsolved.solveForHuman(goal - literal.value, variables)
                Times -> unsolved.solveForHuman(goal / literal.value, variables)
                Minus -> when {
                    a is Literal -> unsolved.solveForHuman(a.value - goal, variables)
                    b is Literal -> unsolved.solveForHuman(goal + b.value, variables)
                    else -> throw IllegalArgumentException("Can't solve with two variable parts")
                }

                Div -> when {
                    a is Literal -> unsolved.solveForHuman(a.value / goal, variables)
                    b is Literal -> unsolved.solveForHuman(goal * b.value, variables)
                    else -> throw IllegalArgumentException("Can't solve with two variable parts")
                }
            }
        }

        companion object {
            fun parse(input: String): Op {
                val (a, op, b) = input.split(" ")
                val opFunction = when (op) {
                    "+" -> Plus
                    "-" -> Minus
                    "*" -> Times
                    "/" -> Div
                    else -> throw IllegalArgumentException()
                }
                return Op(a, b, opFunction)
            }
        }

        enum class Function(val function: (Long, Long) -> Long) {
            Plus(Long::plus), Minus(Long::minus), Times(Long::times), Div(Long::div)
        }
    }

    data class Equality(val a: String, val b: String) : Expression {
        override fun solveForHuman(goal: Long, variables: Map<String, Expression>): Long {
            val a = variables[a]!!
            val b = variables[b]!!
            return when {
                a is Literal -> b.solveForHuman(a.value, variables)
                b is Literal -> a.solveForHuman(b.value, variables)
                else -> throw IllegalArgumentException("Can't solve with two variable parts")
            }
        }
    }

    object Human : Expression {
        override fun solveForHuman(goal: Long, variables: Map<String, Expression>) = goal
    }
}
