package com.github.roschlau.adventofcode.core.solvers.y2022

import com.github.roschlau.adventofcode.core.grids.*
import com.github.roschlau.adventofcode.core.grids.Direction2D.*
import com.github.roschlau.adventofcode.core.solvers.Day
import com.github.roschlau.adventofcode.core.solvers.y2022.Day14.Cell.*

object Day14 : Day {

    override suspend fun solvePart1(input: String): String {
        val scan = Scan(Path.parse(input))
        val sandDrops = generateSequence { scan.dropSand(true) }
        return sandDrops.count()
            .also { scan.print() }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        val scan = Scan(Path.parse(input))
        val sandDrops = generateSequence { scan.dropSand(false) }
        return (sandDrops.count() + 1)
            .also { scan.print() }
            .toString()
    }

    class Scan(
        paths: Sequence<Path>,
        private val sandSource: Vector2DInt = 500 co 0,
    ) {
        private val explicitCells = SparseGrid2D<Cell>()
        private val floorLevel: Int
        init {
            for (path in paths) {
                explicitCells.addAll(path.coveredCoordinates)
            }
            check(sandSource !in explicitCells)
            explicitCells[sandSource] = Source
            floorLevel = explicitCells.yRange().last + 2
        }

        fun dropSand(ignoreFloor: Boolean): Vector2DInt? {
            var current = sandSource
            do {
                val down = current + UP.unitVector // Up because the coordinate system is flipped
                val next = sequenceOf(down, down + LEFT.unitVector, down + RIGHT.unitVector)
                    .firstOrNull { explicitCells[it] == null && (ignoreFloor || it.y != floorLevel) }
                if (next == null) {
                    if (current == sandSource) {
//                        println("Source blocked!")
                        return null
                    }
//                    println("Sand settled!")
                    explicitCells[current] = Sand
//                    print(current)
                    return current
                }
                current = next
//                printScan(explicitCells, current)
            } while (current.y <= floorLevel)
//            println("Sand fell out of scan area!")
            return null
        }

        fun print(mark: Vector2DInt? = null) {
            println(explicitCells.show(flipY = false) { vector2DInt, cell ->
                if (vector2DInt == mark) return@show 'X'
                when (cell) {
                    Rock -> '#'
                    Sand -> '.'
                    Source -> '+'
                    else -> ' '
                }
            })
        }
    }

    class Path(input: List<Vector2DInt>) {
        val coveredCoordinates = SparseGrid2D<Cell>()

        init {
            for ((a, b) in input.windowed(2)) {
                check(a.x == b.x || a.y == b.y) { "Can only draw cardinal axes" }
                val range = if (a.x == b.x) {

                    ascendingRange(a.y, b.y).map { a.x co it }
                } else {
                    ascendingRange(a.x, b.x).map { it co a.y }
                }
                for (cell in range) {
                    coveredCoordinates[cell] = Rock
                }
            }
        }

        fun ascendingRange(a: Int, b: Int): IntRange {
           return if (a <= b) a..b else b..a
        }

        companion object {
            fun parse(input: String): Sequence<Path> {
                return input.lineSequence().map { line ->
                    line.split(" -> ")
                        .map(::parseCoord)
                        .let(::Path)
                }
            }

            fun parseCoord(input: String): Vector2DInt {
                val (x, y) = input.split(",")
                return x.toInt() co y.toInt()
            }
        }
    }

    enum class Cell {
        Rock, Sand, Source
    }
}
