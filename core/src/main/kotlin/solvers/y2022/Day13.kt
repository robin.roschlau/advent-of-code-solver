package com.github.roschlau.adventofcode.core.solvers.y2022

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.IntNode
import com.github.roschlau.adventofcode.core.solvers.Day

object Day13 : Day {

    override suspend fun solvePart1(input: String): String {
        return parse(input)
            .withIndex()
            .filter { (_, pair) -> areInCorrectOrder(pair.first, pair.second) != false }
            .sumOf { (index) -> index + 1 }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        val divider1 = NestedList(listOf(NestedList(listOf(NestedListInt(2)))))
        val divider2 = NestedList(listOf(NestedList(listOf(NestedListInt(6)))))
        val packets = parse(input).flatMap { it.toList() } + divider1 + divider2
        val sortedPackets = packets
            .sortedWith(::compare)
        val div1Position = sortedPackets.indexOf(divider1) + 1
        val div2Position = sortedPackets.indexOf(divider2) + 1
        return (div1Position * div2Position)
            .toString()
    }

    fun areInCorrectOrder(left: NestedListElement, right: NestedListElement): Boolean? {
        return when (compare(left, right)) {
            0 -> null
            -1 -> true
            1 -> false
            else -> throw IllegalArgumentException()
        }
    }

    fun compare(left: NestedListElement, right: NestedListElement): Int {
        return when {
            left is NestedListInt && right is NestedListInt -> when {
                left.value < right.value -> -1
                left.value > right.value -> +1
                else -> 0
            }

            left is NestedList && right is NestedList -> {
                val byElement = left.elements.asSequence().zip(right.elements.asSequence())
                    .map { (left, right) -> compare(left, right) }
                    .firstOrNull { it != 0 }
                if (byElement != null) {
                    return byElement
                }
                return when {
                    left.elements.size < right.elements.size -> -1
                    left.elements.size > right.elements.size -> +1
                    else -> 0
                }
            }

            else -> compare(left.coerceList(), right.coerceList())
        }
    }

    fun parse(input: String): List<Pair<NestedListElement, NestedListElement>> {
        return input.split("\n\n")
            .map { pair ->
                val (left, right) = pair
                    .split("\n")
                    .map { line -> NestedListElement.parse(line) }
                Pair(left, right)
            }
    }

    sealed class NestedListElement {
        companion object {
            fun parse(input: String): NestedListElement {
                val tree: JsonNode = ObjectMapper().readTree(input)
                return parse(tree)
            }
            fun parse(input: JsonNode): NestedListElement {
                return when (input) {
                    is IntNode -> NestedListInt(input.intValue())
                    is ArrayNode -> NestedList(
                        input.elements().asSequence().map { parse(it) }.toList()
                    )
                    else -> throw IllegalArgumentException()
                }
            }
        }

        fun coerceList(): NestedList = when (this) {
            is NestedList -> this
            is NestedListInt -> NestedList(listOf(this))
        }
    }

    data class NestedList(val elements: List<NestedListElement>) : NestedListElement()
    data class NestedListInt(val value: Int) : NestedListElement()
}
