package com.github.roschlau.adventofcode.core.solvers

interface Solvers {

    /**
     * All the keys this [Solvers] instance has a [Solver] for.
     */
    val knownKeys: Set<String>

    /**
     * Find a [Solver] by its key.
     */
    fun findSolver(key: String): Solver?
}

/**
 * A Solver for a specific puzzle.
 */
interface Solver {
    suspend fun solve(input: String): Any
}

/**
 * Creates a new [Solver] from a [function]
 */
internal fun solver(function: suspend (String) -> Any) =
    object : Solver {
        override suspend fun solve(input: String) = function(input)
    }

/**
 * Builds a solver that always simply returns the puzzle input as the solution for testing purposes
 */
internal fun dummySolver(): Solver = solver { it }
