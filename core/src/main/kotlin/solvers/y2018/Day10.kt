package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.range
import com.github.roschlau.adventofcode.core.solvers.Day

internal class Day10 : Day {

    override suspend fun solvePart1(input: String): String {
        val timeline = parse(input)
        return findInterestingConstellation(timeline).second.render()
    }

    override suspend fun solvePart2(input: String): String {
        val timeline = parse(input)
        return findInterestingConstellation(timeline).first.toString()
    }

    private fun findInterestingConstellation(seconds: Sequence<List<Point>>): Pair<Int, List<Point>> {
        for ((index, points) in seconds.withIndex()) {
            if (points.isInteresting()) {
                return index to points
            }
        }
        throw IllegalStateException()
    }

    private fun List<Point>.isInteresting(): Boolean {
        val minX = maxByOrNull { it.x }?.x ?: 0
        val minY = maxByOrNull { it.y }?.y ?: 0
        return count { it.x == minX } + count { it.y == minY } > 20
    }

    private fun List<Point>.render(): String {
        val xs = map { it.x }.range()
        val ys = map { it.y }.range()
        val points = map { it.x to it.y }.toHashSet()
        return ys.joinToString("\n") { y ->
            xs.map { x ->
                if (x to y in points) '#'
                else ' '
            }.join()
        }
    }

    val pattern = Regex("""position=<\s*(-?\d+),\s*(-?\d+)> velocity=<\s*(-?\d+),\s*(-?\d+)>""")
    fun parse(input: String): Sequence<List<Point>> {
        val initialPoints = input.lineSequence()
            .mapNotNull { pattern.matchEntire(it) }
            .map { it.destructured }
            .map { (x, y, vX, vY) -> Point(x.toInt(), y.toInt(), vX.toInt(), vY.toInt()) }
            .toList()
        return generateSequence(initialPoints) { it.map { it.next } }
    }

    data class Point(
        val x: Int,
        val y: Int,
        val vX: Int,
        val vY: Int
    ) {
        val next get() = copy(x = x + vX, y = y + vY)
    }

}
