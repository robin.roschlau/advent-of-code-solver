package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.solvers.Day
import java.util.LinkedList

internal class Day9 : Day {

    override suspend fun solvePart1(input: String): String {
        val (playerCount, lastMarble) = parse(input)
        return play(playerCount, lastMarble)
    }

    override suspend fun solvePart2(input: String): String {
        val (playerCount, lastMarble) = parse(input)
        return play(playerCount, lastMarble * 100)
    }

    private fun play(playerCount: Int, lastMarble: Long): String {
        val marbles = CircularDLList(Marble(0))
        val players = LinkedList((1..playerCount).map { Player(it) })
        var currentMarble = marbles
        for (marble in (1..lastMarble).asSequence().map { Marble(it) }) {
            val currentPlayer = players.requeueFirst()
            if (marble.value % 23 == 0L) {
                currentPlayer.marbles += marble
                val toRemove = (1..7).fold(currentMarble) { acc, _ -> acc.counterClockwise }
                currentPlayer.marbles += toRemove.marble
                currentMarble = toRemove.remove()
            } else {
                currentMarble = currentMarble.clockwise.insertClockwise(marble)
            }
        }
        return players.map { it.score }.maxOrNull().toString()
    }

    private fun <E> LinkedList<E>.requeueFirst(): E =
        removeFirst().also { addLast(it) }

    class CircularDLList(
        var marble: Marble,
        clockwise: CircularDLList? = null,
        counterClockwise: CircularDLList? = null
    ) {
        var clockwise = clockwise ?: this
            private set
        var counterClockwise = counterClockwise ?: this
            private set

        fun insertClockwise(marble: Marble): CircularDLList {
            val new = CircularDLList(marble, clockwise = clockwise, counterClockwise = this)
            clockwise.counterClockwise = new
            clockwise = new
            return new
        }

        fun remove(): CircularDLList {
            clockwise.counterClockwise = counterClockwise
            counterClockwise.clockwise = clockwise
            return clockwise.also {
                counterClockwise = this
                clockwise = this
            }
        }
    }

    data class Marble(val value: Long)

    data class Player(
        val nr: Int,
        val marbles: MutableList<Marble> = mutableListOf()
    ) {
        val score get() = marbles.map { it.value }.sum()
    }

    data class Input(val players: Int, val lastMarble: Long)
    private val pattern = Regex("""(\d+) players; last marble is worth (\d+) points""")
    fun parse(input: String): Input {
        return pattern.matchEntire(input)!!.destructured
            .let { (players, lastMarble) -> Input(players.toInt(), lastMarble.toLong()) }
    }

}
