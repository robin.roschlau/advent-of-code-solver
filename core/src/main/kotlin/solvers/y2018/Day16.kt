package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.solvers.Day

class Day16 : Day {

    override suspend fun solvePart1(input: String): String {
        return parse(input).first
            .map { possibleOpcodeNames(it) }
            .count { it.size >= 3 }.toString()
    }

    override suspend fun solvePart2(input: String): String {
        val (samples, testProgram) = parse(input)
        val constraints = samples
            .map { it to possibleOpcodeNames(it) }
            .groupingBy { it.first.instruction[0] }
            .aggregate { _, accumulator: List<String>?, element, _ ->
                accumulator?.filter { it in element.second } ?: element.second
            }.toMap()
        val mapping = solveOpcodes(constraints)!!
        val device = Device()
        for (line in testProgram) {
            val (opcode, a, b, c) = line
            val op = opcodes[mapping[opcode]]!!
            device.op(a, b, c)
        }
        return device.reg[0].toString()
    }

    private fun solveOpcodes(constraints: Map<Int, List<String>>): Map<Int, String>? {
        if (constraints.isEmpty()) return mapOf()
        val next = constraints.entries.sortedBy { it.value.size }.first()
        val rest = constraints - next.key
        val (opcodeName, solvedRest) = next.value.asSequence()
            .mapNotNull { nextCandidate ->
                solveOpcodes(
                    rest.mapValues { (_, candidates) -> candidates.filter { it != nextCandidate } }
                )?.let { nextCandidate to it }
            }
            .firstOrNull()
            ?: return null
        return solvedRest + (next.key to opcodeName)
    }

    private fun possibleOpcodeNames(sample: Sample): List<String> {
        return opcodes
            .filter { (_, op) -> sample.matches(op) }
            .map { (name, _) -> name }
    }
}

private class Device(
    val reg: IntArray = IntArray(4) { 0 }
)

private class Sample(
    val before: IntArray,
    val instruction: IntArray,
    val after: IntArray
) {
    fun matches(op: Op): Boolean {
        val device = Device()
        before.copyInto(device.reg)
        val (_, a, b, c) = instruction
        device.op(a, b, c)
        return device.reg.contentEquals(after)
    }
}

private val samplePattern = Regex("""Before: \[(\d+), (\d+), (\d+), (\d+)]\s+(\d+) (\d+) (\d+) (\d+)\s+After:\s+\[(\d+), (\d+), (\d+), (\d+)]""")
private fun parse(input: String): Pair<Sequence<Sample>, Sequence<List<Int>>> {
    val samples = samplePattern.findAll(input)
        .map { it.groupValues.mapIndexed { i, group -> if(i == 0) 0 else group.toInt() } }
        .map { Sample(
            before = intArrayOf(it[1], it[2], it[3], it[4]),
            instruction = intArrayOf(it[5], it[6], it[7], it[8]),
            after = intArrayOf(it[9], it[10], it[11], it[12])
        ) }
    val testProgram = input.split("\n\n\n\n")[1].lineSequence()
        .map { line -> line.split(" ").map { it.toInt() } }
    return samples to testProgram
}

private typealias Op = Device.(a: Int, b: Int, c: Int) -> Unit

private val opcodes = mapOf<String, Op>(
    "addr" to { a, b, c -> reg[c] = reg[a] + reg[b] },
    "addi" to { a, b, c -> reg[c] = reg[a] + b },

    "mulr" to { a, b, c -> reg[c] = reg[a] * reg[b] },
    "muli" to { a, b, c -> reg[c] = reg[a] * b },

    "banr" to { a, b, c -> reg[c] = reg[a] and reg[b] },
    "bani" to { a, b, c -> reg[c] = reg[a] and b },

    "borr" to { a, b, c -> reg[c] = reg[a] or reg[b] },
    "bori" to { a, b, c -> reg[c] = reg[a] or b },

    "setr" to { a, _, c -> reg[c] = reg[a] },
    "seti" to { a, _, c -> reg[c] = a },

    "gtir" to { a, b, c -> reg[c] = if (a > reg[b]) 1 else 0 },
    "gtri" to { a, b, c -> reg[c] = if (reg[a] > b) 1 else 0 },
    "gtrr" to { a, b, c -> reg[c] = if (reg[a] > reg[b]) 1 else 0 },

    "eqir" to { a, b, c -> reg[c] = if (a == reg[b]) 1 else 0 },
    "eqri" to { a, b, c -> reg[c] = if (reg[a] == b) 1 else 0 },
    "eqrr" to { a, b, c -> reg[c] = if (reg[a] == reg[b]) 1 else 0 }
)
