package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.solvers.Day

internal class Day8 : Day {

    override suspend fun solvePart1(input: String): String =
        parse(input)
            .metadataChecksum
            .toString()

    override suspend fun solvePart2(input: String): String =
        parse(input)
            .value
            .toString()

    private fun parse(input: String) = parseNextNode(input.split(" ").map { it.toInt() })

    private fun parseNextNode(input: List<Int>): Node {
        require(input.size >= 2)
        val (childCount, metadataLength) = input
        val children = mutableListOf<Node>()
        var remainingInput = input.drop(2)
        var remainingChildren = childCount
        while (remainingChildren > 0) {
            val next = parseNextNode(remainingInput)
            children += next
            remainingInput = remainingInput.drop(next.length)
            remainingChildren -= 1
        }
        val metadata = remainingInput.take(metadataLength)
        return Node(children.toList(), metadata)
    }

    data class Node(
        val children: List<Node>,
        val metadata: List<Int>
    ) {
        val length: Int by lazy { 2 + children.sumBy { it.length } + metadata.size }
        val metadataChecksum: Int by lazy {
            metadata.sum() + children.sumBy { it.metadataChecksum }
        }
        val value: Int by lazy {
            if (children.isEmpty()) {
                metadata.sum()
            } else {
                metadata
                    .mapNotNull { children.getOrNull(it - 1) }
                    .sumBy { it.value }
            }
        }
    }

}
