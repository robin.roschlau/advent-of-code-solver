package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.grids.FixedGrid2D
import com.github.roschlau.adventofcode.core.grids.Vector2DInt
import com.github.roschlau.adventofcode.core.grids.co
import com.github.roschlau.adventofcode.core.grids.plus
import com.github.roschlau.adventofcode.core.solvers.Day

class Day17 : Day {

    val spring = 500 co 0

    val oneDown = 0 co 1
    val oneUp = 0 co -1
    val oneLeft = -1 co 0
    val oneRight = 1 co 0

    override suspend fun solvePart1(input: String): String {
        val tracedGrid = parse(input)
            .apply { traceWater(spring) }
        return tracedGrid
            .count { it.reached() }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        val tracedGrid = parse(input)
            .apply { traceWater(spring) }
        return tracedGrid
            .count { it == Square.RestingWater }
            .toString()
    }

    suspend fun FixedGrid2D<Square>.traceWater(from: Vector2DInt) {
        var current = from
        while (true) {
            if (current + oneDown !in this) {
                return
            }
            val nextDown = this[current + oneDown]
            if (nextDown == Square.DriedSand) {
                return
            }
            if (nextDown.isFree()) {
                current += oneDown
                this[current] = Square.DriedSand
            } else {
                val spreadLeft = generateSequence(current) { last ->
                    (last + oneLeft).takeIf { next -> this[next].isFree() }
                }.windowed(2).takeWhile { (prev, _) -> !this[prev + oneDown].isFree() }
                val spreadRight = generateSequence(current) { last ->
                    (last + oneRight).takeIf { next -> this[next].isFree() }
                }.windowed(2).takeWhile { (prev, _) -> !this[prev + oneDown].isFree() }
                val spread = (spreadRight + spreadLeft).map { (_, next) -> next }
                val drops = spread.filter { this[it + oneDown].isFree() }
                if (drops.count() == 0) {
                    this[current] = Square.RestingWater
                    spread.forEach { this[it] = Square.RestingWater }
                    current += oneUp
                } else {
                    this[current] = Square.DriedSand
                    spread.forEach { this[it] = Square.DriedSand }
                    drops.forEach { traceWater(it) }
                    return
                }
            }
        }
    }

    val veinPattern = Regex("""([xy])=(\d+), [yx]=(\d+)..(\d+)""")
    fun parse(input: String): FixedGrid2D<Square> {
        val clayCoords = veinPattern.findAll(input)
            .map { it.destructured }
            .flatMap { (orientation, d1, d2, d3) ->
                (d2.toInt()..d3.toInt()).asSequence()
                    .map { if (orientation == "x") d1.toInt() co it else it co d1.toInt() }
            }.toSet()
        val maxX = clayCoords.maxByOrNull { it.x }!!.x
        val maxY = clayCoords.maxByOrNull { it.y }!!.y
        val minY = clayCoords.minByOrNull { it.y }!!.y - 1
        return FixedGrid2D(maxX + 2, maxY - minY + 1) { x, y -> if (x co (y + minY) in clayCoords) Square.Clay else Square.Sand }
    }

    enum class Square {
        Sand, Clay, RestingWater, DriedSand, Tracer;

        override fun toString() = when (this) {
            Sand -> " "
            Clay -> "#"
            RestingWater -> "~"
            DriedSand -> "|"
            Tracer -> "O"
        }

        fun isFree() = when (this) {
            Sand -> true
            Clay -> false
            DriedSand -> true
            RestingWater -> false
            Tracer -> error("This shouldn't happen")
        }

        fun reached() = when (this) {
            Sand -> false
            Clay -> false
            DriedSand -> true
            RestingWater -> true
            Tracer -> error("This shouldn't happen")
        }
    }
}
