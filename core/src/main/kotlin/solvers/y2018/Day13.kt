package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.solvers.Day
import com.github.roschlau.adventofcode.core.solvers.y2018.Day13.Direction.Down
import com.github.roschlau.adventofcode.core.solvers.y2018.Day13.Direction.Left
import com.github.roschlau.adventofcode.core.solvers.y2018.Day13.Direction.Right
import com.github.roschlau.adventofcode.core.solvers.y2018.Day13.Direction.Up

class Day13 : Day {

    override suspend fun solvePart1(input: String): String {
        val (track, carts) = parse(input)
        while (true) {
            for (cart in carts.sortedBy { (it.position.second * 1000) + it.position.first }) {
                cart.move()
                cart.align(track[cart.position])
                if (carts.count { it.position == cart.position } > 1) {
                    return cart.position.let { (x, y) -> "$x,$y" }
                }
            }
        }
    }

    override suspend fun solvePart2(input: String): String {
        var (track, carts) = parse(input)
        while (true) {
            for (cart in carts.sortedBy { (it.position.second * 1000) + it.position.first }) {
                cart.move()
                cart.align(track[cart.position])
                if (carts.count { it.position == cart.position } > 1) {
                    carts = carts.filter { it.position != cart.position }.toSet()
                }
            }
            if (carts.size == 1) {
                return carts.first().position.let { (x, y) -> "$x,$y" }
            }
        }
    }

    fun parse(input: String): Pair<TrackMap, Set<Cart>> {
        val lines = input.lines()
        val charMap = Array(lines.map { it.length }.maxOrNull()!!) { y -> CharArray(lines.size) { x ->
            lines[x].getOrElse(y) { ' ' }
        } }
        val carts = charMap
            .mapIndexed { x, row -> row.mapIndexed { y, char ->
                Direction.parse(char)?.let { direction -> Cart(x to y, direction) }
            } }.flatten().filterNotNull().toSet()
        for (cart in carts) {
            charMap[cart.position] = straight(cart.direction)
        }
        return charMap to carts
    }

    private fun straight(direction: Direction) = when (direction) {
        Up, Down -> '|'
        Left, Right -> '-'
    }

    data class Cart(
        var position: Pair<Int, Int>,
        var direction: Direction,
        var nextTurn: Turn = Turn.Left
    ) {
        fun move() {
            position = when (direction) {
                Up -> position.run { copy(second = second - 1) }
                Down -> position.run { copy(second = second + 1) }
                Left -> position.run { copy(first = first - 1) }
                Right -> position.run { copy(first = first + 1) }
            }
        }

        fun align(char: Char) {
            when (char) {
                '\\' -> direction = when (direction) {
                    Up -> Left
                    Left -> Up
                    Down -> Right
                    Right -> Down
                }
                '/' -> direction = when (direction) {
                    Up -> Right
                    Right -> Up
                    Down -> Left
                    Left -> Down
                }
                '+' -> turn()
                else -> direction
            }
        }

        fun turn() {
            direction = direction.turn(nextTurn)
            nextTurn = nextTurn.next()
        }
    }

    enum class Direction { Up, Down, Left, Right;

        fun turn(turn: Turn) = when (turn) {
            Turn.Straight -> this
            Turn.Left -> when (this) {
                Up -> Left
                Left -> Down
                Down -> Right
                Right -> Up
            }
            Turn.Right -> when (this) {
                Up -> Right
                Right -> Down
                Down -> Left
                Left -> Up
            }
        }

        companion object {
            fun parse(char: Char): Direction? {
                return when (char) {
                    '^' -> Up
                    'v' -> Down
                    '>' -> Right
                    '<' -> Left
                    else -> null
                }
            }
        }
    }

    enum class Turn { Left, Straight, Right;
        fun next() = when (this) {
            Left -> Straight
            Straight -> Right
            Right -> Left
        }
    }
}

private operator fun <X, Y, V>  Map<Pair<X, Y>, V>.get(x: X, y: Y) = get(x to y)

private typealias TrackMap = Array<CharArray>
private operator fun TrackMap.get(position: Pair<Int, Int>) = this[position.first][position.second]
private operator fun TrackMap.set(position: Pair<Int, Int>, char: Char) {
    this[position.first][position.second] = char
}
