package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.combinations
import com.github.roschlau.adventofcode.core.solvers.Day

internal class Day2 : Day {

    override suspend fun solvePart1(input: String) =
        input.lines().let { ids ->
            val twins = ids.count { it.hasCharExactly(times = 2) }
            val triplets = ids.count { it.hasCharExactly(times = 3) }
            twins * triplets
        }.toString()

    override suspend fun solvePart2(input: String): String {
        val (id1, id2) = input.lines()
            .combinations()
            .first { (first, second) -> first.distanceTo(second) == 1 }
        return id1.removeDifferences(id2)
    }

}

private fun String.distanceTo(other: String) =
    zip(other).count { (first, second) -> first != second }

private fun String.removeDifferences(other: String) =
    zip(other)
        .filter { (first, second) -> first == second }
        .map { it.first }
        .join()

private fun String.hasCharExactly(times: Int) =
    groupBy { it }
        .mapValues { it.value.size }
        .filterValues { it == times }
        .isNotEmpty()

internal fun Iterable<Char>.join() = joinToString("")
