package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.solvers.Day
import kotlin.math.abs
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.yield

internal class Day5 : Day {

    override suspend fun solvePart1(input: String) =
        input.react().length.toString()

    override suspend fun solvePart2(input: String) = coroutineScope {
        // React the polymer down as far as we can, so we don't have to repeat that 26 times later
        val prereacted = input.react()
        ('a'..'z')
            .map { async { prereacted.remove(it.toString()).react().length } }
            .map { it.await() }
            .minOrNull()!!.toString()
    }

    private fun String.remove(lowercaseUnit: String) =
        replace(lowercaseUnit, "").replace(lowercaseUnit.toUpperCase(), "")

    private suspend fun String.react(): String {
        var prev: String
        var next = this
        val nextBuilder = StringBuilder()
        do {
            prev = next
            nextBuilder.clear()
            var i = 0
            while (i < prev.length) {
                if (i != prev.lastIndex && shouldReact(prev[i], prev[i + 1])) {
                    i += 2
                } else {
                    nextBuilder.append(prev[i])
                    i += 1
                }
            }
            next = nextBuilder.toString()
            yield()
        } while (prev.length != next.length)
        return next
    }

    val caseDistance = 'a' - 'A'
    fun shouldReact(c1: Char, c2: Char): Boolean {
        return abs(c1 - c2) == caseDistance
    }

}
