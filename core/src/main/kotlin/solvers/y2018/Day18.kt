package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.accumulating
import com.github.roschlau.adventofcode.core.findCycle
import com.github.roschlau.adventofcode.core.grids.FixedGrid2D
import com.github.roschlau.adventofcode.core.grids.adjacent
import com.github.roschlau.adventofcode.core.grids.co
import com.github.roschlau.adventofcode.core.solvers.Day
import com.github.roschlau.adventofcode.core.times

class Day18 : Day {

    override suspend fun solvePart1(input: String): String {
        val grid = (this::evolve * 10)(parse(input))
        return grid.resourceValue().toString()
    }

    override suspend fun solvePart2(input: String): String {
        val cycle = (1..1_000_000_000).asSequence()
            .accumulating(parse(input)) { acc, _ -> evolve(acc) }
            .findCycle()
        return cycle.elementAt(1_000_000_000).resourceValue().toString()
    }

    fun evolve(grid: FixedGrid2D<AcreState>): FixedGrid2D<AcreState> {
        return FixedGrid2D(grid.xSize, grid.ySize) { x, y ->
            grid[x co y]
                .evolve(
                    (x co y)
                        .adjacent()
                        .filter { it in grid }
                        .map(grid::get)
                )
        }
    }

    private fun FixedGrid2D<AcreState>.resourceValue(): Int =
        count { it == AcreState.Trees } * count { it == AcreState.Lumberyard }

    enum class AcreState {
        Open, Trees, Lumberyard;

        override fun toString(): String = when (this) {
            Open -> "."
            Trees -> "|"
            Lumberyard -> "#"
        }

        fun evolve(adjacent: List<AcreState>): AcreState {
            return when (this) {
                Open -> if (adjacent.count { it == Trees } >= 3) Trees else Open
                Trees -> if (adjacent.count { it == Lumberyard } >= 3) Lumberyard else Trees
                Lumberyard -> if (adjacent.contains(Lumberyard) && adjacent.contains(Trees)) Lumberyard else Open
            }
        }
    }

    fun parse(input: String): FixedGrid2D<AcreState> {
        val lines = input.lines()
        return FixedGrid2D(lines.first().length, lines.size) { x, y -> when (lines[y][x]) {
            '.' -> AcreState.Open
            '|' -> AcreState.Trees
            '#' -> AcreState.Lumberyard
            else -> throw IllegalArgumentException()
        } }
    }
}
