package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.solvers.Day

internal class Day7(
    private val workers: Int = 5,
    private val timeBoost: Int = 60
) : Day {

    override suspend fun solvePart1(input: String): String {
        val dependencies = parse(input)
            .fold(mapOf<Char, List<Char>>()) { acc, constraint ->
                val existingDependencies = acc[constraint.before] ?: listOf()
                acc + (constraint.before to (existingDependencies + constraint.step))
            }
        val remainingSteps = (dependencies.keys + dependencies.values.flatten()).toMutableSet()
        val steps = mutableListOf<Char>()
        while (remainingSteps.isNotEmpty()) {
            val next = remainingSteps
                .filter { nextCandidate -> dependencies[nextCandidate]?.all { it in steps } ?: true }
                .sorted()
                .first()
            steps += next
            remainingSteps -= next
        }
        return steps.join()
    }

    override suspend fun solvePart2(
        input: String
    ): String {
        val dependencies = parse(input)
            .fold(mapOf<Char, List<Char>>()) { acc, constraint ->
                val existingDependencies = acc[constraint.before] ?: listOf()
                acc + (constraint.before to (existingDependencies + constraint.step))
            }
        val remainingSteps = (dependencies.keys + dependencies.values.flatten())
            .map { Step(it) }
            .toMutableSet()
        val availableSteps = mutableSetOf<Step>()
        var stepsInProgress = setOf<Pair<Step, Int>>()
        val doneSteps = mutableListOf<Char>()
        var currentTime = -1
        while (listOf(remainingSteps, availableSteps, stepsInProgress).any { it.isNotEmpty() }) {
            currentTime += 1
            // Increase work done
            stepsInProgress = stepsInProgress.map { it.run { copy(second = second + 1) } }.toSet()
            // Finish steps that are done
            doneSteps += stepsInProgress.filter { it.second >= it.first.time }.map { it.first.name }
            stepsInProgress = stepsInProgress.filter { it.first.name !in doneSteps }.toSet()
            // Update available steps
            availableSteps += remainingSteps.filter { dependencies[it.name]?.all { it in doneSteps } ?: true }
            remainingSteps.removeIf { it in availableSteps }
            // Start next available steps
            while (stepsInProgress.size < workers && availableSteps.isNotEmpty()) {
                stepsInProgress += availableSteps.sortedBy { it.name }.first() to 0
                availableSteps.removeIf { it in stepsInProgress.map { it.first } }
            }
        }
        return currentTime.toString()
    }

    private val Step.time get() = name - 'A' + 1 + timeBoost

    private val pattern = Regex("""Step (.) must be finished before step (.) can begin""")
    private fun parse(input: String) = input.lineSequence()
        .map { pattern.find(it)!!.destructured }
        .map { (step, before) -> Constraint(step[0], before[0]) }

}

private data class Step(val name: Char)

private data class Constraint(val step: Char, val before: Char)
