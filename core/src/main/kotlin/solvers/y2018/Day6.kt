package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.minMax
import com.github.roschlau.adventofcode.core.solvers.Day
import java.util.Objects
import kotlin.math.abs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

internal class Day6(val part2TotalDistance: Int = 10000) : Day {

    override suspend fun solvePart1(input: String) = coroutineScope {
        parse(input).let { coords ->
            val area = relevantArea(coords)
            val mappedLocations = area.coords
                .map { coord ->
                    async(Dispatchers.Default) {
                        val closest = coord.closest(coords)
                        if (closest == coord) closest
                        else coord.copy(label = closest?.label)
                    }
                }
                .map { it.await() }
                .filter { it.label != null }
            val shrunk = area.shrink(1)
            // Disqualify the labels of all coordinates that touch the outer edge, because those have infinite area
            // (this assumption only holds using the manhattan distance!)
            val disqualified = mappedLocations.filter { it !in shrunk }.map { it.label!! }
            mappedLocations
                .filter { it.label !in disqualified }
                .groupingBy { it.label }
                .eachCount()
                .map { it.value }
                .maxOrNull()
                .toString()
        }
    }

    override suspend fun solvePart2(input: String) =
        parse(input).let { coords ->
            val area = relevantArea(coords)
            area.coords
                .map { location -> coords.map { it to it.manhattanDistanceTo(location) }.sumBy { it.second } }
                .filter { it < part2TotalDistance }
                .count()
                .toString()
        }

    private fun Coord.closest(others: Sequence<Coord>): Coord? {
        val distances = others.map { it to it.manhattanDistanceTo(this) }
        val min = distances.map { it.second }.minOrNull()!!
        if (distances.count { it.second == min } > 1) {
            return null
        }
        return distances.first { it.second == min }.first
    }

    private fun relevantArea(coords: Sequence<Coord>): Area {
        val (left, right) = coords.map { it.x }.minMax()
        val (top, bottom) = coords.map { it.y }.minMax()
        return Area(left - 1, top - 1, right + 1, bottom + 1)
    }

    private fun parse(input: String) =
        input.lineSequence()
            .zip(generateSequence('A') { it + 1 })
            .map { (line, char) ->
                line.split(", ")
                    .map(String::toInt)
                    .let { (x, y) -> Coord(x, y, char) }
            }

}

private data class Coord(val x: Int, val y: Int, val label: Char? = null) {
    fun manhattanDistanceTo(other: Coord): Int =
        abs(x - other.x) + abs(y - other.y)

    override fun equals(other: Any?): Boolean {
        if (other !is Coord) return false
        return x to y == other.x to other.y
    }

    override fun hashCode(): Int {
        return Objects.hash(x, y)
    }
}

private data class Area(val left: Int, val top: Int, val right: Int, val bottom: Int) {
    val coords by lazy { (left..right).flatMap { x -> (top..bottom).map { y -> Coord(x, y) } }.toSet() }

    fun shrink(amount: Int) = copy(
        left = left - amount,
        top = top - amount,
        right = right - amount,
        bottom = bottom - amount
    )

    operator fun contains(coord: Coord) = coord.x in (left..right) && coord.y in (top..bottom)

    fun print(coords: Iterable<Coord>) {
        val map = coords.associateBy { it.x to it.y }
        for (y in top..bottom) {
            for (x in left..right) {
                print(map[x to y]?.label ?: ".")
            }
            println()
        }
    }
}
