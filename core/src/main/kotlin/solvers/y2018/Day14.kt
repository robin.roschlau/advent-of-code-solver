package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.solvers.Day
import com.github.roschlau.adventofcode.core.solvers.y2017.parseInt
import com.github.roschlau.adventofcode.core.solvers.y2018.Day13.Direction.*
import java.util.ArrayList

class Day14 : Day {

    override suspend fun solvePart1(input: String): String {
        val target = input.toInt()
        return recipes().drop(target).take(10).joinToString("")
    }

    override suspend fun solvePart2(input: String): String {
        val target = input.toList().map { it.parseInt() }
        return recipes()
            .windowed(input.length)
            .indexOfFirst { it == target }
            .toString()
    }

    private fun recipes() = sequence {
        val scoreboard = IntArray(40_000_000)
        scoreboard[0] = 3
        scoreboard[1] = 7
        yield(3)
        yield(7)
        var firstElf = 0
        var secondElf = 1
        var next = 2
        while (true) {
            val firstRecipe = scoreboard[firstElf]
            val secondRecipe = scoreboard[secondElf]
            val sum = firstRecipe + secondRecipe
            sum.toString()
                .map { it.parseInt() }
                .forEach {
                    scoreboard[next++] += it
                    yield(it)
                }
            firstElf += 1 + firstRecipe
            firstElf %= next
            secondElf += 1 + secondRecipe
            secondElf %= next
        }
    }
}
