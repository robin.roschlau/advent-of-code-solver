package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.solvers.Day

class Day11 : Day {

    override suspend fun solvePart1(input: String): String {
        val sat = summedAreaTable(input.toInt())
        return bestSquare(sat, 3)
            .let { (square, _) -> square }
            .let { (x, y) -> "$x,$y" }
    }

    override suspend fun solvePart2(input: String): String {
        val sat = summedAreaTable(input.toInt())
        return (1..300).asSequence()
            .map { bestSquare(sat, it) }
            .maxByOrNull { (_, level) -> level }!!
            .first.let { (x, y, size) -> "$x,$y,$size" }
    }

    private fun summedAreaTable(gridSerial: Int): SummedAreaTable {
        val table = Array(301) { IntArray(301) }
        for (column in 1..300) {
            var currentRow = 0
            for (row in 1..300 ) {
                currentRow += powerLevel(column, row, gridSerial)
                table[column][row] = table[column - 1][row] + currentRow
            }
        }
        return table
    }

    private fun powerLevel(x: Int, y: Int, gridSerial: Int): Int {
        val rackId = x + 10
        var powerLevel = rackId * y
        powerLevel += gridSerial
        powerLevel *= rackId
        powerLevel /= 100
        powerLevel %= 10
        return powerLevel - 5
    }

    private fun bestSquare(sat: SummedAreaTable, size: Int): Pair<Square, Int> =
        squaresOfSize(size)
            .map { it to it.powerLevel(sat) }
            .maxByOrNull { it.second }!!

    private fun squaresOfSize(size: Int): Sequence<Square> =
        (1..301 - size).asSequence().flatMap { x -> (1..301 - size).asSequence().map { y -> Square(x, y, size) } }

    data class Square(val x: Int, val y: Int, val size: Int) {

        private val p1 = x-1 to y-1
        private val p2 = x-1 + size to y-1
        private val p3 = x-1 to y-1 + size
        private val p4 = x-1 + size to y-1 + size

        fun powerLevel(sat: SummedAreaTable): Int =
            sat[p1] + sat[p4] - sat[p2] - sat[p3]

    }

}

private typealias SummedAreaTable = Array<IntArray>
private operator fun SummedAreaTable.get(point: Pair<Int, Int>) = get(point.first).get(point.second)
