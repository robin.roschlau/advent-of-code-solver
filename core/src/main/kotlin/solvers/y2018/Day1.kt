package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.accumulating
import com.github.roschlau.adventofcode.core.firstRepeated
import com.github.roschlau.adventofcode.core.repeat
import com.github.roschlau.adventofcode.core.solvers.Day

internal class Day1 : Day {

    override suspend fun solvePart1(input: String) =
        parse(input).sum().toString()

    override suspend fun solvePart2(input: String): String =
        parse(input)
            .repeat()
            .accumulating(0, Int::plus)
            .firstRepeated()
            .toString()

    private fun parse(input: String) = input.lines().map(String::toInt)

}
