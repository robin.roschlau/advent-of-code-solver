package com.github.roschlau.adventofcode.core.solvers.y2018

import com.github.roschlau.adventofcode.core.solvers.Day
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

private typealias Minute = Int
private typealias Timeline = Sequence<Minute>

internal class Day4 : Day {

    override suspend fun solvePart1(input: String) =
        parse(input)
            .maxByOrNull { (_, timeline) -> timeline.size }!!
            .let { (guard, timeline) -> guard * mostFrequentMinute(timeline).minute }
            .toString()

    override suspend fun solvePart2(input: String) =
        parse(input)
            .mapValues { (_, timeline) -> mostFrequentMinute(timeline)  }
            .maxByOrNull { (_, maxMinute) -> maxMinute.count }!!
            .let { (guard, maxMinute) -> guard * maxMinute.minute }
            .toString()

    private fun parse(input: String): Map<Int, List<Minute>> {
        var currentGuard = -1
        return input.lines().sorted()
            .asSequence()
            .map { Event.of(it, currentGuard) }
            .onEach { if (it.type == EventType.StartShift) currentGuard = it.guard }
            .groupBy { event -> event.guard }
            .mapValues { (_, events) -> toTimeline(events).toList() }
    }

    data class MinuteCount(val minute: Minute, val count: Int)
    private fun mostFrequentMinute(timeline: List<Minute>): MinuteCount =
        timeline.groupingBy { it }.eachCount().maxByOrNull { it.value }
            ?.let { (minute, count) -> MinuteCount(minute, count) }
            ?: MinuteCount(0, 0)

    private fun toTimeline(events: List<Event>): Timeline = sequence {
        var fellAsleep: Minute? = null
        for (event in events.sortedBy { it.time }) {
            if (event.type == EventType.FallAsleep) {
                fellAsleep = event.time.minute
            }
            if (event.type == EventType.WakeUp) {
                check(fellAsleep != null)
                (fellAsleep until event.time.minute).forEach { yield(it) }
            }
        }
    }
}

private data class Event(
    val time: LocalDateTime,
    val guard: Int,
    val type: EventType
) {
    companion object {
        val pattern = Regex("""\[(.+)] (wakes up|falls asleep|Guard #(\d+) begins shift)""")
        val timeParser = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm")
        fun of(string: String, currentGuard: Int): Event {
            val matchResult = requireNotNull(pattern.matchEntire(string))
            val (time, event) = matchResult.destructured
            val eventTime = LocalDateTime.parse(time, timeParser)
            return when {
                event == "wakes up" -> Event(eventTime, currentGuard, EventType.WakeUp)
                event == "falls asleep" -> Event(eventTime, currentGuard, EventType.FallAsleep)
                event.startsWith("Guard #") -> Event(eventTime, matchResult.groupValues[3].toInt(), EventType.StartShift)
                else -> throw IllegalArgumentException()
            }
        }
    }
}

private enum class EventType { FallAsleep, WakeUp, StartShift }
