package com.github.roschlau.adventofcode.core.solvers.y2017

import com.github.roschlau.adventofcode.core.solvers.Year

internal object Year2017 : Year {
    override fun all() = listOf(
        1 to Day1(),
        2 to Day2()
    )
}
