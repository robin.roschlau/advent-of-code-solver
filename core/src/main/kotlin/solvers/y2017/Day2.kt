package com.github.roschlau.adventofcode.core.solvers.y2017

import com.github.roschlau.adventofcode.core.possiblePairings
import com.github.roschlau.adventofcode.core.solvers.Day

internal class Day2 : Day {

    override suspend fun solvePart1(input: String) =
        checksum(parse(input)).toString()

    override suspend fun solvePart2(input: String) =
        divisionChecksum(parse(input)).toString()

    private fun parse(input: String): Spreadsheet =
        input.split("\n", "\t") { it.toInt() }

    private fun checksum(spreadsheet: Spreadsheet) =
        spreadsheet.sumOf { line -> line.maxOrNull()!! - line.minOrNull()!! }

    private fun divisionChecksum(spreadsheet: Spreadsheet) = spreadsheet
        .map { line ->
            line.possiblePairings().first { it.first % it.second == 0 }
        }
        .sumOf { it.first / it.second }
}

internal fun <T> String.split(separator1: String, separator2: String, transform: (String) -> T) =
    split(separator1).map { it.split(separator2).map(transform) }

private typealias Spreadsheet = List<List<Int>>
