package com.github.roschlau.adventofcode.core.solvers.y2017

import com.github.roschlau.adventofcode.core.solvers.Day

internal class Day1 : Day {

    override suspend fun solvePart1(input: String) =
        captcha(input, 1).toString()

    override suspend fun solvePart2(input: String) =
        captcha(input, input.length / 2).toString()

    private fun captcha(input: String, shift: Int) =
        input.zip(input shift shift)
            .filter { it.first == it.second }
            .sumBy { it.first.parseInt() }
}

internal infix fun String.shift(n: Int) = when {
    // Shift right
    n > 0 -> this.takeLast(n) + this.dropLast(n)
    // Shift left
    n < 0 -> this.drop(-n) + this.take(-n)
    // No shift
    else -> this
}

internal fun Char.parseInt() = toString().toInt()
