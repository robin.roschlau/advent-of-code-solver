package com.github.roschlau.adventofcode.core.solvers.y2019

import com.github.roschlau.adventofcode.core.grids.Direction2D
import com.github.roschlau.adventofcode.core.grids.Vector2DInt
import com.github.roschlau.adventofcode.core.grids.co
import com.github.roschlau.adventofcode.core.grids.manhattanDistanceTo
import com.github.roschlau.adventofcode.core.grids.plus
import com.github.roschlau.adventofcode.core.solvers.Day

object Day3 : Day {

    override suspend fun solvePart1(input: String): String {
        val wires = input.lines().map { parse(it) }
        val intersections = findIntersections(wires[0], wires[1])
        return intersections.map { it.manhattanDistanceTo(0 co 0) }.minOrNull().toString()
    }

    override suspend fun solvePart2(input: String): String {
        val wires = input.lines().map { parse(it) }
        val intersections = findIntersections(wires[0], wires[1])
        return intersections
            .map { minDistance(it, wires) }
            .minOrNull()
            .toString()
    }

    private fun findIntersections(wire1: Wire, wire2: Wire) =
        wire1.toSet().intersect(wire2.toSet()) - (0 co 0)

    private fun minDistance(intersection: Vector2DInt, wires: List<Wire>) =
        wires.map { it.indexOf(intersection) }.sum()

    fun parse(input: String): Wire = sequence {
        val segmentStrings = input.split(',').asSequence()
        var cur = 0 co 0
        for (string in segmentStrings) {
            val direction = Direction2D.forChar(string[0])
            val length = string.drop(1).toInt()
            for (i in 1..length) {
                yield(cur)
                cur += direction.unitVector
            }
        }
    }
}

private typealias Wire = Sequence<Vector2DInt>
