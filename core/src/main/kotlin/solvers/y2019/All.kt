package com.github.roschlau.adventofcode.core.solvers.y2019

import com.github.roschlau.adventofcode.core.solvers.Year

internal object Year2019 : Year {
    override fun all() = listOf(
        1 to Day1
    )
}
