package com.github.roschlau.adventofcode.core.solvers.y2019

import com.github.roschlau.adventofcode.core.solvers.Day

object Day4 : Day {

    override suspend fun solvePart1(input: String): String {
        val range = input.split('-').let { (a, b) -> (a.toInt())..(b.toInt()) }
        return range
            .filter {
                it.isSixDigit() &&
                    it.hasADouble() &&
                    it.neverDecreasing()
            }
            .count()
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        val range = input.split('-').let { (a, b) -> (a.toInt())..(b.toInt()) }
        return range
            .filter {
                it.isSixDigit() &&
                    it.hasAnExactDouble() &&
                    it.neverDecreasing()
            }
            .count()
            .toString()
    }
}

fun Int.isSixDigit() = this in 100_000..999_999
fun Int.hasADouble() = this.toString().windowed(2).any { it[0] == it[1] }
fun Int.hasAnExactDouble(): Boolean {
    var counter = 1
    var lastChar: Char? = null
    for (char in this.toString()) {
        if (char == lastChar) {
            counter++
        }
        if (char != lastChar) {
            if (counter == 2) {
                return true
            }
            counter = 1
        }
        lastChar = char
    }
    return counter == 2
}
fun Int.neverDecreasing() = this.toString().windowed(2).all { it[0].toInt() <= it[1].toInt() }
