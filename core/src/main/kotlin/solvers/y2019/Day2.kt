package com.github.roschlau.adventofcode.core.solvers.y2019

import com.github.roschlau.adventofcode.core.possiblePairings
import com.github.roschlau.adventofcode.core.solvers.Day

object Day2 : Day {

    override suspend fun solvePart1(input: String): String {
        val program = parse(input)
        return IntcodeComputer.run(program, 12, 2).toString()
    }

    override suspend fun solvePart2(input: String): String {
        val program = parse(input)
        val start = System.currentTimeMillis()
        val (noun, verb) = (0..99).possiblePairings(includeSelf = true)
            .first { (noun, verb) -> IntcodeComputer.run(program, noun, verb) == 19690720 }
        val end = System.currentTimeMillis()
        println("Part 2 took ${end - start}ms")
        return (100 * noun + verb).toString()
    }

    fun parse(input: String) =
        input.trim().split(',').map { it.toInt() }.toIntArray()

}

object IntcodeComputer {

    fun run(program: IntArray, noun: Int, verb: Int): Int {
        require(noun in 0..99)
        require(verb in 0..99)
        val memory = program.clone()
        var instructionPointer = 0
        memory[1] = noun
        memory[2] = verb
        while (true) {
            val opcode = memory[instructionPointer]
            if (opcode == Opcode.Halt) {
                break
            }
            val params = memory.slice((instructionPointer+1)..(instructionPointer+3))
            when (opcode) {
                Opcode.Add -> {
                    val targetPointer = params[2]
                    memory[targetPointer] = memory[params[0]] + memory[params[1]]
                    instructionPointer += 4
                }
                Opcode.Multiply -> {
                    val targetPointer = params[2]
                    memory[targetPointer] = memory[params[0]] * memory[params[1]]
                    instructionPointer += 4
                }
                else -> throw IllegalArgumentException("Unkown Opcode: $opcode")
            }
        }
        return memory[0]
    }

    object Opcode {
        const val Add = 1
        const val Multiply = 2
        const val Halt = 99
    }
}
