package com.github.roschlau.adventofcode.core.solvers.y2019

import com.github.roschlau.adventofcode.core.permutations
import com.github.roschlau.adventofcode.core.repeat
import com.github.roschlau.adventofcode.core.solvers.Day
import com.github.roschlau.adventofcode.core.solvers.y2017.parseInt

object Day7 : Day {

    override suspend fun solvePart1(input: String): String {
        val program = IntcodeComputerV3.parseProgram(input)
        return (0..4).permutations()
            .map { it.map(::Amplifier) }
            .map { runAmpChain(it, program) }
            .maxOrNull()
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        TODO()
    }

    private fun runAmpChain(amps: List<Amplifier>, program: IntArray): Int {
        return amps.fold(0) { acc, amplifier -> amplifier.runProgram(program, acc) }
    }
}

class Amplifier(
    val phaseSetting: Int
) {
    init {
        require(phaseSetting in 0..4)
    }

    fun runProgram(program: IntArray, input: Int): Int {
        return IntcodeComputerV3.run(program, sequenceOf(phaseSetting, input)).last()
    }
}

object IntcodeComputerV3 {

    fun parseProgram(input: String) =
        input.trim().split(',').map { it.toInt() }.toIntArray()

    fun run(program: IntArray, inputs: Sequence<Int>): Sequence<Int> = sequence {
        val memory = program.clone()
        var instructionPointer = 0
        var ipModified: Boolean
        val input = inputs.iterator()::next
        while (true) {
            val (op, args) = parseInstruction(instructionPointer, memory)
            if (op == Op.Halt) {
                break
            }
            ipModified = false
            when (op) {
                Op.Add -> {
                    val targetPointer = args[2]
                    require(targetPointer.mode == ParameterMode.Position)
                    memory[targetPointer.value] = resolve(args[0], memory) + resolve(args[1], memory)
                }
                Op.Multiply -> {
                    val targetPointer = args[2]
                    require(targetPointer.mode == ParameterMode.Position)
                    memory[targetPointer.value] = resolve(args[0], memory) * resolve(args[1], memory)
                }
                Op.Input -> {
                    val targetPointer = args[0]
                    require(targetPointer.mode == ParameterMode.Position)
                    memory[targetPointer.value] = input()
                }
                Op.Output -> {
                    val targetPointer = args[0]
                    yield(resolve(targetPointer, memory))
                }
                Op.JumpIfTrue -> {
                    if (resolve(args[0], memory) != 0) {
                        instructionPointer = resolve(args[1], memory)
                        ipModified = true
                    }
                }
                Op.JumpIfFalse -> {
                    if (resolve(args[0], memory) == 0) {
                        instructionPointer = resolve(args[1], memory)
                        ipModified = true
                    }
                }
                Op.LessThan -> {
                    memory[args[2].value] =
                        if (resolve(args[0], memory) < resolve(args[1], memory)) {
                            1
                        } else {
                            0
                        }
                }
                Op.Equals -> {
                    memory[args[2].value] =
                        if (resolve(args[0], memory) == resolve(args[1], memory)) {
                            1
                        } else {
                            0
                        }
                }
                else -> TODO()
            }
            if (!ipModified) {
                instructionPointer += 1 + op.paramCount
            }
        }
    }

    private fun resolve(arg: Argument, memory: IntArray): Int = when (arg.mode) {
        ParameterMode.Immediate -> arg.value
        ParameterMode.Position -> memory[arg.value]
        else -> throw IllegalArgumentException("Unknown ParameterMode: ${arg.mode}")
    }

    private fun parseInstruction(ip: Int, memory: IntArray): Pair<Op, List<Argument>> {
        val opcode = memory[ip]
        val op = Op.lookup(opcode % 100) // Only last two digits contain the opcode
        val paramModes = (opcode / 100).toString().reversed().asSequence().map { it.parseInt() } + listOf(0).repeat()
        if (op == Op.Halt) {
            return op to emptyList()
        }
        val rawArgs =
            if (op.paramCount > 0) {
                memory.slice((ip + 1)..(ip + op.paramCount))
            } else {
                emptyList()
            }
        val args = rawArgs.asSequence()
            .zip(paramModes)
            .map { (rawArg, mode) -> Argument(mode = mode, value = rawArg) }
            .toList()
        return op to args
    }

    enum class Op(val paramCount: Int) {
        Add(3),
        Multiply(3),
        Input(1),
        Output(1),
        JumpIfTrue(2),
        JumpIfFalse(2),
        LessThan(3),
        Equals(3),
        Halt(0),
        ;

        companion object {
            fun lookup(opcode: Int) = when (opcode) {
                1 -> Add
                2 -> Multiply
                3 -> Input
                4 -> Output
                5 -> JumpIfTrue
                6 -> JumpIfFalse
                7 -> LessThan
                8 -> Equals
                99 -> Halt
                else -> throw IllegalArgumentException("Unknown Opcode: $opcode")
            }
        }
    }

    object ParameterMode {
        const val Position = 0
        const val Immediate = 1
    }

    class Argument(val mode: Int, val value: Int)
}
