package com.github.roschlau.adventofcode.core.solvers.y2019

import com.github.roschlau.adventofcode.core.solvers.Day
import kotlin.math.max

object Day1 : Day {

    override suspend fun solvePart1(input: String) =
        solveWith(this::part1FuelFor, input)

    override suspend fun solvePart2(input: String) =
        solveWith(this::part2FuelFor, input)

    private fun solveWith(fuelCalculator: (weight: Int) -> Int, input: String) =
        input.lines()
            .map(String::toInt)
            .sumBy(fuelCalculator)
            .toString()

    private fun part1FuelFor(weight: Int) =
        max((weight / 3) - 2, 0)

    private fun part2FuelFor(weight: Int): Int {
        val result = part1FuelFor(weight)
        if (result <= 0) return 0
        return result + part2FuelFor(result)
    }
}
