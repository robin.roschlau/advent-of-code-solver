package com.github.roschlau.adventofcode.core.solvers.y2021

import com.github.roschlau.adventofcode.core.solvers.Day

object Day1 : Day {

    override suspend fun solvePart1(input: String): String {
        return input.lineSequence()
            .map { it.toInt() }
            .zipWithNext()
            .count { (a, b) -> a < b }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        return input.lineSequence()
            .map { it.toInt() }
            .windowed(3) { it.sum() }
            .zipWithNext()
            .count { (a, b) -> a < b }
            .toString()
    }
}
