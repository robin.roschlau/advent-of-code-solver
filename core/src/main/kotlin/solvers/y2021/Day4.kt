package com.github.roschlau.adventofcode.core.solvers.y2021

import com.github.roschlau.adventofcode.core.grids.Vector2DInt
import com.github.roschlau.adventofcode.core.grids.co
import com.github.roschlau.adventofcode.core.solvers.Day

object Day4 : Day {

    override suspend fun solvePart1(input: String): String {
        val input = parse(input)
        val game = Game(input.boards)
        input.drawOrder.forEach { number ->
            val winningBoard = game.draw(number)
            if (winningBoard.isNotEmpty()) {
                println("Win at number: $number")
                println("Winning board: $winningBoard")
                return winningBoard.first().score(number).toString()
            }
        }
        throw IllegalStateException("Draw order exhausted")
    }

    override suspend fun solvePart2(input: String): String {
        val input = parse(input)
        val game = Game(input.boards)
        input.drawOrder.forEach { number ->
            val winningBoard = game.draw(number)
            if (game.numberOfBoardsStillPlaying == 0) {
                println("Last win at number: $number")
                println("Last winning board: $winningBoard")
                return winningBoard.first().score(number).toString()
            }
        }
        throw IllegalStateException("Draw order exhausted")
    }

    class Game(boards: List<Board>) {
        private val boards = boards.toMutableSet()
        val numberOfBoardsStillPlaying get() = boards.size

        fun draw(number: Int): Set<Board> {
            boards.forEach { it.cross(number) }
            val winningBoards = boards.filter { it.wins }.toSet()
            boards.removeAll(winningBoards)
            return winningBoards
        }
    }

    class Board(
        private val fields: Array<IntArray>,
    ) {
        init { check(fields.size == 5 && fields.all { it.size == 5 }) }
        private val markedFields: Array<BooleanArray> = Array(5) { BooleanArray(5) }

        val wins: Boolean
            get() {
                for (line in markedFields) {
                    if (line.all { it }) {
                        return true
                    }
                }
                for (row in 0..4) {
                    val rowValues = (0..4).map { col -> markedFields[col][row] }
                    if (rowValues.all { it }) {
                        return true
                    }
                }
                return false
            }

        fun score(winningNumber: Int): Int {
            val sumOfUnmarkedNumbers = fields.withIndex()
                .flatMap { (x, line) -> line.withIndex().map { (y, value) -> value to (x co y) } }
                .filter { (_, coord) -> markedFields[coord.x][coord.y].not() }
                .also { values -> println("Unmarked numbers: $values") }
                .sumOf { (value, _) -> value }
            println("Sum of unmarked numbers: $sumOfUnmarkedNumbers")
            return sumOfUnmarkedNumbers * winningNumber
        }

        private val numberCoordinates: Map<Int, Vector2DInt> by lazy {
            fields.withIndex().flatMap { (x, line) -> line.withIndex().map { (y, value) -> value to (x co y) } }
                .toMap()
        }

        fun cross(number: Int) {
            val coord = numberCoordinates[number]
            if (coord != null) {
                markedFields[coord.x][coord.y] = true
            }
        }
    }

    fun parse(input: String): Input {
        val drawOrder = input.lineSequence().first().split(',').map { it.toInt() }
        val boards = input.split("\n\n").drop(1)
            .map { boardString ->
                parseBoard(boardString
                    .lines()
                    .map { line -> line.trim().split(Regex("""\s+""")).map { it.toInt() } })
            }
        return Input(drawOrder, boards)
    }

    class Input(
        val drawOrder: List<Int>,
        val boards: List<Board>,
    )

    fun parseBoard(input: List<List<Int>>): Board {
        return Board(input.map { it.toIntArray() }.toTypedArray())
    }
}
