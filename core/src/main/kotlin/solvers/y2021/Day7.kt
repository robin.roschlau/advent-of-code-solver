package com.github.roschlau.adventofcode.core.solvers.y2021

import com.github.roschlau.adventofcode.core.solvers.Day
import kotlin.math.abs
import kotlin.math.roundToInt

object Day7 : Day {

    override suspend fun solvePart1(input: String): String {
        val positions = input.split(",").map { it.toInt() }
        val target = positions.median()
        return positions.sumOf { abs(it - target) }.toString()
    }

    fun List<Int>.median(): Int {
        return sorted()[size / 2]
    }

    override suspend fun solvePart2(input: String): String {
        val positions = input.split(",").map { it.toInt() }
        var target = positions.average().roundToInt()
        var cost = totalCost(positions, target)
        var below = totalCost(positions, target - 1)
        var above = totalCost(positions, target + 1)
        while (true) {
            if (cost < below && cost < above) {
                println("Aligning on $target")
                return cost.toString()
            }
            if (below < cost) {
                println("Correcting down")
                above = cost
                cost = below
                target -= 1
                below = totalCost(positions, target - 1)
            } else {
                println("Correcting up")
                below = cost
                cost = above
                target += 1
                above = totalCost(positions, target + 1)
            }
        }
    }

    fun totalCost(positions: List<Int>, target: Int): Int {
        return positions.sumOf { fuelCost(abs(it - target)) }
    }

    fun fuelCost(steps: Int): Int {
        return steps * (steps + 1) / 2
    }
}
