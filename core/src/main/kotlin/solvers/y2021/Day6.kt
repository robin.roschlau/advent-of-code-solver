package com.github.roschlau.adventofcode.core.solvers.y2021

import com.github.roschlau.adventofcode.core.solvers.Day

object Day6 : Day {

    override suspend fun solvePart1(input: String): String {
        val fish = parse(input)
        for (day in 1..80) {
            day(fish)
        }
        return fish.sum().toString()
    }

    override suspend fun solvePart2(input: String): String {
        val fish = parse(input)
        println(fish.toList())
        for (day in 1..256) {
            day(fish)
        }
        return fish.sum().toString()
    }

    fun day(fish: LongArray) {
        val copy = fish.copyOf()
        for (i in 0..7) {
            fish[i] = copy[i + 1]
        }
        fish[6] = fish[6] + copy[0]
        fish[8] = copy[0]
    }

    fun parse(input: String): LongArray {
        val fish = LongArray(9) { 0 }
        for (f in input.split(",").map { it.toInt() }) {
            fish[f] += 1L
        }
        return fish
    }
}
