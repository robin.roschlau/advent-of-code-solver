package com.github.roschlau.adventofcode.core.solvers.y2021

import com.github.roschlau.adventofcode.core.solvers.Day

object Day2 : Day {

    override suspend fun solvePart1(input: String): String {
        var hor = 0
        var depth = 0
        input.lines().forEach { line ->
            val (dim, number) = line.split(' ')
            when (dim) {
                "forward" -> hor += number.toInt()
                "down" -> depth += number.toInt()
                "up" -> depth -= number.toInt()
                else -> throw IllegalArgumentException()
            }
        }
        return (hor * depth).toString()
    }

    override suspend fun solvePart2(input: String): String {
        var hor = 0
        var depth = 0
        var aim = 0
        input.lines().forEach { line ->
            val (dim, number) = line.split(' ')
            val units = number.toInt()
            when (dim) {
                "forward" -> {
                    hor += units
                    depth += aim * units
                }
                "down" -> aim += units
                "up" -> aim -= units
                else -> throw IllegalArgumentException()
            }
        }
        return (hor * depth).toString()
    }
}
