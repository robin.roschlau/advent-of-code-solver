package com.github.roschlau.adventofcode.core.solvers.y2021

import com.github.roschlau.adventofcode.core.solvers.Year

internal object Year2021 : Year {
    override fun all() = listOf(
        1 to Day1,
    )
}
