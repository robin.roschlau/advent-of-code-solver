package com.github.roschlau.adventofcode.core.solvers.y2021

import com.github.roschlau.adventofcode.core.grids.Vector2DInt
import com.github.roschlau.adventofcode.core.grids.co
import com.github.roschlau.adventofcode.core.solvers.Day

object Day5 : Day {

    override suspend fun solvePart1(input: String): String {
        val lines = parse(input)
        val coveredPoints = mutableMapOf<Vector2DInt, Int>()
        lines
            .filter { !it.isDiagonal }
            .flatMap { line -> line.points }
            .forEach { point -> coveredPoints[point] = coveredPoints.getOrDefault(point, 0) + 1 }
//        render(coveredPoints)
        return coveredPoints.filterValues { it > 1 }.count().toString()
    }

    override suspend fun solvePart2(input: String): String {
        val lines = parse(input)
        val coveredPoints = mutableMapOf<Vector2DInt, Int>()
        lines
            .flatMap { line -> line.points }
            .forEach { point -> coveredPoints[point] = coveredPoints.getOrDefault(point, 0) + 1 }
//        render(coveredPoints)
        return coveredPoints.filterValues { it > 1 }.count().toString()
    }

    fun render(points: Map<Vector2DInt, Int>) {
        val xMax = points.keys.maxOf { it.x }
        val yMax = points.keys.maxOf { it.y }
        for (y in (0..yMax)) {
            println((0..xMax).joinToString("") { x -> points[x co y]?.toString() ?: "." })
        }
    }

    fun parse(input: String): List<Line> {
        return input.lineSequence()
            .map { line ->
                val (start, end) = line.split(" -> ").map { it.split(",").map(String::toInt).let(::Vector2DInt) }
                Line(start, end)
            }
            .toList()
    }

    class Line(
        private val start: Vector2DInt,
        private val end: Vector2DInt,
    ) {
        val isDiagonal get() = start.x != end.x && start.y != end.y
        val points: Set<Vector2DInt> by lazy {
            if (start.x == end.x) {
                return@lazy positiveRange(start.y, end.y).map { start.x co it }.toSet()
            }
            if (start.y == end.y) {
                return@lazy positiveRange(start.x, end.x).map { it co start.y }.toSet()
            }
            return@lazy positiveRange(start.x, end.x)
                .zip(positiveRange(start.y, end.y))
                .map { (x, y) -> x co y }
                .toSet()
        }

        override fun toString() = "${start.x},${start.y} -> ${end.x},${end.y}"
    }

    fun positiveRange(x: Int, y: Int) = IntProgression.fromClosedRange(x, y, if (x < y) 1 else -1)
}
