package com.github.roschlau.adventofcode.core.solvers.y2021

import com.github.roschlau.adventofcode.core.solvers.Day

object Day3 : Day {

    override suspend fun solvePart1(input: String): String {
        val lines = input.lines()
        val valueLength = lines.first().length
        val counts = IntArray(valueLength)
        for (line in lines) {
            for ((i, c) in line.withIndex()) {
                if (c == '1') {
                    counts[i] += 1
                }
            }
        }
        val majority = lines.size / 2
        val gamma = counts.joinToString("") { if (it < majority) "0" else "1" }
        val epsilon = gamma.map { if (it == '0') '1' else '0' }.joinToString("")
        return (gamma.toInt(2) * epsilon.toInt(2)).toString()
    }

    override suspend fun solvePart2(input: String): String {
        val lines = input.lines()
        // O2
        val o2Candidates = lines.toMutableSet()
        var o2Place = 0
        while (o2Candidates.size > 1) {
            val mostCommon = findMostCommon(o2Candidates, o2Place)
            o2Candidates.removeIf { it[o2Place] != mostCommon }
            o2Place += 1
        }
        val o2 = o2Candidates.first().also { println(it) }.toInt(2)
        // CO2
        val co2Candidates = lines.toMutableSet()
        var co2Place = 0
        while (co2Candidates.size > 1) {
            val leastCommon = findLeastCommon(co2Candidates, co2Place)
            co2Candidates.removeIf { it[co2Place] != leastCommon }
            co2Place += 1
        }
        val co2 = co2Candidates.first().also { println(it) }.toInt(2)
        // Result
        return (o2 * co2).toString()
    }

    private fun findMostCommon(strings: Set<String>, place: Int): Char {
        return if (strings.count { it[place] == '1' } >= strings.size / 2.0) '1' else '0'
    }

    private fun findLeastCommon(strings: Set<String>, place: Int): Char {
        return if (strings.count { it[place] == '0' } <= strings.size / 2.0) '0' else '1'
    }

}
