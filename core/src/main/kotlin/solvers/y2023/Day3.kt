package com.github.roschlau.adventofcode.core.solvers.y2023

import arrow.core.Either
import com.github.roschlau.adventofcode.core.grids.FixedGrid2D
import com.github.roschlau.adventofcode.core.grids.co
import com.github.roschlau.adventofcode.core.solvers.Day

object Day3 : Day {

    override suspend fun solvePart1(input: String): String {
        val grid = parseGrid(input)
        return grid
            .cells()
            .filter { cell -> (cell.value is Symbol) }
            .flatMap { cell -> grid.get8Neighbours(cell.coordinate).filterIsInstance<FoundNumber>() }
            .toSet()
            .sumOf { it.number }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        val grid = parseGrid(input)
        return grid
            .cells()
            .filter { cell -> (cell.value is Symbol) && cell.value.symbol == '*' }
            .mapNotNull { symbolCell ->
                grid.get8Neighbours(symbolCell.coordinate)
                    .filterIsInstance<FoundNumber>()
                    .toSet()
                    .takeIf { it.size == 2 }
                    ?.map { it.number }
                    ?.reduce { acc, t -> Math.multiplyExact(acc, t) }
            }
            .sum()
            .toString()
    }

    private fun parseGrid(input: String): FixedGrid2D<SchematicElement?> {
        val naiveGrid = FixedGrid2D.fromInput(input) { char ->
            when (char) {
                '.' -> null
                in '0'..'9' -> Either.left(char.digitToInt().toLong())
                else -> Either.right(char)
            }
        }

        val numbers = naiveGrid.coordinates()
            .groupBy({ it.x }, { it.y })
            .flatMap { (x, ys) ->
                buildList {
                    var current: FoundNumber? = null
                    for (y in ys.sorted()) {
                        val cell = naiveGrid[x, y]
                        if (cell is Either.Left) {
                            current = current?.copy(number = current.number * 10 + cell.a, endY = y)
                                ?: FoundNumber(cell.a, x, y)
                        } else {
                            if (current != null) {
                                add(current)
                                current = null
                            }
                        }
                    }
                    if (current != null) {
                        add(current)
                    }
                }
            }
        val numbersByCoordinate = numbers
            .flatMap { number ->
                (number.startY..number.endY).map { y -> (number.x co y) to number }
            }
            .toMap()
        return naiveGrid.map { value, coordinate ->
            when (value) {
                is Either.Left -> numbersByCoordinate[coordinate]!!
                is Either.Right -> Symbol(value.b)
                null -> null
            }
        }
    }

    sealed class SchematicElement
    class Symbol(val symbol: Char) : SchematicElement()
    data class FoundNumber(val number: Long, val x: Int, val startY: Int, val endY: Int = startY) : SchematicElement()
}
