package com.github.roschlau.adventofcode.core.solvers.y2023

import com.github.roschlau.adventofcode.core.solvers.Year

internal object Year2023 : Year {
    override fun all() = listOf(
        1 to Day1,
    )
}
