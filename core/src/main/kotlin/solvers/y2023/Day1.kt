package com.github.roschlau.adventofcode.core.solvers.y2023

import com.github.roschlau.adventofcode.core.findAllOverlapping
import com.github.roschlau.adventofcode.core.solvers.Day

object Day1 : Day {

    override suspend fun solvePart1(input: String): String {
        return input.lineSequence()
            .map { naiveLineValue(it) }
            .sum()
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        return input.lineSequence()
            .map { actualLineValue(it) }
            .sum()
            .toString()
    }

    private fun naiveLineValue(line: String): Int {
        val digits = line.mapNotNull { it.digitToIntOrNull() }
        return digits.first() * 10 + digits.last()
    }

    private fun actualLineValue(line: String): Int {
        val digits = matcher.findAllOverlapping(line)
            .map {
                if (it.value.length == 1) {
                    it.value.toInt()
                } else {
                    checkNotNull(digits[it.value])
                }
            }
        return digits.first() * 10 + digits.last()
    }

    val digits = mapOf(
        "one" to 1,
        "two" to 2,
        "three" to 3,
        "four" to 4,
        "five" to 5,
        "six" to 6,
        "seven" to 7,
        "eight" to 8,
        "nine" to 9,
    )

    val matcher = Regex((digits.keys + digits.values).joinToString("|", "(", ")"))
}
