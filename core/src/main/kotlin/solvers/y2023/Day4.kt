package com.github.roschlau.adventofcode.core.solvers.y2023

import com.github.roschlau.adventofcode.core.solvers.Day
import kotlin.math.pow

object Day4 : Day {

    override suspend fun solvePart1(input: String): String {
        return input.lineSequence()
            .mapIndexed { i, line -> parseCard(i, line) }
            .sumOf {
                it.score()
            }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        val cards = input.lineSequence()
            .mapIndexed { i, line -> parseCard(i, line) }
            .toList()
        val copies = LongArray(cards.size) { 1 }
        for ((i, card) in cards.withIndex()) {
            val cardCount = copies[i]
            for (copiedCard in card.copiesCards) {
                copies[copiedCard] += cardCount
            }
        }
        return copies
            .sum()
            .toString()
    }

    private fun parseCard(index: Int, input: String): Card {
        val (winning, have) = input
            .dropWhile { it != ':' }
            .drop(1)
            .dropWhile { it.isWhitespace() }
            .split(" | ")
            .map { list ->
                list.trim()
                    .replace(Regex("\\s+"), " ")
                    .split(" ")
                    .map {
                        it.trim().toInt()
                    }
            }
        if (winning.toSet().size != winning.size || have.size != have.toSet().size) {
            throw IllegalArgumentException()
        }
        return Card(index, winning.toSet(), have.toSet())
    }

    class Card(
        index: Int,
        winning: Set<Int>,
        have: Set<Int>,
    ) {
        private val winningNumbers = winning.intersect(have).size
        val copiesCards = (index + 1)..(index + winningNumbers)
        fun score(): Long {
            val numbers = winningNumbers
            if (numbers == 0) return 0
            return 1 * 2.0.pow(numbers - 1.0).toLong()
        }
    }
}
