package com.github.roschlau.adventofcode.core.solvers.y2023

import com.github.roschlau.adventofcode.core.solvers.Day

object Day2 : Day {

    override suspend fun solvePart1(input: String): String {
        return input.lineSequence()
            .map { parseGame(it) }
            .filter { checkPossibleWithBag(it, part1Bag) }
            .sumOf { it.id }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        return input.lineSequence()
            .map { parseGame(it) }
            .map { minimumCubeSet(it) }
            .sumOf { powerOf(it) }
            .toString()
    }

    class Game(val id: Int, val cubeSets: List<Map<String, Int>>)

    private fun parseGame(line: String): Game {
        val id = line.removePrefix("Game ").takeWhile { it.isDigit() }.toInt()
        val cubeSetsString = line.split(": ")[1]
        val cubeSets = cubeSetsString.split("; ")
            .map { cubeSetString ->
                cubeSetString.split(", ")
                    .associate {
                        val (numberString, color) = it.split(" ")
                        color to numberString.toInt()
                    }
            }
        return Game(id, cubeSets)
    }

    private fun checkPossibleWithBag(game: Game, bag: Map<String, Int>): Boolean {
        return game.cubeSets.all { cubeSet ->
            cubeSet.all { (color, count) -> (bag[color] ?: 0) >= count }
        }
    }

    private fun minimumCubeSet(game: Game): Map<String, Int> {
        return game.cubeSets.reduce { acc, next ->
            (acc.keys + next.keys)
                .associateWith { key -> maxOf(acc[key] ?: 0, next[key] ?: 0) }
        }
    }

    private fun powerOf(cubeSet: Map<String, Int>): Long {
        return (cubeSet["red"]?.toLong() ?: 0L) * (cubeSet["green"] ?: 0) * (cubeSet["blue"] ?: 0)
    }

    private val part1Bag = mapOf(
        "red" to 12,
        "green" to 13,
        "blue" to 14,
    )
}
