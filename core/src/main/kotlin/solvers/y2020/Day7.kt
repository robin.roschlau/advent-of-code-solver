package com.github.roschlau.adventofcode.core.solvers.y2020

import com.github.roschlau.adventofcode.core.solvers.Day

internal val Day7 = Day(
    parseInput = { input ->
        input.lines().map { Rule.parse(it) }
    },
    part1 = { input ->
        val searchColors = mutableSetOf("shiny gold")
        var prevSize = 0
        while (prevSize != searchColors.size) {
            // This approach is really not optimal, since it goes through all rules and matches every search color every
            // time, even the ones already seen. It's fast enough though ¯\_(ツ)_/¯
            prevSize = searchColors.size
            searchColors += input
                .filter { rule -> rule.children.any { (_, color) -> color in searchColors } }
                .map { it.containerColor }
        }
        searchColors.size - 1 // Excluding the original search color
    },
    part2 = { input ->
        val rules = input.associate { it.containerColor to it.children }
        var nestedBagCount = 0
        var newBags = listOf("shiny gold")
        while (newBags.isNotEmpty()) {
            newBags = newBags.flatMap { rules[it]!!.flatMap { (amount, color) -> List(amount) { color } } }
            nestedBagCount += newBags.size
        }
        nestedBagCount // Again, excluding outermost bag
    },
)

typealias BagColor = String

data class Rule(val containerColor: BagColor, val children: List<Pair<Int, BagColor>>) {
    companion object {
        fun parse(input: String): Rule {
            val (containerColor, childRules) = input.split(" bags contain ")
            if (childRules == "no other bags.") {
                return Rule(containerColor, emptyList())
            }
            val children = childRules.split(", ").map {
                val (_, amount, color) = checkNotNull(childMatcher.matchEntire(it)?.groupValues) { "Could not match $it" }
                amount.toInt() to color
            }
            return Rule(containerColor, children)
        }

        private val childMatcher = Regex("""(\d+?) (.+?) bags?\.?""")
    }
}
