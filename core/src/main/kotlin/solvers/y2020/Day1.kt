package com.github.roschlau.adventofcode.core.solvers.y2020

import com.github.roschlau.adventofcode.core.solvers.Day

object Day1 : Day {

    override suspend fun solvePart1(input: String): String {
        val numbers = input
            .lines()
            .map { it.toInt() }
            .withIndex()
        for ((i, a) in numbers) {
            for ((j, b) in numbers) {
                if (i >= j) {
                    continue
                }
                if (a + b == 2020) {
                    return (a * b).toString()
                }
            }
        }
        throw IllegalArgumentException("No solution found")
    }

    override suspend fun solvePart2(input: String): String {
        val numbers = input
            .lines()
            .map { it.toInt().also { parsed -> check(parsed > 0) { "Optimization invariant broken, $parsed is <= 0" } } }
            .withIndex()
        for ((i, a) in numbers) {
            for ((j, b) in numbers) {
                if (i >= j || a + b >= 2020) {
                    // Skip over any combination of i & j that is already larger than our target in sum
                    continue
                }
                for ((k, c) in numbers) {
                    if (j >= k) {
                        continue
                    }
                    if (a + b + c == 2020) {
                        return (a * b * c).toString()
                    }
                }
            }
        }
        throw IllegalArgumentException("No solution found")
    }
}
