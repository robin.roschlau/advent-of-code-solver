package com.github.roschlau.adventofcode.core.solvers.y2020

import com.github.roschlau.adventofcode.core.solvers.Year

internal object Year2020 : Year {
    override fun all() = listOf(
        1 to Day1,
        2 to Day2,
        3 to Day3,
        4 to Day4,
        5 to Day5,
        6 to Day6,
    )
}
