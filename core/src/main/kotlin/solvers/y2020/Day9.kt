package com.github.roschlau.adventofcode.core.solvers.y2020

import com.github.roschlau.adventofcode.core.solvers.Day

private class Input(val preambleLength: Int, val numbers: List<Long>)

internal val Day9 = Day(
    parseInput = { input ->
        val lines = input.lines()
        Input(lines.first().toInt(), lines.drop(1).map { it.toLong() })
    },
    part1 = ::part1,
    part2 = { input ->
        val target = part1(input)
        val candidates = mutableListOf<Long>()
        for (number in input.numbers) {
            candidates += number
            while (candidates.sum() > target) {
                candidates.removeAt(0)
            }
            if (candidates.sum() == target) {
                return@Day candidates.minOrNull()!! + candidates.maxOrNull()!!
            }
        }
    },
)

private fun part1(input: Input) =
    input.numbers
        .windowed(input.preambleLength + 1)
        .first {
            val number = it.last()
            val candidates = it.take(input.preambleLength).withIndex()
            outer@ for ((i, a) in candidates) {
                inner@ for ((j, b) in candidates) {
                    if (j <= i) {
                        continue@inner
                    }
                    if (a + b == number) {
                        return@first false
                    }
                }
            }
            true
        }
        .last()

