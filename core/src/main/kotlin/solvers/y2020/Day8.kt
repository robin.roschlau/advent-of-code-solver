package com.github.roschlau.adventofcode.core.solvers.y2020

import com.github.roschlau.adventofcode.core.solvers.Day

internal val Day8 = Day(
    parseInput = { input ->
        input.lines()
            .map {
                val (op, argument) = it.split(" ")
                Instruction(op, argument.toInt())
            }
    },
    part1 = { input ->
        VM.runCode(input).also { check(it.looped) }.lastAcc
    },
    part2 = { input ->
        for (i in input.indices) {
            if (input[i].operation == "acc") {
                continue
            }
            val result = VM.runCode(input, i)
            if (!result.looped) {
                println("Flipping instruction $i (${input[i]}) was successful")
                return@Day result.lastAcc
            }
        }
    },
)

object VM {
    fun runCode(program: List<Instruction>, flipInstruction: Int = -1): Result {
        var accumulator = 0
        var ip = 0
        val seen = mutableSetOf<Int>()
        while (ip <= program.lastIndex) {
            if (ip in seen) {
                return Result(looped = true, accumulator)
            }
            seen += ip
            val ins = program[ip]
            val op = when {
                ip != flipInstruction -> ins.operation
                ins.operation == "acc" -> "acc"
                ins.operation == "nop" -> "jmp"
                ins.operation == "jmp" -> "nop"
                else -> throw IllegalArgumentException("Unknown operation: ${ins.operation}")
            }
            when (op) {
                "nop" -> { /* Noop */ }
                "acc" -> { accumulator += ins.argument }
                "jmp" -> { ip += ins.argument - 1 }
                else -> throw IllegalArgumentException("Unknown operation: ${ins.operation}")
            }
            ip += 1
        }
        return Result(looped = false, accumulator)
    }
}

class Result(val looped: Boolean, val lastAcc: Int)

class Instruction(
    val operation: String,
    val argument: Int,
) {
    override fun toString() = "$operation $argument"
}
