package com.github.roschlau.adventofcode.core.solvers.y2020

import com.github.roschlau.adventofcode.core.solvers.Day

object Day2 : Day {

    override suspend fun solvePart1(input: String): String {
        return input.lineSequence()
            .map(PasswordEntry::parse)
            .count(PasswordEntry::isValidA)
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        return input.lineSequence()
            .map(PasswordEntry::parse)
            .count(PasswordEntry::isValidB)
            .toString()
    }
}

private data class PasswordEntry(val char: Char, val a: Int, val b: Int, val password: String) {

    fun isValidA() =
        password.count { it == char } in a..b

    fun isValidB() =
        (password[a - 1] == char) xor (password[b - 1] == char)

    companion object {
        val pattern = Regex("""^(\d+)-(\d+) (\w): (.+)$""")
        fun parse(input: String): PasswordEntry {
            val match = requireNotNull(pattern.matchEntire(input))
            val (_, start, end, char, pw) = match.groupValues
            return PasswordEntry(char.first(), start.toInt(), end.toInt(), pw)
        }
    }
}
