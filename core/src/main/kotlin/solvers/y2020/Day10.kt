package com.github.roschlau.adventofcode.core.solvers.y2020

import com.github.roschlau.adventofcode.core.solvers.Day

internal val Day10 = Day(
    parseInput = { input ->
        val chargers = input.lines()
            .map { it.toInt() }
        (chargers + 0 + (chargers.maxOrNull()!! + 3))
            .sorted()
    },
    part1 = { input ->
        val differences = input
            .zipWithNext { a, b -> b - a }
            .groupBy { it }
            .mapValues { it.value.size }
        differences.getValue(1) * differences.getValue(3)
    },
    part2 = { input ->
        val adapters = mutableListOf(Adapter(0, 1))
        for (joltage in input.drop(1)) {
            val leadToBy = adapters.filter { it.joltage in (joltage - 3) until joltage }
            adapters += Adapter(
                joltage,
                leadToBy.map { it.combinationsLeadingToIt }.sum()
            )
        }
        adapters.last().combinationsLeadingToIt
    },
)

data class Adapter(
    val joltage: Int,
    val combinationsLeadingToIt: Long,
)
