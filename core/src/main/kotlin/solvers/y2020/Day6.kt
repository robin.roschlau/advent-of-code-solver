package com.github.roschlau.adventofcode.core.solvers.y2020

import com.github.roschlau.adventofcode.core.solvers.Day

internal val Day6 = Day(
    parseInput = { input ->
        input
            .split("\n\n")
            .map { group ->
                group.split('\n').map { it.toSet() }
            }
    },
    part1 = { groups ->
        groups
            .map { it.reduce(Set<Char>::union).size }
            .sum()
    },
    part2 = { groups ->
        groups
            .map { it.reduce(Set<Char>::intersect).size }
            .sum()
    },
)
