package com.github.roschlau.adventofcode.core.solvers.y2020

import com.github.roschlau.adventofcode.core.solvers.Day

object Day4 : Day {

    override suspend fun solvePart1(input: String): String {
        return input.split("\n\n")
            .map { parsePassport(it) }
            .count { checkPassportValid(it, false) }
            .toString()
    }

    override suspend fun solvePart2(input: String): String {
        return input.split("\n\n")
            .map { parsePassport(it) }
            .count { checkPassportValid(it, true) }
            .toString()
    }

    fun checkPassportValid(passport: PassportData, checkFormat: Boolean): Boolean {
        return expectedFields.all { (name, regex) ->
            if (checkFormat) {
                val value = passport[name]
                value != null && regex.matches(value)
            } else {
                name in passport.keys
            }
        }
    }

    val expectedFields = mapOf(
        "byr" to Regex("""^(19[2-9]\d|200[0-2])$"""),
        "iyr" to Regex("""^20(1\d|20)$"""),
        "eyr" to Regex("""^20(2\d|30)$"""),
        "hgt" to Regex("""^(1([5-8]\d|9[0-3])cm|(59|6\d|7[0-6])in)$"""),
        "hcl" to Regex("""^#[0-9a-f]{6}$"""),
        "ecl" to Regex("""(amb|blu|brn|gry|grn|hzl|oth)"""),
        "pid" to Regex("""^\d{9}$"""),
    )

    fun parsePassport(input: String): PassportData = input
            .split(Regex("\\s+"))
            .map { it.split(":") }
            .associate { (field, value) -> field to value }
}

private typealias PassportData = Map<String, String>
