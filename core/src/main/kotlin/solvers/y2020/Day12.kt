package com.github.roschlau.adventofcode.core.solvers.y2020

import com.github.roschlau.adventofcode.core.grids.Vector2DInt
import com.github.roschlau.adventofcode.core.grids.co
import com.github.roschlau.adventofcode.core.grids.manhattanDistanceTo
import com.github.roschlau.adventofcode.core.grids.plus
import com.github.roschlau.adventofcode.core.grids.times
import com.github.roschlau.adventofcode.core.solvers.Day

internal val Day12 = Day(
    parseInput = { input ->
        input.lines().map { it.first() to it.drop(1).toInt() }
    },
    part1 = { input ->
        var pos = 0 co 0
        var direction = 'E'
        for (instruction in input) {
            when (instruction.first) {
                'E', 'W', 'N', 'S' -> pos += (instruction.first.asVector() * instruction.second)
                'R' -> repeat(instruction.second / 90) { direction = direction.right() }
                'L' -> repeat(instruction.second / 90) { direction = direction.left() }
                'F' -> pos += (direction.asVector() * instruction.second)
            }
        }
        pos.manhattanDistanceTo(0 co 0)
    },
    part2 = { input ->
        var waypoint = 10 co 1
        var pos = 0 co 0
        for (instruction in input) {
            when (instruction.first) {
                'E', 'W', 'N', 'S' -> waypoint += (instruction.first.asVector() * instruction.second)
                'R' -> repeat(instruction.second / 90) { waypoint = waypoint.rotateRight() }
                'L' -> repeat(instruction.second / 90) { waypoint = waypoint.rotateLeft() }
                'F' -> repeat(instruction.second) { pos += waypoint }
            }
        }
        pos.manhattanDistanceTo(0 co 0)
    },
)

private fun Char.right() = when (this) {
    'E' -> 'S'
    'S' -> 'W'
    'W' -> 'N'
    'N' -> 'E'
    else -> throw IllegalArgumentException()
}

private fun Char.left() = when (this) {
    'S' -> 'E'
    'W' -> 'S'
    'N' -> 'W'
    'E' -> 'N'
    else -> throw IllegalArgumentException()
}

private fun Vector2DInt.rotateRight(): Vector2DInt {
    return this.copy(x = this.y, y = -this.x)
}

private fun Vector2DInt.rotateLeft(): Vector2DInt {
    return this.copy(x = -this.y, y = this.x)
}

private fun Char.asVector() = when (this) {
    'E' -> (1 co 0)
    'W' -> (-1 co 0)
    'N' -> (0 co 1)
    'S' -> (0 co -1)
    else -> throw IllegalArgumentException()
}
