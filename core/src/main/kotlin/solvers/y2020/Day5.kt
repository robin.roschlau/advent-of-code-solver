package com.github.roschlau.adventofcode.core.solvers.y2020

import com.github.roschlau.adventofcode.core.solvers.Day

object Day5 : Day {

    override suspend fun solvePart1(input: String): Any {
        return parse(input).maxOrNull()!!
    }

    override suspend fun solvePart2(input: String): Any {
        return parse(input)
            .sorted()
            .zipWithNext()
            .first { (a, b) -> a + 2 == b }
            .let { (a, _) -> a + 1 }
    }

    fun parse(input: String): List<Int> = input
        .lines()
        .map { getPassId(it) }

    fun getPassId(pass: String): Int = pass
        .replace(Regex("[BR]"), "1")
        .replace(Regex("[FL]"), "0")
        .toInt(2)

}
