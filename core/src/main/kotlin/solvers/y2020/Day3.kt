package com.github.roschlau.adventofcode.core.solvers.y2020

import com.github.roschlau.adventofcode.core.solvers.Day
import com.github.roschlau.adventofcode.core.solvers.y2020.Day3.Map.Cell.BelowMap
import com.github.roschlau.adventofcode.core.solvers.y2020.Day3.Map.Cell.Empty
import com.github.roschlau.adventofcode.core.solvers.y2020.Day3.Map.Cell.Tree

object Day3 : Day {

    override suspend fun solvePart1(input: String): String {
        return checkSlope(Map(input), 3, 1).toString()
    }

    override suspend fun solvePart2(input: String): String {
        val slopes = listOf(
            1 to 1,
            3 to 1,
            5 to 1,
            7 to 1,
            1 to 2,
        )
        val map = Map(input)
        return slopes.map { (right, down) -> checkSlope(map, right, down) }
            .reduce { acc, i -> acc * i }
            .toString()
    }

    private fun checkSlope(map: Map, right: Int, down: Int): Int {
        var x = 0
        var y = 0
        var trees = 0
        while (true) {
            when (map[x, y]) {
                BelowMap -> return trees
                Tree -> trees += 1
                Empty -> {}
            }
            x += right
            y += down
        }
    }

    class Map(input: String) {
        init { require(input.isNotBlank()) }
        private val baseTreePositions = input.lines().map { line -> line.map { char -> char == '#' } }

        operator fun get(x: Int, y: Int): Cell {
            require(y >= 0)
            if (y > baseTreePositions.lastIndex) return BelowMap
            val line = baseTreePositions[y]
            val cell = line[x % line.size]
            return if (cell) Tree else Empty
        }

        enum class Cell { Empty, Tree, BelowMap }
    }
}
