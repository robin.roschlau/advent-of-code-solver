package com.github.roschlau.adventofcode.core.solvers.y2020

import com.github.roschlau.adventofcode.core.grids.FixedGrid2D
import com.github.roschlau.adventofcode.core.grids.Vector2DInt
import com.github.roschlau.adventofcode.core.grids.co
import com.github.roschlau.adventofcode.core.solvers.Day
import com.github.roschlau.adventofcode.core.solvers.y2020.Seat.*

internal val Day11 = Day(
    parseInput = { input ->
        input.lines()
        FixedGrid2D.fromInput(input) { when (it) {
            '.' -> null
            'L' -> Empty
            else -> throw IllegalArgumentException()
        } }
    },
    part1 = { inputGrid ->
        simulateSeating(inputGrid, { grid, x, y -> grid.get8Neighbours(x, y).filterNotNull() }, 4)
    },
    part2 = { inputGrid ->
        simulateSeating(inputGrid, FixedGrid2D<Seat?>::getVisible, 5)
    },
)

private fun simulateSeating(
    grid: FixedGrid2D<Seat?>,
    getRelevantSeats: (grid: FixedGrid2D<Seat?>, x: Int, y: Int) -> List<Seat>,
    minOccupiedToMove: Int,
): Int {
    val changes = mutableListOf<Pair<Vector2DInt, Seat>>()
    do {
        changes.clear()
        for (x in 0 until grid.xSize) {
            for (y in 0 until grid.ySize) {
                val cell = grid[x, y] ?: continue
                val relevantSeats = getRelevantSeats(grid, x, y)
                if (cell == Empty && relevantSeats.none { it == Occupied }) {
                    changes += x co y to Occupied
                }
                if (cell == Occupied && relevantSeats.count { it == Occupied } >= minOccupiedToMove) {
                    changes += x co y to Empty
                }
            }
        }
        changes.forEach { (coordinate, newState) ->
            grid[coordinate] = newState
        }
    } while (changes.isNotEmpty())
    return grid.count { it == Occupied }
}

private enum class Seat { Empty, Occupied }

private fun FixedGrid2D<Seat?>.getVisible(x: Int, y: Int): List<Seat> {
    val neighbors = mutableListOf<Seat>()
    for (vx in (-1)..(+1)) {
        cell@for (vy in (-1)..(+1)) {
            if (vx == 0 && vy == 0) continue
            var c = x co y
            while (true) {
                c = (c.x + vx) co (c.y + vy)
                if (c.x < 0 || c.y < 0 || c.x >= this.xSize || c.y >= this.ySize) {
                    continue@cell
                }
                val currentCell = this[c]
                if (currentCell != null) {
                    neighbors += currentCell
                    continue@cell
                }
            }
        }
    }
    return neighbors
}
