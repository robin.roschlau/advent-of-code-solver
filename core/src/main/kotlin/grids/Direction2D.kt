package com.github.roschlau.adventofcode.core.grids

enum class Direction2D(
    val unitVector: Vector2DInt
) {
    UP(0 co 1),
    RIGHT(1 co 0),
    DOWN(0 co -1),
    LEFT(-1 co 0),
    ;

    companion object {
        /**
         * Parses a direction from a character. Character has to match &#91;URDL].
         */
        fun forChar(char: Char) = when (char) {
            'U' -> UP
            'R' -> RIGHT
            'D' -> DOWN
            'L' -> LEFT
            else -> throw IllegalArgumentException("Unknown direction: $char")
        }
    }
}
