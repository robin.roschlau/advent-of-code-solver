package com.github.roschlau.adventofcode.core.grids

import kotlin.math.max
import kotlin.math.min

/**
 * A mutable data structure saving values of type [T] in a two-dimensional grid of fixed size. The grid is internally
 * backed by a nested array.
 */
class FixedGrid2D<T>(
    private val grid: Array<Array<T>>
) {

    class Cell<T>(val coordinate: Vector2DInt, val value: T)

    companion object {
        /**
         * Creates a `FixedGrid2D<T>` from the given [input] string, with each character in the input being converted
         * into a cell value in the grid after having the [transformChars] function applied to it.
         * Lines in the input run along the y-axis of the resulting grid.
         */
        inline fun <reified T> fromInput(input: String, transformChars: (Char) -> T): FixedGrid2D<T> {
            val grid = input.lines().map { it.map(transformChars).toTypedArray() }.toTypedArray()
            val ySize = grid.first().size
            require(grid.all { it.size == ySize })
            return FixedGrid2D(grid)
        }

        inline operator fun <reified T> invoke(xSize: Int, ySize: Int, init: (x: Int, y: Int) -> T) =
            FixedGrid2D(Array(xSize) { x -> Array(ySize) { y -> init(x, y) } })
    }

    val xSize: Int = grid.size
    val ySize: Int = grid.first().size
        .also { ySize -> require(grid.all { it.size == ySize }) }

    fun coordinates(): List<Vector2DInt> {
        return (0 until xSize).flatMap { x -> (0 until ySize).map { y -> Vector2DInt(x, y) } }
    }

    fun cells() = coordinates().map { Cell(it, this[it]) }

    operator fun get(x: Int, y: Int): T = grid[x][y]
    operator fun get(coordinate: Vector2DInt): T = get(coordinate.x, coordinate.y)

    operator fun set(x: Int, y: Int, t: T) { grid[x][y] = t }
    operator fun set(coordinate: Vector2DInt, t: T) = set(coordinate.x, coordinate.y, t)

    fun get8Neighbours(x: Int, y: Int): List<T> {
        val neighbors = mutableListOf<T>()
        for (nx in (x-1)..(x+1)) {
            if (nx < 0 || nx > xSize - 1) continue
            for (ny in (y-1)..(y+1)) {
                if (ny < 0 || ny > ySize - 1) continue
                if (nx == x && ny == y) continue
                neighbors += this[nx,ny]
            }
        }
        return neighbors
    }
    fun get8Neighbours(coordinate: Vector2DInt): List<T> = get8Neighbours(coordinate.x, coordinate.y)

    fun count(predicate: (T) -> Boolean): Int {
        var count = 0
        for (col in grid) for (cell in col) if (predicate(cell)) count += 1
        return count
    }

    operator fun contains(coordinate: Vector2DInt): Boolean =
        coordinate.x in (0 until xSize) && coordinate.y in (0 until ySize)

    /**
     * Creates a new grid by applying [transform] to every cell in this grid. This function does not mutate this grid.
     */
    inline fun <reified R> map(transform: (value: T, coordinate: Vector2DInt) -> R): FixedGrid2D<R> {
        val newGrid = Array(xSize) { x -> Array(ySize) { y -> transform(this[x, y], Vector2DInt(x, y)) } }
        return FixedGrid2D(newGrid)
    }

    override fun toString(): String =
        (0 until ySize).joinToString("\n") { y -> (0 until xSize).map { x -> this[x, y] }.joinToString("") }

    fun toString(rectA: Vector2DInt, rectB: Vector2DInt, markPoint: Pair<Vector2DInt, T>? = null): String {
        require(rectA in this)
        require(rectB in this)
        val ys = min(rectA.y, rectB.y) .. max(rectA.y, rectB.y)
        val xs = min(rectA.x, rectB.x) .. max(rectA.x, rectB.x)
        val prev = markPoint?.let { (coord, marker) -> this[coord].also { this[coord] = marker } }
        val result = ys.joinToString("\n") { y -> xs.map { x -> this[x, y] }.joinToString("") }
        markPoint?.let { (coord, _) -> this[coord] = prev as T }
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as FixedGrid2D<*>
        return grid.contentDeepEquals(other.grid)
    }

    override fun hashCode(): Int {
        return grid.contentDeepHashCode()
    }
}
