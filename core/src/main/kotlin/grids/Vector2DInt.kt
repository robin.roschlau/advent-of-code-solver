package com.github.roschlau.adventofcode.core.grids

import kotlin.math.abs

/**
 * A Coordinate in 2D space with dimensions being called [x] and [y] and measured in integer steps.
 */
data class Vector2DInt(val x: Int, val y: Int) {
    constructor(values: List<Int>) : this(values[0], values[1]) {
        require(values.size == 2)
    }
    override fun toString() = "($x, $y)"
}

/**
 * Convenience function for creating a new [Vector2DInt]
 */
infix fun Int.co(y: Int) = Vector2DInt(this, y)

/**
 * Returns a list of the 8 coordinates immediately surrounding this one.
 */
fun Vector2DInt.adjacent() =
    (-1..1).flatMap { x -> (-1..1).map { y -> x co y } }
        .filterNot { it.x == 0 && it.y == 0 }
        .map { this + it }

/**
 * Adds [other] to this [Vector2DInt].
 */
operator fun Vector2DInt.plus(other: Vector2DInt): Vector2DInt =
    copy(x = x + other.x, y = y + other.y)
/**
 * Subtracts [other] from this [Vector2DInt].
 */
operator fun Vector2DInt.minus(other: Vector2DInt): Vector2DInt =
    copy(x = x - other.x, y = y - other.y)

operator fun Vector2DInt.times(int: Int): Vector2DInt =
    copy(x = x * int, y = y * int)

fun Vector2DInt.manhattanDistanceTo(other: Vector2DInt): Int =
    abs(x - other.x) + abs(y - other.y)
