package com.github.roschlau.adventofcode.core.grids

import java.util.*

private typealias Coordinate = Vector2DInt

/**
 * A mutable data structure saving values of type [T] in a two-dimensional grid of unbounded size. The grid is
 * internally backed by a [Map]<[Coordinate], [T]>.
 */
class SparseGrid2D<T : Any> {
    private val grid = mutableMapOf<Coordinate, T>()
    private val xs = TreeSet<Int>()
    private val ys = TreeSet<Int>()

    /**
     * Sets the cell at [coordinate] to [element]. Will overwrite the current element at [coordinate], if any.
     */
    operator fun set(coordinate: Coordinate, element: T) {
        grid[coordinate] = element
        xs += coordinate.x
        ys += coordinate.y
    }

    /**
     * @return The value at the specified [coordinate] in the grid, or null if there is no element at that coordinate.
     */
    operator fun get(coordinate: Coordinate): T? {
        return grid[coordinate]
    }

    /**
     * Adds the [other] grid to this one, overwriting any already existing cells in this grid.
     * The [other] grid stays unchanged.
     */
    fun addAll(other: SparseGrid2D<T>) {
        for (cell in other.grid) {
            set(cell.key, cell.value)
        }
    }

    /**
     * Returns the total number of filled cells in this grid.
     */
    fun count(): Int =
        grid.size

    /**
     * Checks if there is a value assigned to the [coordinate] in the grid.
     *
     * @return `true` if a value is assigned to the [coordinate], `false` otherwise.
     */
    operator fun contains(coordinate: Coordinate): Boolean =
        coordinate in grid

    /**
     * Shows this grid as a String with rows corresponding to lines and columns corresponding to character columns
     * The [showCell] parameter allows calling code to determine how to render each cell in the grid.
     */
    fun show(flipY: Boolean = true, showCell: (Coordinate, T?) -> Char) = buildString {
        for (y in yRange().let { if (flipY) it.reversed() else it }) {
            for (x in xRange()) {
                append(showCell(x co y, grid[x co y]))
            }
            appendLine()
        }
    }

    fun xRange() = xs.first()..xs.last()
    fun yRange() = ys.first()..ys.last()
}

fun Iterable<Coordinate>.print() {
    val grid = SparseGrid2D<Unit>()
    for (c in this) {
        grid[c] = Unit
    }
    println(grid.show { _, exists -> if (exists != null) '#' else '.' })
}
