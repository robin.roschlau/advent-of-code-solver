# Module "cli"

## Module description
This module provides a CLI interface to the Advent of Code solvers.
They can be called like this:

```bash
# List all known solver keyss:
aoc ls

# Solve a puzzle:
# aoc solve <year> <day> <part> <input> 
aoc solve 2017 1 1 1122
# "3"

# Check a puzzle solution:
# aoc check <year> <day> <part> <solution> <input>
aoc check 2017 1 1 3 1122
# "Correct Solution!"
```

Both the `check` and `solve` commands also allow omitting the `<input>` argument and reading the puzzle input from stdin instead:

```bash
echo "1122" | aoc solve 2017 1 1
# "3"

# Check a puzzle solution:
# aoc check <year> <day> <part> <solution> <input>
echo "1122" | aoc check 2017 1 1 3
# "Correct Solution!"
```

## Running the tests
`./gradlew :cli:check`

## Building and trying the CLI in your terminal
`./gradlew :cli:installDist`
This will install the cli into the folder `<project-root>/cli/build/install/aoc`.
You can run it from there by executing one of the files in `<project-root>/cli/build/install/aoc/bin/`.
