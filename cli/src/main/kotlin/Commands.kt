package com.github.roschlau.adventofcode.cli

import com.github.ajalt.clikt.core.CliktError
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.defaultLazy
import com.github.roschlau.adventofcode.core.solvers.AdventOfCode
import com.github.roschlau.adventofcode.core.solvers.AocPuzzlePart
import com.github.roschlau.adventofcode.core.solvers.Solvers
import kotlinx.coroutines.runBlocking
import java.io.InputStream
import java.io.PrintStream


class ListKeys(
    private val solvers: Solvers,
    override val stdIn: InputStream,
    override val stdOut: PrintStream
) : TestableCommand(name = "ls", help = "List all known puzzle keys") {

    override fun run() =
        solvers.knownKeys
            .sorted()
            .forEach(::println)
}

class Solve(
    private val solvers: Solvers,
    override val stdIn: InputStream,
    override val stdOut: PrintStream
) : PuzzleCommand(help="Solve a puzzle") {
    private val input by argument(help = "Your puzzle input. If not given as a cmd argument, you can instead supply it via stdin, for example by piping a file via `cat`.")
        .defaultLazy { readStdinUntilEOF() }

    override fun run() = runBlocking {
        val solution = solvers.solve(year, day, part, input)
        println(solution)
    }
}

class Check(
    private val solvers: Solvers,
    override val stdIn: InputStream,
    override val stdOut: PrintStream
) : PuzzleCommand(help="Check a proposed solution to a puzzle, without revealing the correct solution") {
    private val proposedSolution by argument()
    private val input by argument(help = "Your puzzle input. If not given as a cmd argument, you can instead supply it via stdin, for example by piping a file via `cat`.")
        .defaultLazy { readStdinUntilEOF() }

    override fun run() = runBlocking {
        val solution = solvers.solve(year, day, part, input)
        println(if (proposedSolution == solution) "Correct solution!" else "Wrong solution!")
    }
}

private suspend fun Solvers.solve(year: Int, day: Int, aocPuzzlePart: AocPuzzlePart, input: String): Any {
    return findSolver(AdventOfCode.buildKey(year, day, aocPuzzlePart))
        ?.solve(input)
        ?: throw CliktError("There is no solver for this puzzle yet!")
}
