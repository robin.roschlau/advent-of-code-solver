package com.github.roschlau.adventofcode.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.transformAll
import com.github.ajalt.clikt.parameters.types.int
import com.github.ajalt.clikt.parameters.types.restrictTo
import com.github.roschlau.adventofcode.core.solvers.AocPuzzlePart
import java.io.InputStream
import java.io.PrintStream

/**
 * Wrapper class around [CliktCommand] that provides custom [TestableCommand.println] and [TestableCommand.readLine]
 * functions that use the [stdIn] and [stdOut] streams determined by child classes, to make testing command interaction
 * easier.
 */
abstract class TestableCommand(
    help: String,
    name: String? = null
) : CliktCommand(help = help, name = name) {

    protected abstract val stdOut: PrintStream
    protected abstract val stdIn: InputStream
    private val inReader by lazy { stdIn.bufferedReader() }

    protected fun println(message: Any?) {
        stdOut.println(message)
    }

    protected fun readLine(): String? = inReader.readLine()

    /**
     * Reads from [inReader] until it encounters an EOF. May not return if [inReader] never reads an EOF.
     */
    protected fun readStdinUntilEOF(): String =
        generateSequence { readLine() }
            .joinToString("\n")
}

/**
 * Represents a command that operates on a specific AoC puzzle, identified by the parameters year, day and part passed
 * as command line arguments.
 */
abstract class PuzzleCommand(help: String) : TestableCommand(help) {
    protected val year: Int by argument().int().restrictTo(2015..2018)
    protected val day: Int by argument().int().restrictTo(1..31)
    protected val part: AocPuzzlePart by argument().int().restrictTo(1..2)
        .transformAll { (part) -> AocPuzzlePart.fromInt(part) ?: throw IllegalStateException() }
}
