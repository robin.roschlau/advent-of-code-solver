package com.github.roschlau.adventofcode.cli

import com.github.ajalt.clikt.core.NoRunCliktCommand
import com.github.ajalt.clikt.core.subcommands
import com.github.roschlau.adventofcode.core.coreModule
import java.io.InputStream
import java.io.PrintStream
import org.koin.core.context.startKoin
import org.koin.core.logger.EmptyLogger
import org.koin.core.qualifier.StringQualifier
import org.koin.dsl.module
import org.koin.experimental.builder.single

fun main(args: Array<String>) {
    val koin = startKoin() {
        modules(listOf(coreModule, cliModule))
        logger(EmptyLogger())
    }.koin
    NoRunCliktCommand(name = "aoc", help = "Command line interface to solve Advent of Code Puzzles")
        .subcommands(
            koin.get<ListKeys>(),
            koin.get<Solve>(),
            koin.get<Check>()
        )
        .main(args)
}

val cliModule = module {
    single<InputStream>(StringQualifier("stdin")) { System.`in` }
    single<PrintStream>(StringQualifier("stdout")) { System.out }
    single<ListKeys>()
    single<Solve>()
    single<Check>()
}
