package com.github.roschlau.adventofcode.cli

import com.github.roschlau.adventofcode.core.solvers.Solver
import com.github.roschlau.adventofcode.core.solvers.Solvers
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test

class SolveTest {

    val constantSolver = mockk<Solver> {
        coEvery { solve("Input") } returns "Solution"
    }

    val solvers = mockk<Solvers> {
        every { findSolver("AOC-2018-1-1") } returns constantSolver
    }

    /**
     * Checks that the CLI correctly solves a puzzle when called like `solve <year> <day> <part> <input>`
     */
    @Test
    fun `solves puzzles with input from cmd arg`() {
        val out = StringCapturePrintStream()
        val cmd = Solve(solvers, mockk(), out)
        cmd.parse(listOf("2018", "1",  "1", "Input"))
        out.assertLines("Solution")
    }

    /**
     * Checks that the CLI reads the puzzle input from stdin when called like `solve <year> <day> <part>`
     */
    @Test
    fun `solves puzzles with input from stdin`() {
        val input = "Input".byteInputStream()
        val out = StringCapturePrintStream()
        val cmd = Solve(solvers, input, out)
        cmd.parse(listOf("2018", "1",  "1"))
        out.assertLines("Solution")
    }

}
