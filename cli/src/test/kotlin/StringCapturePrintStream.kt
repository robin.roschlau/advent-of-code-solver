package com.github.roschlau.adventofcode.cli

import org.junit.jupiter.api.Assertions.assertEquals
import java.io.ByteArrayOutputStream
import java.io.OutputStream
import java.io.PrintStream

/**
 * A simple [PrintStream] that captures everything printed to it and provides assertion functions on it.
 * Can be used as a drop-in replacement for [System.out] in tests to check the program output.
 */
class StringCapturePrintStream(
    private val stream: OutputStream = ByteArrayOutputStream()
) : PrintStream(stream) {

    val captured get() = stream.toString()

    /**
     * Asserts that all [lines] have been printed, in order, to this Stream.
     */
    fun assertLines(vararg lines: String) = assertLines(lines.toList())

    /**
     * Asserts that all [lines] have been printed, in order, to this Stream.
     */
    fun assertLines(lines: List<String>) {
        assertEquals(
            lines.joinToString(separator = System.lineSeparator(), postfix = System.lineSeparator()),
            captured
        )
    }
}
