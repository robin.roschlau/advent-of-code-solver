package com.github.roschlau.adventofcode.cli

import com.github.roschlau.adventofcode.core.solvers.Solvers
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test

class ListKeysTest {

    val solvers = mockk<Solvers> {
        every { knownKeys } returns setOf("Key 1", "Key 2")
    }

    /**
     * Checks that the CLI will list all keys known to the underlying solvers, each on a separate line.
     */
    @Test
    fun `ls lists all known keys`() {
        val out = StringCapturePrintStream()
        val cmdUnderTest = ListKeys(solvers, mockk(), out)
        cmdUnderTest.parse(listOf())
        out.assertLines(solvers.knownKeys.sorted())
    }

}
