package com.github.roschlau.adventofcode.cli

import com.github.roschlau.adventofcode.core.solvers.Solver
import com.github.roschlau.adventofcode.core.solvers.Solvers
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test

class CheckTest {

    val constantSolver = mockk<Solver> {
        coEvery { solve("Input") } returns "Solution"
    }

    val solvers = mockk<Solvers> {
        every { findSolver("AOC-2018-1-1") } returns constantSolver
    }

    /**
     * Checks that the CLI correctly verifies a proposed solution when called like
     * `check <year> <day> <part> <solution> <input>`
     */
    @Test
    fun `checks correct solution with input from cmd arg`() {
        val out = StringCapturePrintStream()
        val cmd = Check(solvers, mockk(), out)
        cmd.parse(listOf("2018", "1",  "1", "Solution", "Input"))
        out.assertLines("Correct solution!")
    }

    /**
     * Checks that the CLI correctly verifies a proposed solution and reads the puzzle input from stdin when called like
     * `check <year> <day> <part> <solution>`
     */
    @Test
    fun `checks correct solution with input from stdin`() {
        val input = "Input".byteInputStream()
        val out = StringCapturePrintStream()
        val cmd = Check(solvers, input, out)
        cmd.parse(listOf("2018", "1", "1", "Solution"))
        out.assertLines("Correct solution!")
    }

    /**
     * Checks that the CLI correctly falsifies a proposed solution when called like
     * `check <year> <day> <part> <solution> <input>`
     */
    @Test
    fun `checks wrong solution with input from cmd arg`() {
        val out = StringCapturePrintStream()
        val cmd = Check(solvers, mockk(), out)
        cmd.parse(listOf("2018", "1",  "1", "Solution, but wrong", "Input"))
        out.assertLines("Wrong solution!")
    }

    /**
     * Checks that the CLI correctly falsifies a proposed solution and reads the puzzle input from stdin when called like
     * `check <year> <day> <part> <solution>`
     */
    @Test
    fun `checks wrong solution with input from stdin`() {
        val input = "Input".byteInputStream()
        val out = StringCapturePrintStream()
        val cmd = Check(solvers, input, out)
        cmd.parse(listOf("2018", "1", "1", "Solution, but wrong"))
        out.assertLines("Wrong solution!")
    }

}
