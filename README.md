# Advent of Code Solvers
This project was created to serve as an example for a hands-on demonstration of Kotlin that's a little closer to real use cases than a "Hello World"-Example.

The project is meant to demonstrate Kotlin-specific approaches to common software problems like DI, Web interfaces, DB access or CLIs,
and to make the setting interesting I chose to build it around the brilliant [Advent of Code](https://adventofcode.com/) puzzles.

## Branches
The `master` branch contains the most recent, full implementation of the project.

There are several additional branches that contain versions of this project that are stripped of some implementation code and have failing tests.
These branches serve as entry points for participants in the hands-on, to try and implement the missing functionality to satisfy the tests.
Each branch represents a specific set of tasks for one topic at a specific level of difficulty. Check out the branch you
want to start with, and the README-files on that branch will point you the way to the tasks.

And finally, there is the `aoc` branch, which is the whole project without the Solver implementations,
for when you just want to use the project as a starting point to go through the Advent of Code yourself.

### Topics
There are 4 topics to explore in different difficulties (see below for an explanation of the difficulty levels).

#### Core
These tasks are in the core module, and are mostly pure Kotlin.
Perfect if you want to get a feel for the core functionality or if you are not particularly interested in the libraries
and instead want to work on your Kotlin skills.

[Difficulty 1](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/core-1/core) | [Difficulty 2](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/core-2/core) | [Difficulty 3](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/core-3/core) | [Difficulty 4](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/difficulty-4/core)

[Difficulty 3 - Coroutines](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/core-3-coroutines/core) - This one is an additional branch dealing exclusively with the task of making the Solver coroutine-ready.

#### Web API
These tasks are in the web-api module. They give you a little glimpse into how webservices built with [Ktor](https://ktor.io) look like.

[Difficulty 1](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/web-1/web-api) | [Difficulty 2](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/web-2/web-api) | [Difficulty 3](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/web-3/web-api) | [Difficulty 4](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/difficulty-4/web-api)

#### Database
These tasks are also in the web-api module, but the webservice functionality is complete, and the task instead show you
how to work with [Exposed](https://github.com/JetBrains/Exposed) as an SQL DSL.
Little remark here, while the [KDocs](https://kotlinlang.org/docs/reference/kotlin-doc.html#kdoc-syntax) for the other
libraries used here is generally pretty good, the KDocs for Exposed are very sparse to nonexistent, so it might be
helpful to pull up [the examples](https://github.com/JetBrains/Exposed#sql-dsl-sample) in a webbrowser while working on
these tasks.

[Difficulty 1](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/db-1/web-api) | [Difficulty 2](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/db-2/web-api) | [Difficulty 3](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/db-3/web-api) | [Difficulty 4](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/difficulty-4/web-api)

#### Command Line Interface
These tasks are in the cli module, and show you how to build command line interfaces with Kotlin using [CliKt](https://ajalt.github.io/clikt/) (spoken like "clicked").

[Difficulty 1](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/cli-1/cli) | [Difficulty 2](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/cli-2/cli) | [Difficulty 3](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/cli-3/cli) | [Difficulty 4](https://gitlab.com/robin.roschlau/advent-of-code-solver/tree/difficulty-4/cli)

### Levels of difficulty
- difficulty-1: "Don't mind me, just looking for now!"
  For beginners that have very little experience with Kotlin. There are lots of comments to point the participant in the right direction.
- difficulty-2: "So, what's this all about?"
  For people who have dabbled with Kotlin a little already, but have yet to approach bigger projects with it.
  Tasks are a little harder, fewer comments to hold your hand.
- difficulty-3: "Give me a challenge!" is for people who are already quite confident in Kotlin and want to explore the used libraries in a little more depth.
- difficulty-4: "I own this place!" For Kotlin veterans that enjoy a challenge.
  These are tasks I would love to do at some point, but haven't gotten around to yet myself (or have no idea how to do - yup, I'm shamelessly exploiting you), so no solution-branch for you!
  If you're already at this level though, it might be more rewarding for you to instead pair up with someone with less experience and mentor them through the lower difficulties. Your choice!

Don't think too hard about this, if a difficulty turns out to be too easy or to hard for you, nothing wrong with switching to another one :) 

## Module structure
There are three modules: core, cli, web-api.
(If you have ideas for more frontends, you're very welcome to suggest or even build them yourself!)
This section gives a very short overview, have a look at their individual READMEs for more detailled information. 

**core** contains the puzzle solvers and logic to look them up. This is mostly just plain Kotlin.
To allow the other modules to inject solvers it exposes a [Koin module](https://insert-koin.io/) at `com.github.roschlau.adventofcode.core.coreModule`

**cli** uses the library [CliKt](https://ajalt.github.io/clikt/) to build a command line interface for solving puzzles and checking solutions.

**web-api** uses [Ktor](https://ktor.io) (Web server) and [Exposed](https://github.com/JetBrains/Exposed) (Database access) to build a Web-API for solving puzzles and checking solutions. 

## Running the tests
`./gradlew clean check` from the root directory will do a clean build of the project and run all tests on it.
On master, this might take a little while, because by default it includes all tests for the individual AoC puzzles that are implemented so far.
On my dev machine it's around half a minute for the clean build and another half minute for the tests right now, the latter figure rising with every additional puzzle solved.
