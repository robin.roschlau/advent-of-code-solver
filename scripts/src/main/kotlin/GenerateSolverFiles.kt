import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.request.cookie
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsText
import io.ktor.http.HttpStatusCode
import io.ktor.http.isSuccess
import io.ktor.http.userAgent
import kotlinx.coroutines.runBlocking
import org.intellij.lang.annotations.Language
import java.nio.file.Files
import java.nio.file.Path
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import kotlin.io.path.exists
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.nameWithoutExtension

/**
 * The time zone in which the puzzles will unlock at midnight. See https://adventofcode.com/2023/about#faq_unlocktime
 */
val AoCTimeZone: ZoneOffset = ZoneOffset.ofHours(-5)

fun main() {
    val today = ZonedDateTime.now(AoCTimeZone)
    val year = chooseYear(today)
    if (year == null) {
        println("You've already created files for all puzzles in the Advent of Code. Congratulations!")
        return
    }

    val day = chooseDay(year, today)
        ?: return
    val input = if (ZonedDateTime.of(year, 12, day, 0, 0, 0, 0, ZoneId.of("-5")) > today) {
        print("The puzzle input for this day is not available yet. The test file will not contain your puzzle input. Continue? (Y/n) ")
        if (readln() != "Y") {
            return
        }
        ""
    } else {
        runBlocking { getInput(year, day) }
    }
    if (input == null) {
        return
    }
    createDaySolutionFile(year, day)
    createDayTestFile(year, day, input)
}

private val projectRoot = Path.of(System.getProperty("user.dir"))
private val solversRoot = projectRoot.resolve("core/src/main/kotlin/solvers")

private fun chooseYear(today: ZonedDateTime): Int? {
    val defaultYear = mostRecentYearWithMissingDays(today)
        ?: return null
    print("Enter the year ($defaultYear): ")
    return readln()
        .takeIf { it.isNotBlank() }
        ?.toInt()
        ?: defaultYear
}

private fun chooseDay(year: Int, today: ZonedDateTime): Int? {
    val nextMissingDay = nextMissingDayOfYear(year)
    if (nextMissingDay == null) {
        println("You've already created files for all puzzles in the Advent of Code of $year.")
        return null
    }
    val defaultDay = if (
        year == today.year &&
        today.isAoCDay() &&
        !dayExists(year, today.dayOfMonth)
    ) {
        today.dayOfMonth
    } else {
        nextMissingDay
    }
    if (nextMissingDay <= 25) {
        println("The first missing day in that year is day $nextMissingDay.")
    }
    print("Enter the day ($defaultDay): ")
    val day = readln().takeIf { it.isNotBlank() }?.toInt() ?: defaultDay
    if (dayExists(year, day)) {
        println("A solution file for this day already exists.")
        return null
    }
    return day
}

private fun createDaySolutionFile(year: Int, day: Int) {
    Files.writeString(solversRoot.resolve("y$year").resolve("Day$day.kt"), dayContent(year, day))
}

private fun createDayTestFile(year: Int, day: Int, input: String) {
    val testsRoot = Path.of(System.getProperty("user.dir"), "core/src/test/kotlin/solvers/y$year")
    Files.writeString(
        testsRoot.resolve("Day${day}Test.kt"),
        dayTestContent(year, day, input),
    )
}

private fun mostRecentYearWithMissingDays(today: ZonedDateTime): Int? {
    val mostRecentStarted = if (today.monthValue == 12) today.year else today.year - 1
    return (mostRecentStarted downTo 2015)
        .firstOrNull { year -> nextMissingDayOfYear(year) != null }
}

private fun nextMissingDayOfYear(year: Int): Int? {
    val yearRoot = solversRoot.resolve("y$year")
    val nextDay = (1..26).firstOrNull { !yearRoot.resolve("Day$it.kt").exists() }
        ?: 1
    if (nextDay >= 26) {
        return null
    }
    return nextDay
}

private fun dayExists(year: Int, dayOfMonth: Int): Boolean {
    val yearRoot = solversRoot.resolve("y$year")
    return yearRoot.resolve("Day$dayOfMonth.kt").exists()
}

private fun ZonedDateTime.isAoCDay() = monthValue == 12 && dayOfMonth <= 25

private val client = HttpClient(CIO)
private suspend fun getInput(year: Int, day: Int): String? {
    return try {
        val response = client.get("https://adventofcode.com/$year/day/$day/input") {
            userAgent("robin@roschlau.me")
            cookie("session", loadToken())
        }
        if (!response.status.isSuccess()) {
            throw RequestFailed(response.status, response.bodyAsText())
        }
        response.bodyAsText()
    } catch (e: Exception) {
        e.printStackTrace(System.out)
        print("Your input could not be downloaded (see stack trace above). The test file will not contain your puzzle input. Continue? (Y/retry/n) ")
        when (readln()) {
            "Y" -> ""
            "retry" -> getInput(year, day)
            else -> null
        }
    }
}

class RequestFailed(code: HttpStatusCode, msg: String) : Exception("$code: $msg")

private fun loadToken(): String {
    val tokenFile = projectRoot.resolve("scripts/secrets/.session_token")
    if (!tokenFile.exists()) {
        print("Please enter your session token: ")
        val token = readln()
        Files.writeString(tokenFile, token)
    }
    return Files.readString(tokenFile).trim()
}

@Language("kotlin")
private fun dayContent(year: Int, day: Int) = """
    package com.github.roschlau.adventofcode.core.solvers.y$year

    import com.github.roschlau.adventofcode.core.solvers.Day

    object Day$day : Day {

        override suspend fun solvePart1(input: String): String {
            return input.lineSequence()
                .map { TODO() }
                .toString()
        }

        override suspend fun solvePart2(input: String): String {
            return input.lineSequence()
                .map { TODO() }
                .toString()
        }
    }
""".trimIndent()

@Language("kotlin")
private fun dayTestContent(year: Int, day: Int, input: String) = """
    package com.github.roschlau.adventofcode.core.solvers.y$year

    import com.github.roschlau.adventofcode.core.args
    import kotlinx.coroutines.runBlocking
    import org.junit.jupiter.api.Assertions
    import org.junit.jupiter.params.ParameterizedTest
    import org.junit.jupiter.params.provider.MethodSource
    
    class Day${day}Test {
    
        @ParameterizedTest(name = "{0} -> {1}")
        @MethodSource("part1Args")
        fun `part 1`(input: String, solution: String) = runBlocking {
            Assertions.assertEquals(solution, Day$day.solvePart1(input))
        }
    
        @ParameterizedTest(name = "{0} -> {1}")
        @MethodSource("part2Args")
        fun `part 2`(input: String, solution: String) = runBlocking {
            Assertions.assertEquals(solution, Day$day.solvePart2(input))
        }
    
        @Suppress("unused")
        companion object {
            @JvmStatic
            fun part1Args() = args(
                testInput to "XXX",
                input to "XXX",
            )
    
            @JvmStatic
            fun part2Args() = args(
                testInput to "XXX",
                input to "XXX",
            )
        }
    }
    
    private val testInput = ""${'"'}
        
    ""${'"'}.trimIndent()
    
    private val input = ""${'"'}
${input.trimEnd().prependIndent("        ")}
    ""${'"'}.trimIndent()
""".trimIndent()
