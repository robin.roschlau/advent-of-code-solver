package com.github.roschlau.adventofcode.web

import arrow.core.Either
import arrow.core.identity
import com.github.roschlau.adventofcode.core.solvers.AdventOfCode
import com.github.roschlau.adventofcode.core.solvers.AocPuzzlePart
import com.github.roschlau.adventofcode.core.solvers.Solvers
import com.github.roschlau.adventofcode.web.caching.MemCachedSolvers
import com.github.roschlau.adventofcode.web.extensions.exit
import com.github.roschlau.adventofcode.web.protocol.RequestProtocols
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.locations.Locations
import io.ktor.locations.get
import io.ktor.locations.post
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.util.pipeline.PipelineContext


@OptIn(KtorExperimentalLocationsAPI::class)
@Location("/{year}/{day}/{part}")
data class Puzzle(val year: Int, val day: Int, val part: Int) {

    /** For when the puzzle input is short and can be passed via url */
    @Location("/solve/{input}")
    data class GetSolve(val puzzle: Puzzle, val input: String)

    /** For when you don't want to encode the puzzle input into the url and instead just pass it in the body */
    @Location("/solve")
    data class Solve(val puzzle: Puzzle)

    @Location("/check")
    data class Check(val puzzle: Puzzle)
}

data class CheckRequest(val input: String, val proposedSolution: String)

@KtorExperimentalLocationsAPI
fun Application.installRoutes(
    solvers: MemCachedSolvers,
    requestProtocols: RequestProtocols
) {
    install(Locations)
    routing {
        get("/keys") {
            call.respond(solvers.knownKeys)
        }

        get<Puzzle.GetSolve> { (puzzle, input) ->
            call.respond(solve(solvers, puzzle, input))
        }

        post<Puzzle.Solve> { (puzzle) ->
            val input = call.receive<String>()
            call.respond(solve(solvers, puzzle, input))
        }

        post<Puzzle.Check> { (puzzle) ->
            val (input, proposedResult) = call.receive<CheckRequest>()
            val result = solve(solvers, puzzle, input)
            if (result == proposedResult) {
                call.respondText("Correct solution")
            } else {
                call.respondText("Wrong solution")
            }
        }

        get("/log") {
            val log = requestProtocols.getAll()
            call.respond(log)
        }

        get("/health") {
            call.respondText("Hello there")
        }
    }
}

private suspend fun PipelineContext<Unit, ApplicationCall>.solve(
    solvers: Solvers,
    puzzle: Puzzle,
    input: String
): String {
    val key = puzzle.toAocKey()
        .unwrap { error -> exit(HttpStatusCode.BadRequest) { error } }
    return solvers.findSolver(key)
        ?.solve(input)
        ?.toString()
        ?: exit(HttpStatusCode.NotFound)
}

private fun Puzzle.toAocKey(): Either<String, String> {
    return part.toAocPuzzlePart()
        .map { part -> AdventOfCode.buildKey(year, day, part) }
}

private fun Int.toAocPuzzlePart() =
    AocPuzzlePart.fromInt(this)
        ?.let { Either.right(it) }
        ?: Either.left("Part has to be either 1 or 2")

private fun <L, R> Either<L, R>.unwrap(left: (L) -> R): R =
    fold(left, ::identity)
