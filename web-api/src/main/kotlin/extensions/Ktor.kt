package com.github.roschlau.adventofcode.web.extensions

import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.util.pipeline.PipelineContext

@Suppress("unused") // Receiver is only used to restrict function visibility
fun PipelineContext<Unit, ApplicationCall>.exit(code: HttpStatusCode): Nothing =
    throw HttpStatusException(code)

@Suppress("unused") // Receiver is only used to restrict function visibility
fun PipelineContext<Unit, ApplicationCall>.exit(code: HttpStatusCode, message: () -> String): Nothing =
    throw HttpStatusException(code, message())

open class HttpStatusException(
    val code: HttpStatusCode,
    val statusText: String = code.description,
    cause: Throwable? = null
) : Exception(statusText, cause)
