package com.github.roschlau.adventofcode.web.extensions

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.module.SimpleModule
import io.ktor.features.ContentNegotiation
import io.ktor.jackson.jackson

inline fun ContentNegotiation.Configuration.jacksonModule(crossinline configureModule: SimpleModule.() -> Unit) = jackson {
    module(configureModule)
}

inline fun ObjectMapper.module(crossinline configureModule: SimpleModule.() -> Unit) = apply {
    registerModule(SimpleModule().apply(configureModule))
}

inline fun <reified T : Any> SimpleModule.deserialize(
    crossinline deserializer: (parser: JsonParser, ctx: DeserializationContext) -> T
) {
    addDeserializer(T::class.java, object : JsonDeserializer<T>() {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext): T {
            return deserializer(p, ctxt)
        }
    })
}


inline fun <reified T : Any> SimpleModule.serialize(
    crossinline serializer: (value: T, jsonGenerator: JsonGenerator, serializers: SerializerProvider) -> Unit
) {
    addSerializer(T::class.java, object : JsonSerializer<T>() {
        override fun serialize(value: T, gen: JsonGenerator, serializers: SerializerProvider) {
            serializer(value, gen, serializers)
        }
    })
}
