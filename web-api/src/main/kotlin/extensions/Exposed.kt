package com.github.roschlau.adventofcode.web.extensions

import io.ktor.http.HttpStatusCode
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ColumnType
import org.jetbrains.exposed.sql.Table

/**
 * A Column to store [HttpStatusCode]s.
 */
fun Table.httpStatus(name: String): Column<HttpStatusCode> =
    registerColumn(name, HttpStatusCodeColumnType())

/**
 * A custom [ColumnType] that stores [HttpStatusCode]s as integers.
 */
class HttpStatusCodeColumnType : ColumnType() {

    override fun sqlType(): String = "SMALLINT"

    override fun valueFromDB(value: Any): HttpStatusCode = when (value) {
        is Int -> HttpStatusCode.fromValue(value)
        is Number -> HttpStatusCode.fromValue(value.toInt())
        else -> error("$value of ${value::class.qualifiedName} cannot be converted to a valid status code")
    }

    override fun notNullValueToDB(value: Any): Int = when (value) {
        is HttpStatusCode -> value.value
        else -> error("$value of ${value::class.qualifiedName} is not a ${HttpStatusCode::class.simpleName}")
    }
}
