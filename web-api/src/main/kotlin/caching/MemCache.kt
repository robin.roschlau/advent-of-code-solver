package com.github.roschlau.adventofcode.web.caching

import com.github.roschlau.adventofcode.core.solvers.Solver
import com.github.roschlau.adventofcode.core.solvers.Solvers

class MemCachedSolvers(private val resolver: Solvers) : Solvers {

    override val knownKeys
        get() = resolver.knownKeys

    private val cache: MutableMap<String, Solver?> = mutableMapOf()

    override fun findSolver(key: String): Solver? {
        return cache.computeIfAbsent(key) { resolver.findSolver(it)?.let(::MemCachedSolver) }
    }
}

class MemCachedSolver(private val solver: Solver) : Solver {

    private val cache = mutableMapOf<String, Any>()

    override suspend fun solve(input: String) =
        cache.computeIfAbsent(input, solver::solve)

}

suspend fun <K : Any, V : Any> MutableMap<K, V>.computeIfAbsent(key: K, mappingFunction: suspend (K) -> V): V {
    val v = get(key)
    if (v != null) return v
    return mappingFunction(key).also { put(key, it) }
}
