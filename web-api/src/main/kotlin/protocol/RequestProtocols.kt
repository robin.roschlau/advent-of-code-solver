package com.github.roschlau.adventofcode.web.protocol

import com.github.roschlau.adventofcode.web.extensions.httpStatus
import io.ktor.http.HttpStatusCode
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Instant

/**
 * Class to access the DB-based request protocols.
 */
class RequestProtocols(private val db: Database) {

    companion object : Table("request_protocol") {
        private val timestamp = long("timestamp")
            .clientDefault { Instant.now().toEpochMilli() }
        private val callerIp = varchar("caller_ip", length = 45)
        private val route = varchar("route", length = 256)
        private val code = httpStatus("code").nullable()
    }

    /** Inserts a new protocol entry into the database. */
    fun insert(callerIp: String, route: String, code: HttpStatusCode?) = transaction(db) {
        insert {
            it[this.callerIp] = callerIp
            it[this.route] = route
            it[this.code] = code
        }
    }

    /** Retrieves all protocol entries currently in the database. */
    fun getAll(): List<ProtocolEntry> = transaction(db) {
        selectAll().map { row ->
            ProtocolEntry(
                Instant.ofEpochMilli(row[timestamp]),
                row[callerIp],
                row[route],
                row[code]
            )
        }
    }
}

data class ProtocolEntry(
    val timestamp: Instant,
    val callerIp: String,
    val route: String,
    val code: HttpStatusCode?
)
