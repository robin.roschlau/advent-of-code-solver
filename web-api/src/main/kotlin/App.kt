package com.github.roschlau.adventofcode.web

import com.github.roschlau.adventofcode.core.coreModule
import com.github.roschlau.adventofcode.web.caching.MemCachedSolvers
import com.github.roschlau.adventofcode.web.extensions.HttpStatusException
import com.github.roschlau.adventofcode.web.extensions.jacksonModule
import com.github.roschlau.adventofcode.web.extensions.serialize
import com.github.roschlau.adventofcode.web.protocol.RequestProtocols
import io.ktor.application.Application
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.Compression
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.features.StatusPages
import io.ktor.features.gzip
import io.ktor.http.HttpHeaders
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.request.uri
import io.ktor.response.respond
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import java.time.Instant
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.Koin
import org.koin.core.context.startKoin
import org.koin.core.qualifier.StringQualifier
import org.koin.dsl.module
import org.koin.experimental.builder.single
import org.koin.logger.SLF4JLogger

fun main() {
    // Start our dependency injection context
    val koin = startKoin() {
        modules(listOf(coreModule, webModule))
        logger(SLF4JLogger())
    }.koin
    embeddedServer(Netty) {
        main(koin)
    }.start(wait = true)
}

@OptIn(KtorExperimentalLocationsAPI::class)
fun Application.main(
    koin: Koin,
) {

    // Enable and configure some ktor features
    // See what is available and generate your own here: https://start.ktor.io
    install(DefaultHeaders) {
        header(HttpHeaders.Server, koin.get(StringQualifier(Properties.serverName)))
    }
    install(CallLogging)
    install(Compression) { gzip() }
    install(StatusPages) {
        exception<HttpStatusException> { exception ->
            call.respond(exception.code, exception.statusText)
        }
    }
    install(ContentNegotiation) {
        jacksonModule {
            serialize<Instant> { value, gen, _ -> gen.writeNumber(value.toEpochMilli()) }
        }
    }

    // Finally, some custom configuration
    installRequestLogging(koin.get())
    installRoutes(koin.get(), koin.get())
}

/**
 * Installs an interceptor that logs all incoming requests to the database.
 */
fun Application.installRequestLogging(protocols: RequestProtocols) {
    intercept(ApplicationCallPipeline.Monitoring) {
        proceed()
        protocols.insert(call.request.local.remoteHost, call.request.uri, call.response.status())
    }
}

@Suppress("RemoveExplicitTypeArguments") // We want to be explicit here to prevent accidentally changing injected types
val webModule = module {
    single(StringQualifier(Properties.serverName)) { "Advent of Code Solver" }
    single<MemCachedSolvers>()
    single<RequestProtocols>()
    single<Database>(createdAtStart = true) {
        Database.connect(url = "jdbc:h2:mem:aoc;DB_CLOSE_DELAY=-1", driver = "org.h2.Driver").also { db ->
            transaction(db) {
                SchemaUtils.createMissingTablesAndColumns(RequestProtocols)
            }
        }
    }
}

/**
 * Defines dependency names to use with koin
 */
object Properties {
    const val serverName = "Server Name"
}
