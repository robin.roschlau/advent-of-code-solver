package com.github.roschlau.adventofcode.web

import com.github.roschlau.adventofcode.core.solvers.Solvers
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class AppTest {

    /**
     * Tests that the server starts and responds on the /health endpoint with a fixed phrase on startup.
     */
    @Test
    fun `app runs`() = appTest {
        handleRequest(HttpMethod.Get, "/health").run {
            assertEquals(HttpStatusCode.OK, response.status())
            assertEquals("Hello there", response.content)
        }
    }

    @Test
    fun `report solver keys`() = appTest {
        handleRequest(HttpMethod.Get, "/keys").run {
            assertEquals(HttpStatusCode.OK, response.status())
            assertEquals(
                testKoin.get<Solvers>().knownKeys.joinToString("\",\"", "[\"", "\"]"),
                response.content
            )
        }
    }

    @Test
    fun `solve dummy solver via GET`() = appTest {
        val input = "DummyInput"
        handleRequest(HttpMethod.Get, "/0/0/1/solve/$input").run {
            assertEquals(HttpStatusCode.OK, response.status())
            assertEquals(input, response.content)
        }
    }

    @Test
    fun `solve dummy solver via POST`() = appTest {
        val input = "DummyInput"
        handleRequest(HttpMethod.Post, "/0/0/1/solve") {
            setBody(input)
        }.run {
            assertEquals(HttpStatusCode.OK, response.status())
            assertEquals(input, response.content)
        }
    }

    @Test
    fun `check dummy solver with correct solution`() = appTest {
        val input = "DummyInput"
        handleRequest(HttpMethod.Post, "/0/0/1/check") {
            addHeader("Content-Type", "application/json")
            setBody("""{ "input": "$input", "proposedSolution": "$input" }""")
        }.run {
            assertEquals(HttpStatusCode.OK, response.status())
            assertEquals("Correct solution", response.content)
        }
    }

    @Test
    fun `check dummy solver with wrong solution`() = appTest {
        val input = "DummyInput"
        handleRequest(HttpMethod.Post, "/0/0/1/check") {
            addHeader("Content-Type", "application/json")
            setBody("""{ "input": "$input", "proposedSolution": "Blah" }""")
        }.run {
            assertEquals(HttpStatusCode.OK, response.status())
            assertEquals("Wrong solution", response.content)
        }
    }

    @Test
    fun `solver not found`() = appTest {
        val input = "DummyInput"
        handleRequest(HttpMethod.Get, "/2010/1/1/solve/$input").run {
            assertEquals(HttpStatusCode.NotFound, response.status())
        }
    }

    @Test
    fun `puzzle part outside of allowed range`() = appTest {
        val input = "DummyInput"
        handleRequest(HttpMethod.Get, "/2017/1/3/solve/$input").run {
            assertEquals(HttpStatusCode.BadRequest, response.status())
        }
    }

}
