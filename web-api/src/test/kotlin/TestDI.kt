package com.github.roschlau.adventofcode.web

import com.github.roschlau.adventofcode.core.coreModule
import com.github.roschlau.adventofcode.web.protocol.RequestProtocols
import io.ktor.server.testing.TestApplicationEngine
import io.ktor.server.testing.withTestApplication
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.context.startKoin
import org.koin.logger.SLF4JLogger

/**
 * Wrapper around [withTestApplication] so that we can do some custom setup around tests
 */
fun appTest(test: TestApplicationEngine.() -> Unit) {
    // Clear out the database so each test runs on a clean slate
    transaction(testKoin.get()) {
        RequestProtocols.deleteAll()
    }
    // Run the test body with our app module running in a Test Engine that doesn't do actual HTTP processing
    withTestApplication(moduleFunction = { main(testKoin) }, test = test)
}

val testKoin = startKoin {
    modules(listOf(coreModule, webModule))
    logger(SLF4JLogger())
}.koin
