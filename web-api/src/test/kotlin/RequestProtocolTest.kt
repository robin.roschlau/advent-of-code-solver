package com.github.roschlau.adventofcode.web

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.github.roschlau.adventofcode.web.extensions.deserialize
import com.github.roschlau.adventofcode.web.extensions.module
import com.github.roschlau.adventofcode.web.protocol.ProtocolEntry
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import java.time.Instant
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RequestProtocolTest {

    /**
     * Makes a few requests to the service, and then checks that these calls are recorded and properly returned from the
     * /log endpoint.
     *
     * Specifically, this test makes requests toward the endpoints /health, /keys and /log as well as the not existing
     * endpoint /blah, and after that checks that the response to another call of /log looks something like this:
     * ```
     * [
     *  { timestamp: <epochMillis>, callerIp: "localhost", route: "/health", code: 200 },
     *  { timestamp: <epochMillis>, callerIp: "localhost", route: "/keys", code: 200 },
     *  { timestamp: <epochMillis>, callerIp: "localhost", route: "/log", code: 200 },
     *  { timestamp: <epochMillis>, callerIp: "localhost", route: "/blah", code: null }
     * ]
     * ```
     * Where `<epochMillis>` of course is a number representing a timestamp in milliseconds since epoch, though the
     * exact timestamps are not checked explicitly.
     * The second call to the /log endpoint itself is not included in that list, as logging should happen after the
     * request has finished processing, so the http code can be properly determined. That's the reason a 404 http code
     * is represented as `null` in the log, because logging happens before ktor determines that a request is unhandled.
     * If you find a way to do logging after that point, feel free to adjust this test accordingly.
     */
    @Test
    fun `requests are logged and can be queried`() = appTest {
        handleRequest(HttpMethod.Get, "/health")
        handleRequest(HttpMethod.Get, "/keys")
        handleRequest(HttpMethod.Get, "/log")
        handleRequest(HttpMethod.Get, "/blah")
        handleRequest(HttpMethod.Get, "/log").run {
            assertEquals(HttpStatusCode.OK, response.status())
            val jackson = jacksonObjectMapper().module {
                deserialize<Instant> { parser, _ -> Instant.ofEpochMilli(parser.longValue) }
            }
            assertEquals(
                listOf(
                    ProtocolEntry(Instant.EPOCH, "localhost", "/health", HttpStatusCode.OK),
                    ProtocolEntry(Instant.EPOCH, "localhost", "/keys", HttpStatusCode.OK),
                    ProtocolEntry(Instant.EPOCH, "localhost", "/log", HttpStatusCode.OK),
                    ProtocolEntry(Instant.EPOCH, "localhost", "/blah", null)
                ),
                jackson.readValue<List<ProtocolEntry>>(response.content!!)
                    .map { it.copy(timestamp = Instant.EPOCH) }
            )
        }
    }
}
