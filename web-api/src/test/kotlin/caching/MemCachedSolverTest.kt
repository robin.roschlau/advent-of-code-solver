package com.github.roschlau.adventofcode.web.caching

import com.github.roschlau.adventofcode.core.solvers.Solver
import io.mockk.MockKAdditionalAnswerScope
import io.mockk.MockKStubScope
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class MemCachedSolverTest {

    /** Mocked solver that will just return the input unchanged as the puzzle solution */
    private val backingSolver = mockk<Solver> {
        coEvery { solve(any()) } returnsArgument 0
    }

    /** The [MemCachedSolver] instance that is being tested */
    private val cachedSolver = MemCachedSolver(backingSolver)

    /**
     * Tests that the [cachedSolver] properly delegates calls to its [Solver.solve] function to the
     * [backingSolver].
     */
    @Test
    fun `delegates solving different inputs to backing solver`() = runBlocking {
        val solution1 = cachedSolver.solve("Input 1")
        val solution2 = cachedSolver.solve("Input 2")
        assertEquals("Input 1", solution1)
        assertEquals("Input 2", solution2)
    }

    /**
     * Tests that, if its [Solver.solve] function is called twice in a row with the same input, [cachedSolver] will only
     * once call the [backingSolver], cache the result and reuse the result for the second call.
     */
    @Test
    fun `calls backing solver only once for same inputs`() = runBlocking {
        cachedSolver.solve("Input")
        cachedSolver.solve("Input")
        coVerify(exactly = 1) { backingSolver.solve("Input") }
    }
}

/**
 * Makes the mocked function call directly return one of its arguments.
 * Requires that argument's type to be assignment compatible with the return type of the function.
 * This condition can't be checked at compile time, but will be checked at runtime via casting.
 *
 * @param n Positionally determines which argument to return (zero-based)
 */
inline infix fun <reified T> MockKStubScope<T, T>.returnsArgument(n: Int): MockKAdditionalAnswerScope<T, T> =
    this answers { invocation.args[n] as T }
