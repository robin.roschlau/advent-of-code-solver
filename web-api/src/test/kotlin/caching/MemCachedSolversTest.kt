package com.github.roschlau.adventofcode.web.caching

import com.github.roschlau.adventofcode.core.solvers.Solver
import com.github.roschlau.adventofcode.core.solvers.Solvers
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test


class MemCachedSolversTest {
    val testKey = "Test-Key"

    /** Mocked solver that will just return the input unchanged as the puzzle solution */
    private val backingSolver = mockk<Solver> {
        coEvery { solve(any()) } returnsArgument 0
    }

    /** The [MemCachedSolvers] instance that is being tested */
    private val backingSolvers = mockk<Solvers> {
        every { knownKeys } returns setOf(testKey)
        every { findSolver(testKey) } returns backingSolver
        every { findSolver(any()) } returns backingSolver
    }

    private val cachedSolvers = MemCachedSolvers(backingSolvers)

    @Test
    fun `passes keys through`() {
        assertEquals(setOf(testKey), cachedSolvers.knownKeys)
    }

    @Test
    fun `wraps solvers through`() = runBlocking {
        val solver = cachedSolvers.findSolver(testKey)
        assertTrue(solver is MemCachedSolver)
        assertEquals("Test", solver!!.solve("Test"))
        coVerify(exactly = 1) { backingSolver.solve("Test") }
    }
}
