# Module "web-api"

## Module description
This module provides a Web interface to the Advent of Code solvers.

The three main endpoints are:

- "/keys" for listing all known solver keys
- "/<year>/<day>/<part>/solve" for solving a puzzle
- "/<year>/<day>/<part>/check" for checking a proposed solution.

For specifics of how the endpoints are supposed to work, check the tests in [AppTest](./src/test/kotlin/AppTest.kt).

## Running the tests
`./gradlew :web-api:check`

## Running the server from IntelliJ
Run the `main`-function inside [App.kt](./src/main/kotlin/App.kt)
